import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Bar,
  mixins: [reactiveProp],
  //props: ['options'],
  data : () =>({
    options: {legend: {
        labels: {
          // This more specific font property overrides the global property
          fontColor: 'white'
        }

    },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }

  }}),
  mounted () {
    this.renderChart(this.chartData, this.options)
  },
  methods: {
    updateData() {
      this.chartData = {}; // without this step, it does not work (no reactive behaviour). Why is this necessary?
      this.chartData = this.data;
    }
  }
}
