var layoutControlVersion = '1.0.74';
var layoutControlDevMode = true;

// GLOBAL VARIABLES
var bScriptLoaded = false;
var loadCompleteCallback;

var layoutMinRoofArea = 10; //10 meters squared

//getting location of script while loading script
var layoutControlScripts = document.getElementsByTagName("script"),
  layoutControlSrc = layoutControlScripts[layoutControlScripts.length - 1].src,
  layoutControlSrcArr = layoutControlSrc.split('/');
layoutControlSrcArr.splice(layoutControlSrcArr.length - 1, 1);
layoutControlSrc = layoutControlSrcArr.join('/');

var layoutControlParentFolderPrefix = layoutControlSrc + '/';

var lc_heap = [];

var layoutControlCopyright = "<div style=\"float: left;\">Copyright 2015 (c) PVComplete</div><div style=\"float: right;\">" + layoutControlVersion + "</div>";

var CYCLE_MIN_RADIUS = 0.1524; //min radius = 0.5 feet

//spacing values for add modules
var rowSpacing = 0;
var columnSpacing = 0;

var layoutControlElementColors = {
  Unclassified: '#f0ed08',
  Roof: '#1060ff',
  Obstruction: '#ff6010',
  InstallationArea: '#409000',
  Setback: '#ff6010',
  CornerZone: '#ff6010',
  EdgeZone: '#FF9F00',
  InteriorZone: '#409000',
  SnapAzimuth: "#FF9F00",
  Rails: '#ff6010',
  Splices: "#FFFFFF",
  Attachments: "#00ff00",
  Shadow: '#949494',
  RectangleArea: '#0daeff'
};

var layoutControlElementZIndexes = {
  Unclassified: 8000,
  Roof: 1000,
  Parapet: 500,
  Obstruction: 2000,
  Module: 3000,
  Setback: 4000,
  CornerZone: 5000,
  EdgeZone: 5250,
  InteriorZone: 5500,
  RectangularSelection: 9000,
  Shadow: 1100
};

var layoutControlMeasurementSystems = {
  1: {
    displayName: 'meters',
    shortName: 'm',
    convertFactor: 1,
    secondUnitId: 1
  },
  2: {
    displayName: 'inches',
    shortName: 'in',
    convertFactor: 39.3701,
    secondUnitId: 2
  },
  3: {
    displayName: 'feet',
    shortName: 'ft',
    convertFactor: 3.28084,
    secondUnitId: 2
  }
};

var layoutControlMaxZoomRes = 21;

var layoutControlRoofGeometry = {
  Setback: true,
  CornerZones: true,
  EdgeZones: false,
  InteriorZone: true
};

// Layout Control data cache

var layoutControlCachePrefix = 'layoutControl',
  layoutControlCacheDataTypes = ['moduleAzimuthArrows'];

var Cache = function() {
  this.storage = window.localStorage;
  var dataTypeCount = layoutControlCacheDataTypes.length;
  for (var i = 0; i < dataTypeCount; i++) {
    var data = JSON.parse(this.storage.getItem(layoutControlCachePrefix + '.' + layoutControlCacheDataTypes[i]));
    if (!data) {
      data = {
        data: null
      }
    }
    this[layoutControlCacheDataTypes[i]] = data.data;
  }
};

Cache.prototype.storeData = function(dataType, data) {
  var bDataTypeExists = false
  dataTypeCount = layoutControlCacheDataTypes.length;
  for (var i = 0; i < dataTypeCount; i++) {
    if (layoutControlCacheDataTypes[i] == dataType) {
      bDataTypeExists = true;
      break;
    }
  }

  if (!bDataTypeExists) {
    throw 'LayoutControl: Can\'t store cache data - \'' + dataType + '\' data type doesn\'t exist';
  }

  var jsonData = JSON.stringify({
    data: data
  });
  this.storage.setItem(layoutControlCachePrefix + '.' + dataType, jsonData);
  this[dataType] = data;
};

var layoutControlCache = new Cache();

// INITIALIZATION PART

function InitializeLayoutControl(onLoadComplete, googleAPIKey) {
  loadCompleteCallback = onLoadComplete;
  //  if (layoutControlDevMode)
  //  {
  //    loadScript(layoutControlParentFolderPrefix + 'layoutControl_helpers.js');
  //  }
  //loadScript("https://maps.googleapis.com/maps/api/js?v3.24&" + ((typeof googleAPIKey === 'undefined' || googleAPIKey === '') ? "" : "key=" + googleAPIKey + "&") + "callback=LoadComplete&libraries=drawing,places,geometry");
  //loadScript("https://maps.googleapis.com/maps/api/js?" + ((typeof googleAPIKey === 'undefined' || googleAPIKey === '') ? "" : "key=" + googleAPIKey + "&") + "callback=LoadComplete&libraries=drawing,places,geometry");
  LoadComplete();
}

function loadScript(src) {
  fileref = document.createElement('script');
  fileref.setAttribute("type", "text/javascript");
  fileref.setAttribute("src", src);
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

function LoadComplete() {
  bScriptLoaded = true;
  //console.log("Google maps script loaded");

  loadCompleteCallback();
}

// Basic objects

var RoofFace = function(path, azimuth, slope) {
  this.polygon2dLocal = path;
  this.name = "";
  var firstLineHeading = parseFloat(google.maps.geometry.spherical.computeHeading(new google.maps.LatLng(path.vertices[0].y, path.vertices[0].x), new google.maps.LatLng(path.vertices[1].y, path.vertices[1].x)).toFixed(0));
  this.azimuth = 90 + firstLineHeading;

  if (this.azimuth < 0)
    this.azimuth = this.azimuth + 360;

  this.slope = slope;
};

var Roof = function(idx, roofFaces, name, description, meanHeight, parapetWall, parapetWallHeight, setback, leastHorizontalDistance, customSetbacks, lc, primitive) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.meanHeight = meanHeight;
  this.parapetWall = parapetWall;
  this.parapetWallHeight = parapetWallHeight;
  this.parapetWallObjects = [];
  this.setback = setback;
  this.customSetbacks = customSetbacks;
  this.leastHorizontalDistance = leastHorizontalDistance;
  this.primitive = primitive;
  this.modules = [];
  this.rails = [];
  this.splices = [];
  this.attachments = [];

  this.roofFaces = roofFaces;
  this.aZone = this.roofFaces[0].aZone;

  this.obstructions = [];
  this.installationAreas = [];

  lc.TransformObject(this, "roof");

  this.AddChangeEventListeners(primitive, lc);

  this.azimuthArrow = this.GetAzimuthArrow(lc, primitive, this.roofFaces[0].azimuth);

  this.DrawGeometry(lc);

  google.maps.event.addListener(this.azimuthArrow, 'click', function() {
    lc.ObjectSelected(this);
  });
};

Roof.prototype.DrawGeometry = function(lc, successCb, errorCb) {
  if (this.drawGeometryDebounce)
    clearTimeout(this.drawGeometryDebounce);

  var instance = this;
  this.drawGeometryDebounce = setTimeout(function() {
    instance.DrawGeometryDebounced(lc, successCb, errorCb);
  }, 150);
};

Roof.prototype.DrawGeometryDebounced = function(lc, successCb, errorCb) {
  if (typeof this.setback != 'number')
    this.setback = parseFloat(this.setback);

  this.ClearGeometry(lc);


  if (this.drawSetbackRequest && this.drawSetbackRequest.readyState != 4) {
    this.drawSetbackRequest.abort();
  }

  var polygon2d = GetPolygon2D(this.primitive),
    setback = lc.ConvertNumberFromTo(this.setback, layoutControlMeasurementSystems[lc.measurementSystem].secondUnitId, 2);
  var roof = this;

  var successFn = function(response) {

    var setback = response.Setback;
    var cornerZones = response.CornerZones;
    var edgeZones = response.EdgeZones;
    var interiorZone = response.InteriorZones;
    var aZone = response.AZone;

    roof.ClearGeometry(lc);

    if (!lc.GetRoofByIndex(roof.index)) return; // validation for async requests

    var setbackAreas = [];
    setback.forEach(function(s) {
      var idx = s.idx ? s.idx : 0;
      if (!setbackAreas[idx]) setbackAreas[idx] = [];
      setbackAreas[idx].push(s);
    });
    setbackAreas.forEach(function(s) {
      if (s) roof.DrawSetback(lc, s);
    });

    if (lc.workspaceId != 6) // SKETCH-2603 dont draw parapet shadows for eco
      roof.DrawParapet(lc);

    roof.DrawCornerZones(lc, cornerZones);
    //
    //    roof.DrawEdgeZones(lc, edgeZones);
    //
    roof.DrawInteriorZone(lc, interiorZone);

    roof.roofFaces[0].aZone = aZone;
    if (response.LeastHorizontalDimension)
      roof.leastHorizontalDistance = lc.ConvertNumberFromTo(response.LeastHorizontalDimension, 2, layoutControlMeasurementSystems[lc.measurementSystem].secondUnitId, 2);
    lc.roofDataUpdated();
  };

  var errorFn = function(response) {
    if (response.statusText != 'abort')
      console.error('error acquiring geometry: ' + response.statusText, response);
  };
  var azimuth = 180 - parseFloat(this.roofFaces[0].azimuth);
  if (azimuth > 180)
    azimuth -= 360;
  azimuth = -azimuth;

  var customSetbacks = Object.assign([], this.customSetbacks);
  // Validate custom setback array
  var validCustomSetbacks = [];
  customSetbacks.forEach(function(item, index, array) {
    if (!isNaN(parseFloat(item))) {
      validCustomSetbacks.push(parseFloat(item));
    }
  });
  customSetbacks = validCustomSetbacks;

  mh = lc.ConvertNumberFromTo(this.meanHeight, lc.measurementSystem, 2);
  lhd = lc.ConvertNumberFromTo(this.leastHorizontalDistance, layoutControlMeasurementSystems[lc.measurementSystem].secondUnitId, 2);
  if (lc.measurementSystem != 3) {
    customSetbacks.forEach(function(item, index, array) {
      array[index] = lc.ConvertNumberFromTo(item, lc.measurementSystem, 2, 4)
    });
  }

  this.drawSetbackRequest = lc.GetSetbackForPolygon2D(polygon2d, setback, mh, lhd, customSetbacks, azimuth, successFn, errorFn);
};

Roof.prototype.DrawSetback = function(lc, responsePoints) {
  if ((this.setback == 0 && this.customSetbacks.every(item = > item == 0)) || !layoutControlRoofGeometry.Setback)
  return;

  var points = [];

  var setbackCount = responsePoints.length;
  for (var i = 0; i < setbackCount; i++) {
    var latlng = new google.maps.LatLng(responsePoints[i].y, responsePoints[i].x);
    points.push(latlng);
  }

  if (!this.setbackPolygon) this.setbackPolygon = [];
  this.setbackPolygon.push(new google.maps.Polygon({
    paths: points,
    strokeColor: layoutControlElementColors.Setback,
    strokeOpacity: 1,
    strokeWeight: 2,
    fillOpacity: 0,
    map: lc.map,
    zIndex: layoutControlElementZIndexes.Setback,
    clickable: false
  }));
};

Roof.prototype.DrawCornerZones = function(lc, responsePoints) {
  if (!layoutControlRoofGeometry.CornerZones)
    return;

  var cornerZonesCount = responsePoints.length;
  for (var i = 0; i < cornerZonesCount; i++) {
    var zonePointCount = responsePoints[i].length;
    var polygonPoints = [];
    for (var k = 0; k < zonePointCount; k++) {
      var point = responsePoints[i][k];
      polygonPoints.push(new google.maps.LatLng(point.y, point.x));
    }

    // var polygon = new google.maps.Polygon({
    //   paths: polygonPoints,
    //   strokeColor: layoutControlElementColors.CornerZone,
    //   fillColor: layoutControlElementColors.CornerZone,
    //   strokeOpacity: 1,
    //   strokeWeight: 2,
    //   fillOpacity: 0.5,
    //   map: lc.map,
    //   zIndex: layoutControlElementZIndexes.CornerZone,
    //   clickable: false
    // });

    var polygon = lc.canvas.createPolygon({
      path: polygonPoints,
      strokeColor: layoutControlElementColors.CornerZone,
      fillColor: layoutControlElementColors.CornerZone,
      strokeOpacity: (lc.roofGeometryBlocked ? 0 : 1),
      strokeWeight: 2,
      fillOpacity: (lc.roofGeometryBlocked ? 0 : 0.5)
    });

    this.cornerZones.push(polygon);
  }
};

Roof.prototype.DrawEdgeZones = function(lc, responsePoints) {
  if (!layoutControlRoofGeometry.EdgeZones)
    return;

  var edgeZonesCount = responsePoints.length;
  for (var i = 0; i < edgeZonesCount; i++) {
    var zonePointCount = responsePoints[i].length;
    var polygonPoints = [];
    for (var k = 0; k < zonePointCount; k++) {
      var point = responsePoints[i][k];
      new google.maps.Marker({
        position: new google.maps.LatLng(point.y, point.x),
        map: lc.map
      });
      polygonPoints.push(new google.maps.LatLng(point.y, point.x));
    }

    var polygon = new google.maps.Polygon({
      paths: polygonPoints,
      strokeColor: layoutControlElementColors.EdgeZone,
      fillColor: layoutControlElementColors.EdgeZone,
      strokeOpacity: 1,
      strokeWeight: 2,
      fillOpacity: 0.5,
      map: lc.map,
      zIndex: layoutControlElementZIndexes.EdgeZone,
      clickable: false
    });

    this.edgeZones.push(polygon);
  }
};

Roof.prototype.DrawInteriorZone = function(lc, responsePoints) {
  if (!layoutControlRoofGeometry.InteriorZone)
    return;

  var zonePointCount = responsePoints.length;
  var polygonPoints = [];
  for (var k = 0; k < zonePointCount; k++) {
    var point = responsePoints[k];
    polygonPoints.push(new google.maps.LatLng(point.y, point.x));
  }

  // var polygon = new google.maps.Polygon({
  //   paths: polygonPoints,
  //   strokeColor: layoutControlElementColors.InteriorZone,
  //   fillColor: layoutControlElementColors.InteriorZone,
  //   strokeOpacity: 1,
  //   strokeWeight: 2,
  //   fillOpacity: 0.5,
  //   map: lc.map,
  //   zIndex: layoutControlElementZIndexes.InteriorZone,
  //   clickable: false
  // });
  var polygon = lc.canvas.createPolygon({
    path: polygonPoints,
    strokeColor: layoutControlElementColors.InteriorZone,
    fillColor: layoutControlElementColors.InteriorZone,
    strokeOpacity: (lc.roofGeometryBlocked ? 0 : 1),
    strokeWeight: 2,
    fillOpacity: (lc.roofGeometryBlocked ? 0 : 0.5)
  });

  this.interiorZone = polygon;
};

Roof.prototype.ClearGeometry = function(lc) {

  if (this.setbackPolygon) {
    this.setbackPolygon.forEach(function(s) {
      s.setMap(null);
    });
    this.setbackPolygon = null;
  }

  if (this.interiorZone && lc && lc.canvas) {
    lc.canvas.removePolygon(this.interiorZone);
    this.interiorZone = null;
  }

  if (this.cornerZones instanceof Array && this.cornerZones.length > 0 && lc && lc.canvas) {
    var windZoneCount = this.cornerZones.length;
    for (var i = 0; i < windZoneCount; i++) {
      lc.canvas.removePolygon(this.cornerZones[i]);
    }
    this.cornerZones.length = 0;
  }

  if (this.edgeZones instanceof Array && this.edgeZones.length > 0) {
    var windZoneCount = this.edgeZones.length;
    for (var i = 0; i < windZoneCount; i++) {
      this.edgeZones[i].setMap(null);
    }
    this.edgeZones.length = 0;
  }

  this.ClearParapet(lc);

  this.drawSetbackRequest = null;
  this.setbackPolygon = null;
  this.cornerZones = [];
  this.edgeZones = [];
  this.interiorZone = null;
};

var overlaps = (function() {
  function getPositions(elem) {
    var pos, width, height;
    pos = $(elem).position();
    width = $(elem).width();
    height = $(elem).height();
    return [[pos.left, pos.left + width], [pos.top, pos.top + height]];
  }

  function comparePositions(p1, p2) {
    var r1, r2;
    r1 = p1[0] < p2[0] ? p1 : p2;
    r2 = p1[0] < p2[0] ? p2 : p1;
    return r1[1] > r2[0] || r1[0] === r2[0];
  }

  return function(a, b) {
    var pos1 = getPositions(a),
      pos2 = getPositions(b);
    return comparePositions(pos1[0], pos2[0]) && comparePositions(pos1[1], pos2[1]);
  };
})();

var layoutControlProjectMinRoofLinesAverage = null;

Roof.prototype.GetPolygonLines = function() {
  var points = this.roofFaces[0].polygon2dLocal.vertices;

  var lines = [];
  var pointsLength = points.length;
  for (var i = 0; i < pointsLength; i++) {
    var p1 = i;
    var p2 = i + 1;
    if (p2 == pointsLength)
      p2 = 0;

    lines.push({
      p1: new google.maps.LatLng(points[p1].y, points[p1].x),
      p2: new google.maps.LatLng(points[p2].y, points[p2].x)
    });
  }
  return lines;
};

Roof.prototype.ComputeAverageLineDistance = function() {
  var lines = this.GetPolygonLines();
  var linesLength = lines.length;
  var sumDist = 0;
  for (var i = 0; i < linesLength; i++) {
    sumDist += google.maps.geometry.spherical.computeDistanceBetween(lines[i].p1, lines[i].p2);
  }
  return sumDist / linesLength;
};

Roof.prototype.RefreshDistanceLabels = function(lc, bHide, overlapping) {
  // calculate label positions
  // form every line segment into a comfy array
  var primitive = this.primitive;
  var zoom = lc.map.getZoom();

  var lines = this.GetPolygonLines();

  // calculate every line midpoint and every line distance
  var midPoints = [];
  var linesLength = lines.length;

  var zoomDensity = {
    23: 141.06215,
    22: 282.1243,
    21: 564.2486,
    20: 1128.497220,
    19: 2256.994440,
    18: 4513.988880,
    17: 9027.977761,
    16: 18055.955520,
    15: 36111.911040
  };

  var zoomOffsetDensity = {
    23: 8,
    22: 6,
    21: 2.1,
    20: 1,
    19: 0.3,
    18: 0.1,
    17: 0.03,
    16: 0.01,
    15: 0.003
  };

  var minDist = layoutControlProjectMinRoofLinesAverage;
  if (minDist < 4) {
    minDist = 4;
  }

  var showCoeff = parseFloat(((minDist * 10) / (zoomDensity[zoom] == undefined ? zoomDensity[20] / (24 - zoom) : zoomDensity[zoom])).toFixed(2));
  var coeff = Math.sqrt(23.5 - zoom) / Math.sqrt(zoomOffsetDensity[zoom] == undefined ? 1 : zoomOffsetDensity[zoom]);

  var bSelected = lc.IsObjectSelected(this);

  // Shift lines so labels appear correctly when drawn clockwise.
  var newLines = lines;
  var popped = newLines.pop();

  newLines.unshift(popped);

  lines = newLines;

  for (var i = 0; i < linesLength; i++) {
    $('.map-distance-label.label-' + this.index + '-' + i).remove();

    if (!bSelected && !primitive.hover)
      continue;

    if (zoom < 15 || bHide || primitive.shapeType == 'circle' || showCoeff <= 0.07 || primitive.dragging)
      continue;

    // var mid = google.maps.geometry.spherical.interpolate(lines[i].p1, lines[i].p2, 0.5);
    var mid = computeMidpoint(lines[i].p1, lines[i].p2);
    var dist = google.maps.geometry.spherical.computeDistanceBetween(lines[i].p1, lines[i].p2);

    var offsetHeading = google.maps.geometry.spherical.computeHeading(lines[i].p1, lines[i].p2);
    offsetHeading += 90;
    if (offsetHeading < 0)
      offsetHeading += 360;

    if (IsPolygonPointsClockwise(primitive)) {
      if (offsetHeading + 180 > 360)
        offsetHeading -= 180;
      else
        offsetHeading += 180;
    }

    var labelHeading = offsetHeading;

    if (overlapping instanceof Array && overlapping.indexOf(i) !== -1) {
      offsetHeading += 180;
    }

    var offset = coeff;
    var offsetMid = google.maps.geometry.spherical.computeOffset(mid, offset, offsetHeading);

    midPoints.push(offsetMid);

    var labelPos = fromLatLngToPoint(offsetMid, lc.map);
    if (labelHeading > 90 && labelHeading < 270)
      labelHeading += 180;

    var labelMeasurementSystem = layoutControlMeasurementSystems[lc.measurementSystem].shortName;

    var scope = angular.element("[ng-app = PVSketchFront]").scope();
    var blockContent = "";

    if (scope.$parent.bCustomSetBackClicked === true) {
      blockContent = '<div class="map-distance-label label-' + this.index + '-' + i + '" style="top: ' + labelPos.y + 'px; left: ' + labelPos.x + 'px; transform: translate(-50%, -50%) rotate(' + labelHeading + 'deg)">' + +(i + 1) +
        '</div>';
    } else {
      //blockContent = '<div class="map-distance-label label-' + this.index + '-' + i + '" style="top: ' + labelPos.y + 'px; left: ' + labelPos.x + 'px; transform: translate(-50%, -50%) rotate(' + labelHeading + 'deg)">' +
      //  +lc.ConvertNumberFromMeters(dist, lc.measurementSystem, 2) + ' ' + labelMeasurementSystem + ' - ' + (i + 1) +
      // '</div>';
      blockContent = '<div class="map-distance-label label-' + this.index + '-' + i + '" style="top: ' + labelPos.y + 'px; left: ' + labelPos.x + 'px; transform: translate(-50%, -50%) rotate(' + labelHeading + 'deg)">' + +lc.ConvertNumberFromMeters(dist, lc.measurementSystem, 2) + ' ' + labelMeasurementSystem '</div>';
    }

    $('#mapContainer').append(blockContent);
  }

  if (!bSelected && !primitive.hover)
    return;
  if (zoom < 15 || bHide || primitive.shapeType == 'circle' || showCoeff <= 0.09 || primitive.dragging)
    return;
  var overlapObjects = [];
  for (var i = 0; i < linesLength; i++) {
    for (var k = 0; k < linesLength; k++) {
      if (k == i)
        continue;

      var mainObject = $('.map-distance-label.label-' + this.index + '-' + i)[0];
      var comparingObject = $('.map-distance-label.label-' + this.index + '-' + k)[0];

      var bOverlaps = overlaps(mainObject, comparingObject);
      if (bOverlaps) {
        overlapObjects.push(i);
      }
    }
  }
  if (overlapObjects.length > 0 && overlapping == undefined) {
    this.RefreshDistanceLabels(lc, false, overlapObjects);
  }
};

var Obstruction = function(idx, primitive, name, description, height, setback, parentRoof, lc) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.height = height;
  this.setback = setback;
  this.primitive = primitive;

  this.polygon2dLocal = GetPolygon2D(primitive);

  this.parentRoof = parentRoof;

  lc.TransformObject(this, "obstruction");

  this.AddChangeEventListeners(primitive, lc);
}

var InstallationArea = function(idx, primitive, name, description, parentRoof, lc) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.polygon2dLocal = GetPolygon2D(primitive);
  this.primitive = primitive;

  this.parentRoof = parentRoof;

  this.AddChangeEventListeners(primitive, lc);
};

var Shadow = function(idx, primitive, name, description, parentObject) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.polygon2dLocal = GetPolygon2D(primitive);
  this.primitive = primitive;
  this.parentObject = parentObject;
};

var Unclassified = function(idx, primitive, name, description, lc) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.polygon2dLocal = GetPolygon2D(primitive);
  this.primitive = primitive;

  this.AddChangeEventListeners(primitive, lc);
};

Unclassified.prototype.AddChangeEventListeners = function(primitive, lc) {
  // add listeners for shape change
  var object = this;
  if (!primitive.shapeType)
    primitive.shapeType = 'polygon';
  switch (primitive.shapeType) {
    case "polygon":
      google.maps.event.addListener(primitive.getPath(), 'set_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        // object.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'insert_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        // object.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'remove_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
      });
      break;
    case "circle":
      google.maps.event.addListener(primitive, 'radius_changed', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        // object.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive, 'center_changed', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        // object.ChangeShape(lc, primitive);
      });
      break;
  }
};

var LayoutModule = function(lc, idx, primitive, azimuth, groupIndex, isOpposite, rowIndex, neighbours) {
  this.index = idx;
  this.primitive = primitive;
  this.azimuth = azimuth;
  this.groupIndex = groupIndex;
  this.isOpposite = isOpposite;
  this.rowIndex = rowIndex;
  this.neighbours = neighbours;

  var instance = this;
  this.GetAzimuthArrow(lc, function(arrow) {
    instance.azimuthArrow = arrow.marker;
  });

  // this.parentInstallationArea = installationArea;
};

LayoutModule.prototype.GetAzimuthArrow = function(lc, callback, key) {
  var opacity = this.azimuth == 0 ? 0 : 0.8;
  var instance = this;
  var azimuth = this.isOpposite ? parseInt(this.azimuth) + 180 : parseInt(this.azimuth);
  GetModuleAzimuthArrowIcon(azimuth, function(icon) {
    callback({
      marker: new google.maps.Marker({
        position: GetCentroid(GetPolygon2D(instance.primitive).vertices),
        map: lc.map,
        draggable: false,
        icon: icon.icon,
        clickable: false,
        visible: lc.moduleAzimuthArrowVisible
      }),
      bCached: icon.bCached
    }, key);
  });
};

var LayoutRail = function(idx, primitive, roofIndex) {
  this.index = idx;
  this.primitive = primitive;
  this.roofIndex = roofIndex;
};

var LayoutSplice = function(idx, primitive, roofIndex) {
  this.index = idx;
  this.primitive = primitive;
  this.roofIndex = roofIndex;
};

function GetModuleAzimuthArrowIcon(heading, callback) {
  heading = heading - 90;

  var canvas = document.createElement('canvas');
  var ctx = canvas.getContext('2d');

  var scale = 0.8;

  var cache = layoutControlCache.moduleAzimuthArrows;
  if (cache) {
    cache = cache[heading];
  }
  if (cache) {
    // console.log('module azimuth cache used');
    callback({
      bCached: false,
      icon: {
        url: cache.base64,
        anchor: new google.maps.Point(cache.anchorX, cache.anchorY)
      }
    });
  } else {
    cache = {};
    cache[heading] = null;
    var image = new Image();
    image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAcAAAATCAQAAAD2HN/VAAAAAnNCSVQICFXsRgQAAAAJcEhZcwAADdcAAA3XAUIom3gAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAsklEQVQYGU3BMQsBYQAG4Lcoi8VmumTTSSmFLIpFVnWTTZglg00Wic0ipZiNJjax8ANQBiOyia6479X1+XLPA20Cp6IIlfA3ZEPAC6XPD/N3KMnVlSfGZlAKT8Epg1lIAX9HkOU33JAizQ1NZs9Q0vsXt4x08eNqWWRTQNLbaz6Y2cHm03qCrJiQjBc5pqbDltjceGR0Alu4uuCbuQukukXWLHggjThn2IAyYHyJv9QBDl/1XFGi7erRfwAAAABJRU5ErkJggg==';
    image.onload = function() {
      var size = newSize(image.width, image.height, heading);

      ctx.canvas.width = size.width;
      ctx.canvas.height = size.height;

      ctx.translate(size.width / 2, size.height / 2);
      ctx.scale(scale, scale);
      ctx.rotate(heading * Math.PI / 180);
      ctx.translate(-size.width / 2, -size.height / 2);
      ctx.drawImage(image, size.width / 2 - image.width / 2, size.height / 2 - image.height / 2);

      var base64 = canvas.toDataURL(),
        anchorX = size.width / 2,
        anchorY = size.height / 2;

      cache[heading] = {
        base64: base64,
        anchorX: anchorX,
        anchorY: anchorY
      };
      layoutControlCache.storeData('moduleAzimuthArrows', cache);
      // console.log('module azimuth arrow cached');

      callback({
        bCached: true,
        icon: {
          url: base64,
          anchor: new google.maps.Point(anchorX, anchorY)
        }
      });
    };
  }

  // http://stackoverflow.com/a/19523585
  var newSize = function(w, h, a) {
    var rads = a * Math.PI / 180;
    var c = Math.cos(rads);
    var s = Math.sin(rads);
    if (s < 0) {
      s = -s;
    }
    if (c < 0) {
      c = -c;
    }
    var size = {};
    size.width = h * s + w * c;
    size.height = h * c + w * s;
    return size;
  };

  return;
}

LayoutModule.prototype.ChangeArrowHeading = function(newHeading) {
  this.azimuthArrow.setIcon(GetModuleAzimuthArrowIcon(newHeading));
};

LayoutModule.prototype.RefreshArrowPosition = function() {
  this.azimuthArrow.setPosition(GetCentroid(GetPolygon2D(this.primitive).vertices));
};

LayoutModule.prototype.AddListeners = function(lc) {
  var module = this;
  google.maps.event.addListener(module.primitive, 'click', function() {
    lc.ModuleSelected(module);
  });
};

LayoutModule.prototype.SetVisible = function(visible) {
  if (typeof visible != 'boolean')
    throw 'LayoutModule:SetVisible - 1st parameters(visible) should be boolean';

  this.primitive.setVisible(visible);
};

var mapObject = function(idx, type, primitive, data) {
  this.index = idx;
  this.type = type;
  this.primitive = primitive;
  this.data = data;
};

var layoutControlTileServers = [
  // 'https://khms.googleapis.com',
  // 'http://khms0.googleapis.com',
  // 'http://khms1.googleapis.com'
  // 'https://khms2.googleapis.com',
  // 'https://khms3.googleapis.com'
  'https://mt0.google.com',
  'https://mt1.google.com',
  'https://mt2.google.com',
  'https://mt3.google.com'
];
var layoutControlTileServer = 0;
var layoutControlLastActualZoom = 0;
var layoutControlLastZoom = 0;

var higherZoomMapType = function(tileSize, zoomLevel) {
  if (tileSize == undefined)
    tileSize = 256;

  this.zoomLevel = zoomLevel;
  this.id = 'HigherZoom';
  this.tileSize = new google.maps.Size(tileSize, tileSize);
};
higherZoomMapType.prototype.maxZoom = 22;
higherZoomMapType.prototype.changeTileSize = function(newSize) {
  // this.tileSize = new google.maps.Size(newSize, newSize);
  // console.log('tile size changed', newSize);
};
higherZoomMapType.prototype.getTile = function(coord, zoom, ownerDocument) {
  var div = ownerDocument.createElement('div');
  div.innerHTML = '';
  // div.innerHTML = this.tileSize.width;
  div.style.color = 'black';
  div.style.background = 'transparent';
  // div.style.border = '2px solid black';
  div.style.opacity = '0';
  div.style.transition = '0.3s opacity';
  div.style.width = this.tileSize.width + 'px';
  div.style.height = this.tileSize.height + 'px';
  var actualZoom = zoom;
  if (zoom > layoutControlMaxZoomRes) {
    actualZoom = layoutControlMaxZoomRes;
  }
  var imgCoords = $.extend({}, coord);
  // if (delta > 0)
  // {
  //   imgCoords.x = Math.ceil((coord.x / newTileSize) * 256);
  //   imgCoords.y = Math.ceil((coord.y / newTileSize) * 256);
  // }

  if (zoom == this.zoomLevel || this.zoomLevel == undefined) {
    var server = layoutControlTileServers[layoutControlTileServer];
    var correctUrl = server + '/vt/lyrs=s&x=' + imgCoords.x + '&y=' + imgCoords.y + '&z=' + actualZoom;
    div.style.backgroundImage = 'url("' + correctUrl + '")';
    div.style.backgroundSize = '100%';
    div.style.opacity = '1';

    layoutControlTileServer++;
    if (layoutControlTileServer > layoutControlTileServers.length - 1)
      layoutControlTileServer = 0;
  }

  return div;
};

function maxZoom(coords, zoom, callback) {
  var server = layoutControlTileServers[layoutControlTileServer];
  var delta;
  if (layoutControlLastZoom != 0)
    delta = zoom - layoutControlLastZoom;
  $.ajax({
    url: server + '/kh?v=702&x=' + coords.x + '&y=' + coords.y + '&z=' + zoom,
    success: function() {
      var correctUrl = server + '/kh?v=702&x=' + coords.x + '&y=' + coords.y + '&z=' + zoom;
      callback(correctUrl, zoom);
      layoutControlLastZoom = zoom;
    },
    error: function() {
      // Decreases amount of spam to Google KHMS servers
      if (layoutControlLastActualZoom != 0 && layoutControlLastActualZoom != undefined && zoom > layoutControlLastActualZoom)
        zoom = layoutControlLastActualZoom;
      else
        zoom--;
      maxZoom(coords, zoom, callback);
      layoutControlLastZoom = zoom;
    }
  });

  layoutControlTileServer++;
  if (layoutControlTileServer > layoutControlTileServers.length - 1)
    layoutControlTileServer = 0;
}

// IEP objects

var Vertex = function(x, y) {
  this.x = x;
  this.y = y;
}

var Polygon2D = function(vertices) {
  this.vertices = vertices;
}

/**
 * Get Polygon2D object with vertices got from given polygon
 * Parameters: polygon object from Google Map
 * Returns: Polygon2D object with vertices of given polygon
 */

function GetPolygon2D(polygon) {
  var vertices = [];

  var shapeType = polygon.shapeType;
  if (!shapeType)
    shapeType = 'polygon';

  switch (shapeType) {
    case "polygon":
      polygon.getPath().forEach(function(latLng) {
        vertices.push(new Vertex(parseFloat(latLng.lng()), parseFloat(latLng.lat())));
      });
      break;
    case "rectangle":
      var northEast = polygon.getBounds().getNorthEast();
      var southWest = polygon.getBounds().getSouthWest();
      vertices.push(new Vertex(southWest.lng(), northEast.lat())); // north west
      vertices.push(new Vertex(northEast.lng(), northEast.lat())); // north east
      vertices.push(new Vertex(northEast.lng(), southWest.lat())); // south east
      vertices.push(new Vertex(southWest.lng(), southWest.lat())); // south west
      break;
    case "circle":
      var currentAngle = 0;
      while (currentAngle < 360) {
        var point = google.maps.geometry.spherical.computeOffset(polygon.getCenter(), polygon.getRadius(), currentAngle);
        vertices.push(new Vertex(point.lng(), point.lat()));
        currentAngle += 22.5;
      }
      break;
  }

  return new Polygon2D(vertices);
}

/**
 * Get Polygon2D object from given point array
 * Parameters: array with points
 * Returns: Polygon2D object with vertices of given points
 */

function GetPolygon2DFromPoints(points) {
  if (points === 'undefined' || points.length == 0) {
    return;
  }
  var vertices = [];
  points.forEach(function(point) {
    vertices.push(new Vertex(point.x, point.y));
  });

  return new Polygon2D(vertices);
}

var CustomRoofEdgeSetBackLabelRefresh = function() {
  CustomSetBackLabelRefresh();
}

// Defining LayoutControl object

var LayoutControl = function(containerId, workspaceId, googleAPIKey, APIServiceUrl, markerVisible, measurementSystem, moduleColor, setbackServiceUrl, rackingConfigurationServiceUrl, shadowsServiceUrl) {
  var idx = lc_heap.length;

  this.objIdx = idx;
  this.APIServiceUrl = APIServiceUrl == undefined || APIServiceUrl == null || APIServiceUrl == "" ? "http://52.33.129.247" : APIServiceUrl;
  this.markerVisible = markerVisible === true ? true : false;
  this.measurementSystem = measurementSystem != undefined ? measurementSystem : 1;
  this.moduleColor = moduleColor != undefined ? moduleColor : "blue";
  this.setbackServiceUrl = setbackServiceUrl;
  this.rackingConfigurationServiceUrl = rackingConfigurationServiceUrl;
  this.shadowsServiceUrl = shadowsServiceUrl;
  this.roofGeometryBlocked = true;

  this.bObjectTypeChanging = false;

  this.containerId = containerId;
  this.workspaceId = workspaceId;
  this.center = {
    lat: 37.59468726568874,
    lng: -122.4132813597098
  };

  // Creating placeholder div for google maps and for panel
  this.panel = initGUI(this, containerId, true);
  this.panelId = this.panel.id;
  this.panelVisible = true;
  this.addressBarVisible = true;
  this.currentDrawingManager = null;
  this.snapAzimuth = false;
  this.snapAzimuthListener = null;
  this.snapAzimuthPath = null;

  this.interactionModes = ['SELECTION', 'DRAW_LINE', 'DRAW_CIRCLE', 'PASTE_OBJECTS', 'RECTANGULAR_SELECTION'];
  this.interactionMode = 'SELECTION';

  this.lastIndexes = {
    Roof: 0,
    InstallationArea: 0,
    Obstruction: 0,
    Unclassified: 0,
    Module: 0,
    MapObject: 0,
    Rails: 0,
    Splices: 0,
    Attachments: 0,
    Shadow: 0
  };

  CustomSetBackLabelRefresh = function() {
    var lc = lc_heap[idx];
    distanceLabelsRefresh(lc);
  }

  displayToastrMessage = function(text) {
    var scope = angular.element("[ng-app = PVSketchFront]").scope();
    scope.$apply(function() {
      scope.displayMessage(text);
    });
  }

  this.outsideObstructions = [];

  // Creating google maps object
  var mapObj = initMap("mapContainer", this.center);
  this.map = mapObj;
  this.canvas = new LayoutControlCanvas(this.map);
  this.marker = new google.maps.Marker({
    position: new google.maps.LatLng(this.center.lat, this.center.lng),
    map: this.map,
    visible: this.markerVisible,
    draggable: false,
    clickable: false
  });

  this.blockModuleAzimuthArrowsVisible = false;
  this.moduleAzimuthArrowVisible = false;

  // Creating roof, obstruction and installationarea arrays
  this.objects = [];
  // this.selectedObject = 'undefined';
  this.selectedObjects = [];
  this.selectedModules = [];

  this.highlightedObject = undefined;

  this.modulesVisible = true;
  this.railsVisible = false;

  this.lastClickLatLng = null;

  this.rectSelectionPixelSum = 0;

  mapObj.setZoom(21);
  mapObj.setTilt(0);

  // Track CTRL and shift button press
  var lc = this;
  lc.ctrlPressed = false;
  lc.shiftPressed = false;
  lc.cmdPressed = false;
  lc.keyStop = false;


  window.onkeydown = function(e) {
    lc.ctrlPressed = ((e.key == 'Control') || (e.ctrlKey == true));
    lc.shiftPressed = ((e.key == 'Shift') || (e.shiftKey == true));
    lc.cmdPressed = ((e.keyCode == 224) || (e.keyCode == 91) || (e.keyCode == 93) || (e.keyCode == 17));
  };

  window.onkeyup = function(e) {
    lc.ctrlPressed = false;
    lc.shiftPressed = false;
    lc.cmdPressed = false;
  };

  // cancel drawing if escape button is pressed
  this.escapeKeyUp = google.maps.event.addDomListener(document, 'keyup', function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 27) {
      lc.bStopPressed = true;

      lc.ChangeInteractionMode('SELECTION');
      lc.DeselectObject();
      lc.DeselectModules();
    }
    lc.SetActiveBtn();
  });

  //Google Document Level keydown and keyup event listeners
  //keydown
  this.documentKeyUpEvent = google.maps.event.addDomListener(document, 'keydown', function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);

    //Ctrl C for copy
    if ((lc.cmdPressed || lc.ctrlPressed) && code === 67) {
      var onCopyObjects = {
        type: "onCopyObjects",
        objects: lc.selectedObjects
      };
      lc.dispatchEvent(onCopyObjects);
    }

    //Ctrl V for paste
    if ((lc.cmdPressed || lc.ctrlPressed) && code === 86) {
      var onPasteObjects = {
        type: "onPasteObjects",
        objects: lc.objectsClipboard
      };
      lc.dispatchEvent(onPasteObjects);
    }

    //Ctrl M for Rectangular Selection
    if ((lc.cmdPressed || lc.ctrlPressed) && code === 77) {
      lc.ChangeInteractionMode('RECTANGULAR_SELECTION', true);
    }
    //Ctrl L for Line
    if ((lc.cmdPressed || lc.ctrlPressed) && code === 76) {
      lc.ChangeInteractionMode('DRAW_LINE');
    }

    //BH Shift to enable rect selection
    // if (lc.shiftPressed && code === 16 && !lc.keyStop) {
    //   //lc.bStopPressed = true;
    //       switch (lc.interactionMode) {
    //         case lc.interactionModes[0]:
    //           e.preventDefault();
    //           if (lc.currentDrawingManager != null)
    //           {
    //             lc.currentDrawingManager.setDrawingMode(null);
    //           }
    //           lc.keyStop = true;
    //           lc.ChangeInteractionMode('RECTANGULAR_SELECTION');
    //           // lc.InitRectangularSelection();
    //           //lc.shiftPressed = false;
    //           break;
    //       }
    // }

    // CTRL Z for Undo
    /*if ((lc.cmdPressed || lc.ctrlPressed) && code === 90) {
      var scope = angular.element("[ng-app = PVSketchFront]").scope();
      var lastOverlay = scope.$parent.drawingEvents.length > 0 ? scope.$parent.drawingEvents[scope.$parent.drawingEvents.length - 1] : null;
      lc.UndoObject(lastOverlay);
    }*/

    // CTRL + arrow keys
    if ((lc.cmdPressed || lc.ctrlPressed) && (code >= 37 && code <= 40)) {
      if (lc.selectedModules.length > 0) {
        var moduleReferences = [];
        var direction = "";

        //get row & column spacing
        var getRowColumnSpacing = {
          type: "getRowColumnSpacing"
        };
        lc.dispatchEvent(getRowColumnSpacing);
        //var hasModules = false;
        //Once the neighbours are populated for newly added module this condition can be used for module detection

        switch (code) {
          case 37:
            direction = "left";
            //hasModules = lc.selectedModules[0].neighbours[7] !== null;
            break;
          case 38:
            direction = "up";
            //hasModules = lc.selectedModules[0].neighbours[1] !== null;
            break;
          case 39:
            direction = "right";
            //hasModules = lc.selectedModules[0].neighbours[4] !== null;
            break;
          case 40:
            direction = "down";
            //hasModules = lc.selectedModules[0].neighbours[6] !== null;
            break;
        }

        lc.selectedModules.forEach(function(module) {
          moduleReferences.push({
            type: module.type,
            azimuth: module.azimuth,
            index: module.index,
            groupIndex: module.groupIndex,
            isOpposite: module.isOpposite,
            rowIndex: module.rowIndex,
            neighbours: [null, null, null, null, null, null, null, null],
            data: module.data,
            polygon2dLocal: GetPolygon2D(module.primitive)
          });
        });

        var onAddSingleModule = {
          type: "onAddSingleModule",
          modules: moduleReferences
        };
        lc.dispatchEvent(onAddSingleModule);

        lc.AddMoreModules(direction, moduleReferences);
      }
    }
  });

  //BH Key up Shift to enable rect selection
  // this.documentKeyUpEvent = google.maps.event.addDomListener(document, 'keyup', function (e) {
  //   var code = (e.keyCode ? e.keyCode : e.which);
  //   if (lc.shiftPressed) {
  //     lc.keyStop = false;
  //         switch (lc.interactionMode) {
  //           case lc.interactionModes[4]:
  //             if (lc.currentDrawingManager != null)
  //             {
  //               lc.currentDrawingManager.setDrawingMode(null);
  //               $('#' + lc.containerId).off('mousemove.drawing');
  //               lc.RefreshDrawDistanceLabel();
  //             }
  //             lc.ChangeInteractionMode('SELECTION');
  //             break;
  //         }
  //   }
  // });

  // TODO: Move this code to AddListeners method
  this.map.addListener("click", function(e) {
    var lc = lc_heap[idx];

    switch (lc.interactionMode) {
      // PASTE_OBJECTS
      case lc.interactionModes[3]:
        lc.PasteObjectsFromClipboard(e.latLng);
        break;
    }

    setTimeout(function() {
      lc.DeselectObject();
      if (lc.interactionMode == 'SELECTION') {
        lc.DeselectModules();
      }
    });

  });

  var distanceLabelsRefresh = function(lc, bHide) {
    lc.GetRoofs().forEach(function(roof) {
      roof.RefreshDistanceLabels(lc, bHide);
    });
  };

  this.mapDragging = false;

  lc.mapLoaded = false;

  // events for distance labels
  google.maps.event.addListenerOnce(this.map, 'bounds_changed', function() {
    var lc = lc_heap[idx];
    if (!lc.mapLoaded)
      lc.mapLoaded = true;
    if (!lc.mapDragging)
      distanceLabelsRefresh(lc);
  });
  google.maps.event.addListener(this.map, 'zoom_changed', function() {
    var lc = lc_heap[idx];
    if (!lc.mapDragging && lc.mapLoaded)
      distanceLabelsRefresh(lc);
  });
  google.maps.event.addListener(this.map, 'center_changed', function() {
    var lc = lc_heap[idx];
    if (!lc.mapDragging && lc.mapLoaded)
      distanceLabelsRefresh(lc);
  });
  this.map.addListener('dragstart', function() {
    var lc = lc_heap[idx];
    lc.mapDragging = true;
    distanceLabelsRefresh(lc, true);
  });
  this.map.addListener('dragend', function() {
    var lc = lc_heap[idx];
    lc.mapDragging = false;
    distanceLabelsRefresh(lc);
  });

  function zoomChanged() {
    if (mapObj.getZoom() >= 21 && lc.modulesVisible) {
      lc.ShowModuleAzimuthArrows();
    } else {
      lc.HideModuleAzimuthArrows();
    }
  }

  mapObj.addListener('zoom_changed', zoomChanged);

  lc_heap.push(this);

  this.UpdateForMeasurementSystem(this.measurementSystem);
  this.UpdateObjectSelection();

  //console.log("Instance of LayoutControl");

  lc.mousedownPixelSum = 0;

  $('#' + containerId).mouseup(function(e) {
    if ((e.offsetX + e.offsetY) != lc.mousedownPixelSum)
      return;

    if (lc.mapLoaded)
      lc.lastClickLatLng = pixelToLatlng(e.offsetX, e.offsetY, lc.map);
  });


  //Add click event on map
  $('#' + containerId).mousedown(function(e) {
    //Deselect object on start drawing
    lc.mousedownPixelSum = e.offsetX + e.offsetY;
    if (lc.currentDrawingManager && lc.currentDrawingManager.drawingMode) {
      lc.DeselectObject();
      lc.DeselectModules();
    }
  });

  $(window).keydown(function(e) {
    if (((e.key === 'Shift') || (e.shiftKey === true)) && lc.interactionMode === lc.interactionModes[0] && ((lc.selectedModules && lc.selectedModules.length > 0) || (lc.selectedObjects && lc.selectedObjects.length > 0 && lc.selectedObjects[0].type === 'Obstruction'))) {
      $('#designSelectBtn').addClass('snapping');
    }
  });

  $(window).keyup(function() {
    $('#designSelectBtn').removeClass('snapping');
  });

  this.setLayoutMinRoofArea = function(minArea) {
    layoutMinRoofArea = minArea;
  };
};

// Roof prototype functions

// Obstruction prototype functions

/**
 * Adds Event listeners related to obstruction object on map
 * Parameters: primitive object from map; LayoutControl object handler
 *
 */
Roof.prototype.AddChangeEventListeners = function(primitive, lc) {
  // add listeners for shape change
  var roof = this;

  var changedFn = function(lc, roof, primitive) {
    if (primitive.dragging)
      return;

    lc.PrimitiveChanged(primitive);
    //var roofsToCheck = lc.GetRoofsWithSameHeight(roof);
    roof.roofFaces[0].polygon2dLocal = GetPolygon2D(primitive);
    roof.RefreshArrowPosition();

    if (!primitive.dragging)
      roof.DrawGeometry(lc);

    roof.RefreshDistanceLabels(lc);
    roof.leastHorizontalDistance = 0;

    lc.setObjectZIndex(lc.objects, roof, 'roof');
    lc.RefreshProjectMinLinesDistances();
  };

  switch (primitive.shapeType) {
    case "polygon":
      google.maps.event.addListener(primitive.getPath(), 'set_at', function() {
        changedFn(lc, roof, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'insert_at', function() {
        changedFn(lc, roof, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'remove_at', function() {
        changedFn(lc, roof, primitive);
      });
      break;
    case "circle":
      google.maps.event.addListener(primitive, 'radius_changed', function() {
        changedFn(lc, roof, primitive);
      });
      google.maps.event.addListener(primitive, 'center_changed', function() {
        changedFn(lc, roof, primitive);
      });
      break;
  }

  google.maps.event.addListener(primitive, 'mouseover', function() {
    primitive.hover = true;
    if (!primitive.dragging)
      roof.RefreshDistanceLabels(lc);
  });

  google.maps.event.addListener(primitive, 'mouseout', function() {
    primitive.hover = false;
    if (!primitive.dragging)
      roof.RefreshDistanceLabels(lc);
  });
};

Roof.prototype.GetAzimuthArrow = function(lc, primitive, azimuth) {
  return new google.maps.Marker({
    position: GetCentroid(GetPolygon2D(primitive).vertices),
    map: lc.map,
    draggable: false,
    clickable: false,
    icon: GetAzimuthArrowIcon(azimuth)
  });
};

Roof.prototype.ChangeArrowHeading = function(newHeading) {
  this.azimuthArrow.setIcon(GetAzimuthArrowIcon(newHeading));
};

Roof.prototype.RefreshArrowPosition = function() {
  this.azimuthArrow.setPosition(GetCentroid(this.roofFaces[0].polygon2dLocal.vertices));
};

Roof.prototype.DeleteInstallationAreaByIndex = function(index) {
  this.installationAreas.splice(this.FindInstallationAreaLocalIndex(index), 1);
};

Roof.prototype.ClearInstallationAreasPrimitives = function(lc) {
  var roof = this;
  roof.installationAreas.forEach(function(installationArea) {
    lc.DeleteObjectByDataInstance(installationArea);
    installationArea.primitive.setMap(null);
  });
};

Roof.prototype.FindInstallationAreaLocalIndex = function(index) {
  var foundInstallationAreaLocalIndex = null;
  this.installationAreas.forEach(function(installationArea, localIndex) {
    if (foundInstallationAreaLocalIndex != null) {
      return;
    }
    if (installationArea.index == index) {
      foundInstallationAreaLocalIndex = localIndex;
    }
  });
  return foundInstallationAreaLocalIndex;
};

Roof.prototype.DeleteModuleByIndex = function(module) {

  if (module) {
    module.primitive.setMap(null);
    module.azimuthArrow.setMap(null);

    var modulesCount = this.modules.length;
    for (var i = 0; i < modulesCount; i++) {
      if (this.modules[i].index === module.index) {
        this.modules.splice(i, 1);
        break;
      }
    }
  }
};

Roof.prototype.DeleteModuleFromNeighbours = function(index, callback) {
  if (index >= 0) {
    this.modules.forEach(function(mod, indexM, modules) {
      if (modules[indexM].neighbours) {
        modules[indexM].neighbours.forEach(function(neigh, indexN, neighbours) {
          if (neighbours[indexN] == index) {
            neighbours[indexN] = null;
          }
        });
      }
    });

    if (callback) callback();
  }
};

Roof.prototype.FindModuleLocalIndex = function(index) {
  var foundModuleLocalIndex = null;
  this.modules.forEach(function(module, localIndex) {
    if (foundModuleLocalIndex != null) {
      return;
    }
    if (module.index == index) {
      foundModuleLocalIndex = localIndex;
    }
  });
  return foundModuleLocalIndex;
};

Roof.prototype.DeleteObstructionByIndex = function(index) {
  this.obstructions.splice(this.FindObstructionLocalIndex(index), 1);
};

Roof.prototype.ClearObstructionsPrimitives = function(lc) {
  var roof = this;
  roof.obstructions.forEach(function(obstruction) {
    lc.DeleteObjectByDataInstance(obstruction);
    obstruction.primitive.setMap(null);
  });
};

Roof.prototype.ClearObstructions = function(lc) {
  var roof = this;
  roof.obstructions.forEach(function(obstruction, index) {
    lc.DeleteObjectByDataInstance(obstruction);
    obstruction.primitive.setMap(null);
  });
  roof.obstructions = [];
};

Roof.prototype.FindObstructionLocalIndex = function(index) {
  var foundObstructionLocalIndex = null;
  this.obstructions.forEach(function(obstruction, localIndex) {
    if (foundObstructionLocalIndex != null) {
      return;
    }
    if (obstruction.index == index) {
      foundObstructionLocalIndex = localIndex;
    }
  });
  return foundObstructionLocalIndex;
};

Roof.prototype.setMeanHeight = function(lc, newMeanHeight) {
  var bufferHeight = this.meanHeight;
  this.meanHeight = newMeanHeight;
  var roofs = lc.GetPolygon2DListFromObjectList(lc.GetRoofsWithSameHeight(this));
  var bOverlap = lc.IsPolygonOverlapOtherPolygons(this.roofFaces[0].polygon2dLocal, roofs);
  if (bOverlap) {
    this.meanHeight = bufferHeight;
    return false;
  }
  return true;
};

Roof.prototype.AddModule = function(index, points, groupIndex, isOpposite, rowIndex, neighbours, lc) {
  var polygon2d = points;

  if (!polygon2d instanceof Polygon2D || !(polygon2d && polygon2d.vertices instanceof Array))
    polygon2d = GetPolygon2DFromPoints(points);

  // --- FOR PIXIJS
  // var path = [];
  // var vertices = polygon2d.vertices.length;
  // for (var i = 0; i < vertices; i++)
  // {
  //   path.push(new google.maps.LatLng(polygon2d.vertices[i].y, polygon2d.vertices[i].x));
  // }
  // lc.canvas.createPolygon({
  //   path: path,
  //   strokeColor: '#888888',
  //   fillColor: '#000088',
  //   fillOpacity: 0.5,
  //   strokeOpacity: 0.5,
  //   strokeWeight: 1
  // });

  // --- NATIVE GOOGLE MAPS

  var fillColor = "";

  switch (lc.moduleColor) {
    case "blue":
      fillColor = "#000088";
      break;
    case "black":
      fillColor = "#000000";
      break;
    default:
      fillColor = "#000088";
      break;
  }

  var modulePrimitive = lc.AddPrimitive(polygon2d, "#888888", fillColor, layoutControlElementZIndexes.Module, 0.5, 0.5, 1);

  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;

  //var moduleIdx = lc.lastIndexes.Module = lc.lastIndexes.Module + 1;

  //Getting index from LayoutService response
  var moduleIdx = index;

  modulePrimitive.setVisible(lc.modulesVisible);

  var module = new LayoutModule(lc, moduleIdx, modulePrimitive, this.roofFaces[0].azimuth, groupIndex, isOpposite, rowIndex, neighbours);
  module.type = 'Module';

  var ModuleData = {
    objectType: module.type,
    index: idx,
    parentRoof: this.index
  };

  module.data = ModuleData;
  module.polygon2dLocal = polygon2d;

  google.maps.event.addListener(module.primitive, 'click', function() {
    lc.ModuleSelected(module);
  });

  google.maps.event.addListener(module.primitive, 'drag', function() {
    lc.ModuleDraged(module);
  });

  google.maps.event.addListener(module.primitive, 'dragstart', function() {
    lc.ModuleDragStart(module);
  });

  google.maps.event.addListener(module.primitive, 'dragend', function() {
    lc.ModuleDragEnd(module);
  });

  var roof = this;

  google.maps.event.addListener(module.primitive, 'mouseover', function() {
    roof.primitive.hover = true;
    roof.RefreshDistanceLabels(lc);
  });

  google.maps.event.addListener(module.primitive, 'mouseout', function() {
    roof.primitive.hover = false;
    roof.RefreshDistanceLabels(lc);
  });

  this.modules.push(module);

  var onModuleCountChanged = {
    type: "onModuleCountChanged",
    objects: lc.GetProjectModuleCount()
  };
  lc.dispatchEvent(onModuleCountChanged);

  return module;
};

Roof.prototype.ClearModules = function(lc) {
  var modulesCount = this.modules.length;
  for (var i = 0; i < modulesCount; i++) {
    this.modules[i].primitive.setMap(null);
    if (this.modules[i].azimuthArrow)
      this.modules[i].azimuthArrow.setMap(null);
  }
  this.modules.length = 0;
  lc.lastIndexes.Module = 0;
  var onModuleCountChanged = {
    type: "onModuleCountChanged",
    objects: lc.GetProjectModuleCount()
  };
  lc.dispatchEvent(onModuleCountChanged);

  this.ClearRails(lc);
};

Roof.prototype.AddSplice = function(lc, splice) {

  if (splice === 'undefined') {
    return;
  }

  var point = [
    new google.maps.LatLng(splice.y, splice.x),
    new google.maps.LatLng(splice.y, splice.x)
  ];
  var splicePrimitive = new google.maps.Polyline({
    path: point,
    geodesic: true,
    strokeColor: layoutControlElementColors.Splices,
    strokeOpacity: 1,
    strokeWeight: 7,
    zIndex: 15010,
    map: lc.map
  });

  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;

  var spliceIdx = lc.lastIndexes.Splices = lc.lastIndexes.Splices + 1;

  var splice = new LayoutSplice(spliceIdx, splicePrimitive, this.index);
  splice.type = 'Splice';

  splicePrimitive.setVisible(lc.railsVisible);

  this.splices.push(splicePrimitive);

  return splice;
};

Roof.prototype.ClearSplices = function(lc) {

  if (this.splices == undefined) {
    this.splices = [];
  }

  var splicesCount = this.splices.length;
  for (var i = 0; i < splicesCount; i++) {
    this.splices[i].setMap(null);
  }
  this.splices.length = 0;
  lc.lastIndexes.Splices = 0;
};

Roof.prototype.AddRail = function(lc, rail) {

  if (rail === 'undefined') {
    return;
  }

  var line = [
    new google.maps.LatLng(rail.StartPoint.y, rail.StartPoint.x),
    new google.maps.LatLng(rail.EndPoint.y, rail.EndPoint.x)
  ];
  var railPrimitive = new google.maps.Polyline({
    path: line,
    geodesic: true,
    strokeColor: layoutControlElementColors.Rails,
    strokeOpacity: 1,
    strokeWeight: 2,
    zIndex: 15000,
    map: lc.map
  });


  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;

  var railIdx = lc.lastIndexes.Rail = lc.lastIndexes.Rail + 1;

  var rail = new LayoutRail(railIdx, railPrimitive, this.index);
  rail.type = 'Module';

  railPrimitive.setVisible(lc.railsVisible);

  this.rails.push(railPrimitive);

  return rail;
};

Roof.prototype.ClearRails = function(lc) {

  if (this.rails == undefined) {
    this.rails = [];
  }

  var railsCount = this.rails.length;
  for (var i = 0; i < railsCount; i++) {
    this.rails[i].setMap(null);
  }
  this.rails.length = 0;
  lc.lastIndexes.Rails = 0;

  this.ClearSplices(lc);
  this.ClearAttachmets(lc);
};

Roof.prototype.AddAttachment = function(lc, attachment) {

  if (attachment === 'undefined') {
    return;
  }

  var point = [
    new google.maps.LatLng(attachment.y, attachment.x),
    new google.maps.LatLng(attachment.y, attachment.x)
  ];
  var attachmentPrimitive = new google.maps.Polyline({
    path: point,
    geodesic: true,
    strokeColor: layoutControlElementColors.Attachments,
    strokeOpacity: 1,
    strokeWeight: 7,
    zIndex: 15010,
    map: lc.map
  });

  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;

  var attachmentIdx = lc.lastIndexes.Attachments = lc.lastIndexes.Attachments + 1;

  var attachment = new LayoutSplice(attachmentIdx, attachmentPrimitive, this.index);
  attachment.type = 'Attachments';

  attachmentPrimitive.setVisible(lc.railsVisible);

  if (this.attachments == undefined) {
    this.attachments = [];
  }
  this.attachments.push(attachmentPrimitive);

  return attachment;
};

Roof.prototype.ClearAttachmets = function(lc) {

  if (this.attachments == undefined) {
    this.attachments = [];
  }

  var attachmentsCount = this.attachments.length;
  for (var i = 0; i < attachmentsCount; i++) {
    this.attachments[i].setMap(null);
  }
  this.attachments.length = 0;
  lc.lastIndexes.Attachments = 0;
};

/**
 * Adds Event listeners related to obstruction object on map
 * Parameters: primitive object from map; LayoutControl object handler
 *
 */
Obstruction.prototype.AddChangeEventListeners = function(primitive, lc) {
  // add listeners for shape change
  var obstruction = this;
  switch (primitive.shapeType) {
    case "polygon":
      google.maps.event.addListener(primitive.getPath(), 'set_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'insert_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive.getPath(), 'remove_at', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      break;
    case "rectangle":
      google.maps.event.addListener(primitive, 'bounds_changed', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      break;
    case "circle":
      google.maps.event.addListener(primitive, 'radius_changed', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      google.maps.event.addListener(primitive, 'center_changed', function() {
        if (primitive.dragging)
          return;

        lc.PrimitiveChanged(primitive);
        obstruction.ChangeShape(lc, primitive);
      });
      break;
  }
};

Obstruction.prototype.ChangeShape = function(lc, primitive) {
  var obstruction = this;
  // var undo = lc.UndoIfOnTopOfOtherObject(GetPolygon2D(primitive), lc.GetObstructionsExcludeByIndex(obstruction.index));
  var undo = false;
  if (!undo) {
    var parentRoof = lc.IsPolygonOnRoof(GetPolygon2D(primitive));
    if (parentRoof) {
      if (obstruction.parentRoof == false) {
        lc.outsideObstructions.splice(lc.outsideObstructions.indexOf(obstruction), 1);
      } else {
        obstruction.parentRoof.obstructions.splice(obstruction.parentRoof.obstructions.indexOf(obstruction), 1);
      }
      parentRoof.obstructions.push(obstruction);
      obstruction.parentRoof = parentRoof;
    } else {
      if (obstruction.parentRoof) {
        obstruction.parentRoof.obstructions.splice(obstruction.parentRoof.obstructions.indexOf(obstruction), 1);
        obstruction.parentRoof = false;
        lc.outsideObstructions.push(obstruction);
      }

    }
    obstruction.polygon2dLocal = GetPolygon2D(primitive);
  }
};

Obstruction.prototype.GetPolygonLines = function() {
  var points = this.polygon2dLocal.vertices;

  var lines = [];
  var pointsLength = points.length;
  for (var i = 0; i < pointsLength; i++) {
    var p1 = i;
    var p2 = i + 1;
    if (p2 == pointsLength)
      p2 = 0;

    lines.push({
      p1: new google.maps.LatLng(points[p1].y, points[p1].x),
      p2: new google.maps.LatLng(points[p2].y, points[p2].x)
    });
  }
  return lines;
};

// InstallationArea prototype functions

/**
 * Adds event listeners related to installation area object on map
 * Parameters: primitive object from map
 *
 */
InstallationArea.prototype.AddChangeEventListeners = function(primitive, lc) {
  // add listeners for shape change
  var installationArea = this;
  google.maps.event.addListener(primitive.getPath(), 'set_at', function() {
    lc.PrimitiveChanged(primitive);
    var undo1 = UndoIfOutsideRoof(primitive, installationArea.parentRoof.primitive);
    // var undo2 = lc.UndoIfOnTopOfOtherObject(GetPolygon2D(primitive), lc.GetInstallationAreasExcludeByIndex(installationArea.index));
    var undo2 = false;
    if (!undo1 || !undo2) {
      installationArea.polygon2dLocal = GetPolygon2D(primitive);
    }
  });
  google.maps.event.addListener(primitive.getPath(), 'insert_at', function() {
    lc.PrimitiveChanged(primitive);
    var undo1 = UndoIfOutsideRoof(primitive, installationArea.parentRoof.primitive);
    // var undo2 = lc.UndoIfOnTopOfOtherObject(GetPolygon2D(primitive), lc.GetInstallationAreasExcludeByIndex(installationArea.index));
    var undo2 = false;
    if (!undo1 || !undo2) {
      installationArea.polygon2dLocal = GetPolygon2D(primitive);
    }
  });
};

// Module prototype function

/**
 * Fills module(makes it's map object fill color and outline color black
 */
LayoutModule.prototype.FillModule = function(lc) {
  var primitiveBuffer = clone(this.primitive);
  this.primitive.setMap(null);
  this.primitive = lc.AddPrimitive(GetPolygon2D(primitiveBuffer), "#FFFFFF", "#AAAAAA", layoutControlElementZIndexes.Module, 0.5);
};

/**
 * Unfills module(makes it's map object fill color transparent and outline color black with 50% transparency
 */
LayoutModule.prototype.UnfillModule = function(lc) {
  var primitiveBuffer = clone(this.primitive);
  this.primitive.setMap(null);
  this.primitive = lc.AddPrimitive(GetPolygon2D(primitiveBuffer), "#888888", "#000000", layoutControlElementZIndexes.Module, 0.5);
};

// LayoutControl functions

/**
 * Sets map center for specified latitude and longitude
 *
 */
LayoutControl.prototype.SetCenter = function(lat, lng) {
  this.map.setCenter(new google.maps.LatLng(lat, lng));
  this.center = {
    lat: lat,
    lng: lng
  };
  //this.SetMaxZoomAtCurrentLocation();
  this.RefreshMarker();
}

/**
 * Sets maps center for specified address
 *
 */
LayoutControl.prototype.SetCenterByAddress = function(address) {
  var places = new google.maps.places.PlacesService(this.map);
  var LayoutControlHandler = this;
  places.textSearch({
    query: address
  }, function(results, status) {
    if (status === 'OK')
      LayoutControlHandler.SetCenter(results[0].geometry.location.lat(), results[0].geometry.location.lng());
  });
}

LayoutControl.prototype.SetMaxZoomAtCurrentLocation = function() {
  var maxZoomService = new google.maps.MaxZoomService();
  var latLngCenter = new google.maps.LatLng(this.center.lat, this.center.lng);
  var lc = this;
  lc.map.setOptions({
    numZoomLevels: 23,
    MAX_ZOOM_LEVEL: 22
  });
  maxZoomService.getMaxZoomAtLatLng(latLngCenter, function(response) {
    if (response.status === google.maps.MaxZoomStatus.OK) {
      google.maps.event.addListenerOnce(lc.map, 'idle', function() {
        lc.map.setZoom(response.zoom);
      });
    }
  });
};

/**
 * Toggle tool panel visibility on/off
 *
 */
LayoutControl.prototype.SetPanelVisible = function(bPanelVisible) {
  this.panelVisible = bPanelVisible;
  $("#" + this.panelId).css("display", this.panelVisible ? "" : "NONE");
};

/**
 * Toggle adress bar visibility on/off
 *
 */
LayoutControl.prototype.SetAddressBarVisible = function(bAddressBarVisible) {
  this.addressBarVisible = bAddressBarVisible;
  $("#" + this.panelId + " .layoutToolPanel_addressBar").css("display", this.addressBarVisible ? "block" : "none");
};

LayoutControl.prototype.ModuleDraged = function(module) {
  var lc = this;
  module.RefreshArrowPosition();

  var selectedModules = this.selectedModules;

  var onModuleDraged = {
    type: "onModuleDraged",
    dragedModules: selectedModules
  };
  this.dispatchEvent(onModuleDraged);

  if (lc.shiftPressed) {
    var threshold = 0.5;
    var polygonPoints = getPolygonPoints(lc, threshold).concat(getModulePoints(lc, module));
    var modulePoints = module.primitive.getPath().getArray();
    var modulePointsAndClosest = getModulePointsAndClosestPoint(modulePoints, polygonPoints, threshold);

    var closestDistance = modulePointsAndClosest[0] ? modulePointsAndClosest[0].distance : null;
    var closestVertex = null;
    var closestPointIndex = null;
    modulePointsAndClosest.forEach(function(point) {
      if (point.distance <= closestDistance) {
        closestDistance = point.distance;
        closestVertex = point.closeBy;
        closestPointIndex = point.index;
      }
    });

    if (closestVertex) {
      snapPolygon(modulePoints, closestPointIndex, closestVertex);
    }
  }

  if (selectedModules.length <= 1) {
    return;
  }

  var selectedModulesLength = selectedModules.length;
  var deltas = module.primitive.dragDeltas;
  for (var i = 0; i < selectedModulesLength; i++) {
    var selectedModule = selectedModules[i];

    if (selectedModule.index === module.index) {
      continue;
    }

    var parentPaths = module.primitive.getPath().getArray(),
      checkingPaths = selectedModule.primitive.getPath().getArray(),
      parentPathsLength = parentPaths.length,
      checkingPathsLength = checkingPaths.length;

    if (parentPathsLength !== checkingPathsLength) {
      continue;
    }

    for (var k = 0; k < parentPathsLength; k++) {
      var parentPath = parentPaths[k];

      var newLat = parentPath.lat() - deltas[selectedModule.index][k].lat,
        newLng = parentPath.lng() - deltas[selectedModule.index][k].lng;

      checkingPaths[k] = new google.maps.LatLng(newLat, newLng);
    }

    selectedModule.primitive.setPaths(checkingPaths);
    selectedModule.RefreshArrowPosition();
  }
};

/**
 * Returns an array of all the points of all roofs and obstructions on the map.
 * These points will be padded with the intermediate points between each polygon's vertexes.
 *
 * @param lc - LayoutControl aka this
 * @param threshold - The snapping threshold
 * @param index - The index of the object being snapped, used to filter out it's own points
 * @returns {*[]} - The array of all vertexes and padded points on the map
 */

function getPolygonPoints(lc, threshold, index) {
  // Get all vertexes and intermediate points for all roofs and obstructions on the map
  var polygonPoints = lc.objects
    .filter(function(obj) {
      return ('Roof' === obj.type || 'Obstruction' === obj.type) && obj.index !== index
    })
    .map(function(obj) {
      return padPolygonPoints(obj.primitive.getPath().getArray(), threshold);
    })
    .reduce(function(a, b) {
      return a.concat(b)
    }, []);

  // Get all vertexes and intermediate points for all roof setbacks on the map
  var setbackPoints = lc.objects
    .filter(function(obj) {
      return 'Roof' === obj.type && obj.data.setbackPolygon && obj.data.setbackPolygon[0]
    })
    .map(function(obj) {
      return padPolygonPoints(obj.data.setbackPolygon[0].getPath().getArray(), threshold);
    })
    .reduce(function(a, b) {
      return a.concat(b)
    }, []);

  polygonPoints = polygonPoints.concat(setbackPoints);

  return polygonPoints;
}

/**
 * Returns a list of all module points within 2 meters of the module being snapped
 *
 * @param lc - The layoutControl instance
 * @param module - The module being snapped
 * @returns [] - A list of all module points within 2 meters of the module being snapped
 */

function getModulePoints(lc, module) {
  // Filter roofs with modules
  return lc.objects.filter(function(object) {
    return 'Roof' === object.type && object.data.modules && object.data.modules.length > 0
  })
    .map(function(roof) {
      return roof.data.modules
      // We want to exclude the module being snapped
        .filter(function(roofModule) {
          return module !== roofModule
        })
        .map(function(roofModule) {
          // We only want points witing 2 meters of the module being snapped
          return roofModule.primitive.getPath().getArray().filter(
            function(point) {
              return google.maps.geometry.spherical.computeDistanceBetween(point, module.primitive.getPath().getArray()[0]) < 2
            })
        })
        .reduce(function(a, b) {
          return a.concat(b)
        }, [])
    })
    .reduce(function(a, b) {
      return a.concat(b)
    }, [])
}

/**
 * Pads the points of a polygon with intermediate points that are <threshold> apart from eachother
 *
 * @param polygonPoints - The array of the polygon's points
 * @param threshold - how far apart the padded points should be
 * @returns {[]} - an array with the polygon's points and the padded points between them
 */

function padPolygonPoints(polygonPoints, threshold) {
  var paddedPoints = [];
  for (var pointIndex = 0; pointIndex < polygonPoints.length; pointIndex++) {
    var initialPoint = polygonPoints[pointIndex];
    // Google's definition of a polygon is a series of points where the first and last point are the same
    // but the array of points of a polygon doesn't loop that way, however we need to calculate intermediate points
    // for all the lines of the polygon
    var nextPoint = pointIndex === polygonPoints.length - 1 ? polygonPoints[0] : polygonPoints[pointIndex + 1];

    // How often do we want to fit a point between the polygon vertexes (should be between 0 and 1)
    // The lower this value the higher the resolution of points
    var interpolationInterval = threshold / google.maps.geometry.spherical.computeDistanceBetween(initialPoint, nextPoint);
    var nPoints = Math.floor(google.maps.geometry.spherical.computeDistanceBetween(initialPoint, nextPoint) / threshold);

    paddedPoints.push(initialPoint);

    for (var interpolationIndex = 1; interpolationIndex < nPoints; interpolationIndex++) {
      // Google can calculate any point between any two other points
      // the third value is the relative distance between the new point and the other two points
      // the closer to 0 that value is the closer the resulting point will be to the first point
      // the closer to 1 that value is the closer the resulting point will be to the second point
      paddedPoints.push(google.maps.geometry.spherical.interpolate(initialPoint, nextPoint, interpolationInterval * interpolationIndex));
    }
  }
  return paddedPoints;
}

/**
 * Calculates the closest point of all polygon points on the map to each of the module's points.
 *
 * @param modulePoints - The array of the module's points
 * @param polygonPoints - The array of all polygon points on the map
 * @param threshold - The snapping threshold
 * @returns {[{closeBy, index, distance}]} - An array of objects representing the closest point to each of the module's points and their distance
 */

function getModulePointsAndClosestPoint(modulePoints, polygonPoints, threshold) {
  var modulePointsandClosest = [];
  modulePoints.forEach(function(modulePoint, index) {
    // Filter only points within the threshold distance
    var closeBy = polygonPoints.filter(function(point) {
      return google.maps.geometry.spherical.computeDistanceBetween(modulePoint, point) < threshold
    })[0] || null;

    if (closeBy) {
      modulePointsandClosest.push({
        // The closest polygon point
        closeBy,
        // The module point index
        index,
        // the distance between the polygon point and the module point
        distance: google.maps.geometry.spherical.computeDistanceBetween(modulePoint, closeBy)
      });
    }
  });

  return modulePointsandClosest;
}

/**
 * Snaps a polygon by it's closest vertex to the closest point.
 *
 * @param primitivePoints - The points of the polygon being snapped
 * @param closestPointIndex - The index of the polygon point that will be the basis for snapping
 * @param closestPoint - The point the polygon will be snapped to
 */

function snapPolygon(primitivePoints, closestPointIndex, closestPoint) {
  var closestPolygonPoint = primitivePoints[closestPointIndex];
  var delta = {
    lat: closestPolygonPoint.lat() - closestPoint.lat(),
    lng: closestPolygonPoint.lng() - closestPoint.lng(),
  };

  primitivePoints.forEach(function(point, index) {
    var newLat = point.lat() - delta.lat;
    var newLng = point.lng() - delta.lng;

    primitivePoints[index] = new google.maps.LatLng(newLat, newLng);
  });

  primitivePoints[closestPointIndex] = closestPoint;
}

LayoutControl.prototype.ModuleDragStart = function(module) {
  var lc = this;
  var onModuleDragStart = {
    type: "onModuleDragStart",
    dragedModules: this.selectedModules
  };
  this.dispatchEvent(onModuleDragStart);

  var selectedModules = this.selectedModules;
  if (selectedModules.length <= 1) {
    return;
  }

  var deltas = {};
  var selectedModulesLength = selectedModules.length;
  for (var i = 0; i < selectedModulesLength; i++) {
    selectedModule = selectedModules[i];

    if (selectedModule.index === module.index) {
      continue;
    }

    var parentPaths = module.primitive.getPath().getArray(),
      checkingPaths = selectedModule.primitive.getPath().getArray(),
      parentPathsLength = parentPaths.length,
      checkingPathsLength = checkingPaths.length;

    if (parentPathsLength !== checkingPathsLength) {
      continue;
    }

    deltas[selectedModule.index] = [];

    for (var k = 0; k < parentPathsLength; k++) {
      var parentPath = parentPaths[k],
        checkingPath = checkingPaths[k];

      var delta = {
        lat: parentPath.lat() - checkingPath.lat(),
        lng: parentPath.lng() - checkingPath.lng()
      };

      deltas[selectedModule.index].push(delta);
    }

  }
  module.primitive.dragDeltas = deltas;
};

LayoutControl.prototype.ModuleDragEnd = function(object) {
  var modulesCount = this.selectedModules.length;

  for (var i = 0; i < modulesCount; i++) {
    this.selectedModules[i].polygon2dLocal = GetPolygon2D(this.selectedModules[i].primitive);
  }

  this.UpdateModuleNeighbours();

  var onModuleDragEnd = {
    type: "onModuleDragEnd",
    dragedModules: this.selectedModules
  };
  this.dispatchEvent(onModuleDragEnd);
};

LayoutControl.prototype.UpdateModuleNeighbours = function() {
  var selectedModuleNum = this.selectedModules.length;
  // Iterate through all selected modules
  for (var m = 0; m < selectedModuleNum; m++) {
    var module = this.selectedModules[m];
    // Iterate through module neighbours
    var neighbourNum = module.neighbours.length;
    var selectedModule = null;
    var neighboursToDelete = [];
    for (var n = 0; n < neighbourNum; n++) {
      var bDelete = true;

      // If there's selected module with index of current neighbour - do not delete it
      for (var m2 = 0; m2 < selectedModuleNum; m2++) {
        if (this.selectedModules[m2].index === module.neighbours[n]) {
          bDelete = false;
          break;
        }
      }

      if (bDelete)
        neighboursToDelete.push(module.neighbours[n]);
    }

    // Delete neighbours
    var deleteNeighboursNum = neighboursToDelete.length;
    for (var n2 = 0; n2 < deleteNeighboursNum; n2++) {
      var outerModule = this.FindModuleByIndex(module, neighboursToDelete[n2]);
      if (outerModule) {
        outerModule.neighbours[outerModule.neighbours.indexOf(module.index)] = null;
      }

      module.neighbours[module.neighbours.indexOf(neighboursToDelete[n2])] = null;
    }
  }
};

//Object dragged state

LayoutControl.prototype.ObjectDraged = function(object) {
  var lc = this;

  if (object.data instanceof Roof) {
    object.data.roofFaces[0].polygon2dLocal = GetPolygon2D(object.primitive);
    object.data.RefreshArrowPosition();
  } else if (object.data instanceof Obstruction) {
    object.data.polygon2dLocal = GetPolygon2D(object.primitive);
    //add refresh shadows call
    //lc.GetShadowByObject(object);

    if ('polygon' === object.primitive.shapeType && lc.shiftPressed) {
      var threshold = 0.5;
      var polygonPoints = getPolygonPoints(lc, threshold, object.index);
      var PolygonPointsAndClosest = getModulePointsAndClosestPoint(object.primitive.getPath().getArray(), polygonPoints, threshold);

      var closestDistance = PolygonPointsAndClosest[0] ? PolygonPointsAndClosest[0].distance : null;
      var closestVertex = null;
      var closestPointIndex = null;
      PolygonPointsAndClosest.forEach(function(point) {
        if (point.distance <= closestDistance) {
          closestDistance = point.distance;
          closestVertex = point.closeBy;
          closestPointIndex = point.index;
        }
      });

      if (closestVertex) {
        snapPolygon(object.primitive.getPath().getArray(), closestPointIndex, closestVertex);
      }
    }
  }

  var selectedObjects = this.selectedObjects;

  var onObjectDraged = {
    type: "onObjectDraged",
    dragedObject: object
  };
  this.dispatchEvent(onObjectDraged);

  //Multi object selection of objects

  // if (selectedObjects.length <= 1)
  // {
  //   return;
  // }
  //
  // var selectedObjectsLength = selectedObjects.length;
  // var deltas = object.primitive.dragDeltas;
  // for (var i = 0; i < selectedObjectsLength; i++)
  // {
  //
  //   var selectedObject = selectedObjects[i];
  //
  //   if (selectedObject.index == object.index)
  //   {
  //     continue;
  //   }
  //
  //   if(selectedObject.primitive.shapeType === 'circle'){
  //
  //     console.log("Shape type: " + selectedObject.primitive.shapeType);
  //
  //     var parentPathCir = object.primitive.getCenter(),
  //       checkingPathCir = selectedObject.primitive.getCenter();
  //
  //     var newLat = parentPathCir.lat() - deltas[selectedObject.index][i].lat,
  //       newLng = parentPathCir.lng() - deltas[selectedObject.index][i].lng;
  //
  //     var newPos = new google.maps.LatLng(newLat, newLng);
  //     checkingPathCir = newPos;
  //
  //     selectedObject.primitive.setCenter(checkingPathCir);
  //
  //   } else if(selectedObject.primitive.shapeType === 'polygon') {
  //
  //     console.log("Shape type: " + selectedObject.primitive.shapeType);
  //
  //     var parentPaths = object.primitive.getPath().getArray(),
  //       checkingPaths = selectedObject.primitive.getPath().getArray(),
  //       parentPathsLength = parentPaths.length,
  //       checkingPathsLength = checkingPaths.length;
  //
  //     if (parentPathsLength != checkingPathsLength)
  //     {
  //       continue;
  //     }
  //
  //     for (var k = 0; k < parentPathsLength; k++)
  //     {
  //       var parentPath = parentPaths[k];
  //
  //       var newLat = parentPath.lat() - deltas[selectedObject.index][k].lat,
  //         newLng = parentPath.lng() - deltas[selectedObject.index][k].lng;
  //
  //       var newPos = new google.maps.LatLng(newLat, newLng);
  //       checkingPaths[k] = newPos;
  //     }
  //
  //     selectedObject.primitive.setPaths(checkingPaths);
  //
  //   } else {
  //
  //   }

  // //Prev Multi object selection of Polygons only
  // var parentPaths = object.primitive.getPath().getArray(),
  //   checkingPaths = selectedObject.primitive.getPath().getArray(),
  //   parentPathsLength = parentPaths.length,
  //   checkingPathsLength = checkingPaths.length;
  //
  // if (parentPathsLength != checkingPathsLength)
  // {
  //   continue;
  // }
  //
  // for (var k = 0; k < parentPathsLength; k++)
  // {
  //   var parentPath = parentPaths[k];
  //
  //   var newLat = parentPath.lat() - deltas[selectedObject.index][k].lat,
  //     newLng = parentPath.lng() - deltas[selectedObject.index][k].lng;
  //
  //   var newPos = new google.maps.LatLng(newLat, newLng);
  //   checkingPaths[k] = newPos;
  // }
  //
  // selectedObject.primitive.setPaths(checkingPaths);
  // }
};



// object drag start

LayoutControl.prototype.ObjectDragStart = function(object) {
  // this.map.setOptions({draggableCursor: 'url(https://maps.gstatic.com/mapfiles/closedhand_8_8.cur), move;', draggingCursor: 'url(https://maps.gstatic.com/mapfiles/closedhand_8_8.cur), move;'});
  var $mapCanvas = $('body');
  $mapCanvas.addClass('moving');

  object.primitive.dragging = true;

  if (object.data instanceof Roof) {
    object.data.ClearGeometry(this);
    object.data.RefreshDistanceLabels(this);
  }

  var onObjectDragStart = {
    type: "onObjectDragStart",
    dragedObject: object
  };
  this.dispatchEvent(onObjectDragStart);

  //Multi Object Selection

  // var selectedObjects = this.selectedObjects;
  //
  // var draggedObject = object;
  //
  // if (selectedObjects.length <= 1)
  // {
  //   return;
  // }
  //
  // var deltas = {};
  // var selectedObjectsLength = selectedObjects.length;
  // for (var i = 0; i < selectedObjectsLength; i++)
  // {
  //   var selectedObject = selectedObjects[i];
  //
  //   if (selectedObject.index == object.index)
  //   {
  //     continue;
  //   }
  //
  //   deltas[selectedObject.index] = [];
  //
  //   if(selectedObject.primitive.shapeType === 'circle'){
  //
  //     console.log("Shape type: " + selectedObject.primitive.shapeType);
  //
  //     var parentPathsCir = object.primitive.getCenter(),
  //       checkingPathsCir = selectedObject.primitive.getCenter();
  //
  //     var delta = {
  //       lat: parentPathsCir.lat() - checkingPathsCir.lat(),
  //       lng: parentPathsCir.lng() - checkingPathsCir.lng()
  //     };
  //
  //     deltas[selectedObject.index].push(delta);
  //
  //   }
  //   else {
  //
  //     console.log("Shape type: " + selectedObject.primitive.shapeType);
  //
  //     var parentPaths = object.primitive.getPath().getArray(),
  //       checkingPaths = selectedObject.primitive.getPath().getArray(),
  //       parentPathsLength = parentPaths.length,
  //       checkingPathsLength = checkingPaths.length;
  //
  //     if (parentPathsLength != checkingPathsLength) {
  //       continue;
  //     }
  //
  //     for (var k = 0; k < parentPathsLength; k++) {
  //       var parentPath = parentPaths[k],
  //         checkingPath = checkingPaths[k];
  //
  //       var delta = {
  //         lat: parentPath.lat() - checkingPath.lat(),
  //         lng: parentPath.lng() - checkingPath.lng()
  //       };
  //
  //       deltas[selectedObject.index].push(delta);
  //     }
  //   }
  //
  //   //PREV Multi object selection of Polygons only
  // //  console.log(object);
  // //
  // //   var parentPaths = object.primitive.getPath().getArray(),
  // //     checkingPaths = selectedObject.primitive.getPath().getArray(),
  // //     parentPathsLength = parentPaths.length,
  // //     checkingPathsLength = checkingPaths.length;
  // //
  // //   if (parentPathsLength != checkingPathsLength)
  // //   {
  // //     continue;
  // //   }
  // //
  // //   deltas[selectedObject.index] = [];
  // //
  // //   for (var k = 0; k < parentPathsLength; k++)
  // //   {
  // //     var parentPath = parentPaths[k],
  // //       checkingPath = checkingPaths[k];
  // //
  // //     var delta = {
  // //       lat: parentPath.lat() - checkingPath.lat(),
  // //       lng: parentPath.lng() - checkingPath.lng()
  // //     };
  // //
  // //     deltas[selectedObject.index].push(delta);
  // //   }
  //
  // }
  // object.primitive.dragDeltas = deltas;
};

LayoutControl.prototype.ObjectDragEnd = function(object) {
  this.map.setOptions({
    draggableCursor: undefined,
    draggingCursor: undefined
  });
  var $mapCanvas = $('body');

  $mapCanvas.removeClass('moving');

  if (object.type == 'Obstruction')
    object.data.polygon2dLocal = GetPolygon2D(object.primitive);
  else if (object.type == 'Roof')
    object.data.roofFaces[0].polygon2dLocal = GetPolygon2D(object.primitive);

  object.primitive.dragging = false;

  if (object.data instanceof Roof) {
    object.data.DrawGeometry(this);
    object.data.RefreshDistanceLabels(this);
  } else if (object.data instanceof Obstruction) {
    object.data.ChangeShape(this, object.primitive);
  }

  this.PrimitiveChanged(object.primitive);



  var objectsCount = this.selectedObjects.length;

  for (var i = 0; i < objectsCount; i++) {
    this.selectedObjects[i].polygon2dLocal = GetPolygon2D(this.selectedObjects[i].primitive);

  }

  var onObjectDragEnd = {
    type: "onObjectDragEnd",
    dragedObject: object
  };
  this.dispatchEvent(onObjectDragEnd);
};

LayoutControl.prototype.GetModuleParentRoof = function(module) {
  var foundRoof = null;
  this.GetRoofs().forEach(function(roof) {
    if (foundRoof)
      return;

    var moduleCount = roof.modules.length;
    for (var i = 0; i < moduleCount; i++) {
      if (roof.modules[i] === module) {
        foundRoof = roof;
        break;
      }
    }
  });
  return foundRoof;
};

LayoutControl.prototype.RetrieveSpacing = function(rowSpacing, ColumnSpacing) {
  this.rowSpacing = rowSpacing;
  this.columnSpacing = ColumnSpacing;

};

/**
 * Adds more modules in the given relative direction.
 *
 * @param direction - Direction in witch to add the modules
 * @param modRefs - Original modules to be copied
 * @constructor
 */
LayoutControl.prototype.AddMoreModules = function(direction, modRefs) {
  const lc = this;

  const modules = [];
  const modulesVertices = [];

  const roofsModulesMaxGroupIndex = [];
  const roofsModulesMaxIndex = [];

  modRefs.forEach(function(module) {
    // Store module's original polygon location in case it gets updated unintentionally.
    let modVertices = {
      index: module.index,
      vertices: angular.copy(module.polygon2dLocal.vertices)
    };

    var frozenModVertices = deepFreeze(modVertices);

    if (modulesVertices.indexOf(frozenModVertices) === -1)
      modulesVertices.push(frozenModVertices);

    // Determine max group and individual index for module based on roof it is from.
    var roof = lc.GetRoofByIndex(module.data.parentRoof);
    roof.modules.forEach(function(roofModule) {
      if (!roofsModulesMaxGroupIndex[roofModule.data.parentRoof]) {
        roofsModulesMaxGroupIndex[roofModule.data.parentRoof] = roofModule.groupIndex;
      }

      if (roofsModulesMaxGroupIndex[roofModule.data.parentRoof] < roofModule.groupIndex) {
        roofsModulesMaxGroupIndex[roofModule.data.parentRoof] = roofModule.groupIndex;
      }

      if (!roofsModulesMaxIndex[roofModule.data.parentRoof]) {
        roofsModulesMaxIndex[roofModule.data.parentRoof] = roofModule.index;
      }

      if (roofsModulesMaxIndex[roofModule.data.parentRoof] < roofModule.index) {
        roofsModulesMaxIndex[roofModule.data.parentRoof] = roofModule.index;
      }
    });

    // Get bounding points of selected module
    let topLeftPoint = null;
    let topRightPoint = null;
    let bottomLeftPoint = null;
    let bottomRightPoint = null;

    const vertices = module.polygon2dLocal.vertices;
    vertices.forEach(function(vertex) {
      if (!topLeftPoint) {
        topLeftPoint = {
          x: vertex.x,
          y: vertex.y
        };
      } else {
        if (vertex.x <= topLeftPoint.x) {
          topLeftPoint = {
            x: vertex.x,
            y: vertex.y
          };
        }
      }

      if (!topRightPoint) {
        topRightPoint = {
          x: vertex.x,
          y: vertex.y
        };
      } else {
        if (vertex.x >= topRightPoint.x) {
          topRightPoint = {
            x: vertex.x,
            y: vertex.y
          };
        }
      }

      if (!bottomLeftPoint) {
        bottomLeftPoint = {
          x: vertex.x,
          y: vertex.y
        };
      } else {
        if (vertex.y <= bottomLeftPoint.y) {
          bottomLeftPoint = {
            x: vertex.x,
            y: vertex.y
          };
        }
      }

      if (!bottomRightPoint) {
        bottomRightPoint = {
          x: vertex.x,
          y: vertex.y
        };
      } else {
        if (vertex.y >= bottomRightPoint.y) {
          bottomRightPoint = {
            x: vertex.x,
            y: vertex.y
          };
        }
      }
    });

    // Determine module offset based on direction key input
    let offsetX = null;
    let offsetY = null;

    switch (direction) {
      case "up":
        offsetX = topLeftPoint.x - bottomLeftPoint.x;
        offsetY = topLeftPoint.y - bottomLeftPoint.y;
        break;
      case "left":
        offsetX = topLeftPoint.x - bottomRightPoint.x;
        offsetY = bottomLeftPoint.y - topRightPoint.y;
        break;
      case "right":
        offsetX = bottomRightPoint.x - topLeftPoint.x;
        offsetY = topRightPoint.y - bottomLeftPoint.y;
        break;
      case "down":
        offsetX = bottomLeftPoint.x - topLeftPoint.x;
        offsetY = bottomLeftPoint.y - topLeftPoint.y;
        break;
    }

    //TODO: Need to apply column and row spacing values when computing the new vertices(lc.columnSpacing,lc.rowSpacing)
    // Copy vertices again from original object and modify these. It will be set to
    // new object's polygon vertices
    const modifiedVertices = angular.copy(module.polygon2dLocal.vertices);
    modifiedVertices.forEach(function(vertex) {
      vertex.x += offsetX;
      vertex.y += offsetY;
    });

    // Add new module to map.
    const newModule = angular.copy(module);
    newModule.polygon2dLocal.vertices = modifiedVertices;

    var lastGIndex = 0;
    var lastModuleGIndex = null;

    if (lastGIndex === 0)
      lastGIndex = roofsModulesMaxGroupIndex[newModule.data.parentRoof] + 1;

    if (roof && lastGIndex && lastGIndex !== 0) {
      if (lastModuleGIndex !== newModule.groupIndex) {
        lastGIndex++;
        lastModuleGIndex = newModule.groupIndex;
      }

      //Can be removed once neighbours are calculated for a newly added Module
      const polygon2d = [];
      let overlaps = false;


      roof.modules.forEach(function(roofModule) {
        let count = 0;
        let i = 0;
        polygon2d.push(roofModule.polygon2dLocal);
        roofModule.polygon2dLocal.vertices.forEach(function(v) {
          if (compareCoordinates(v.x, newModule.polygon2dLocal.vertices[i].x) && compareCoordinates(v.y, newModule.polygon2dLocal.vertices[i].y)) {
            count++;
          }
          i++;
        });
        if (count === 4) {
          overlaps = true;
        }
      });

      if (lc.IsPolygonOnRoof(newModule.polygon2dLocal) && !overlaps) {
        var index = roofsModulesMaxIndex[newModule.data.parentRoof] = roofsModulesMaxIndex[newModule.data.parentRoof] + 1;
        //TODO : generate neighbours for newly added module
        var addedModule = roof.AddModule(index, newModule.polygon2dLocal, lastGIndex, newModule.isOpposite, newModule.rowIndex, newModule.neighbours, lc);

        modules.push(addedModule);
      }
    }
  });

  if (modules.length > 0) {
    lc.dispatchEvent({
      type: "onModulesAdded",
      addedModules: modules,
      deselect: lc.ctrlPressed
    });
    lc.dispatchEvent({
      type: "onObjectsPasted"
    });
  }

  modRefs.forEach(function(module) {
    modulesVertices.forEach(function(og) {
      if (module.index === og.index)
        module.polygon2dLocal.vertices = og.vertices;
    });
  });
};

/**
 * Compares two google maps coordinates up to 5 decimal points precision.
 * Needed because the last few decimal places tend not to be particularly accurate
 * especially when calculations have been done upon them.
 *
 * @param coord1
 * @param coord2
 * @returns {boolean} - True if the coordinates are the same or false otherwise.
 */

function compareCoordinates(coord1, coord2) {
  const correctCoord1 = Number(Math.round(coord1 + 'e' + 5) + 'e-' + 5);
  const correctCoord2 = Number(Math.round(coord2 + 'e' + 5) + 'e-' + 5);

  return correctCoord1 === correctCoord2;
}

function deepFreeze(o) {
  Object.freeze(o);

  Object.getOwnPropertyNames(o).forEach(function(prop) {
    if (o.hasOwnProperty(prop) && o[prop] !== null && (typeof o[prop] === "object" || typeof o[prop] === "function") && !Object.isFrozen(o[prop])) {
      deepFreeze(o[prop]);
    }
  });

  return o;
}

/**
 * Selects module
 */
LayoutControl.prototype.ModuleSelected = function(object, bDoNotCheckGroup, bDoNotDeselect, forceSelect) {
  if (object == undefined || object == 'undefined')
    return;

  this.SetSnapAzimuth(false);

  if (!bDoNotDeselect)
    this.DeselectObject();

  var lc = this;

  if (!bDoNotCheckGroup && object.groupIndex) {
    var modules = this.GetModuleParentRoof(object).modules;
    var modulesToSelect = [];
    var modulesCount = modules.length;

    for (var i = 0; i < modulesCount; i++) {
      if (modules[i].groupIndex == object.groupIndex) {
        modulesToSelect.push(modules[i]);
      }
    }
    modulesToSelect.forEach(function(module) {
      lc.ModuleSelected(module, true, true);
    });
    return;
  }

  // Conditional part of selecting objects
  var mapObjectIdx = this.selectedModules.indexOf(object);

  if ((mapObjectIdx !== -1 && !forceSelect)) {
    var fillColor = "";

    switch (lc.moduleColor) {
      case "blue":
        fillColor = "#000088";
        break;
      case "black":
        fillColor = "#000000";
        break;
      default:
        fillColor = "#000088";
        break;
    }

    //Deselect
    var options = {
      fillOpacity: 1,
      fillColor: fillColor,
      draggable: false
    };

    object.primitive.setOptions(options);
    this.selectedModules.splice(mapObjectIdx, 1);
  } else {
    //Select
    var options = {
      fillOpacity: 1,
      fillColor: '#E0EB2D',
      draggable: true
    };

    // SKETCH - 2579
    if (lc.workspaceId == 6)
      options.draggable = false;

    object.primitive.setOptions(options);
    this.selectedModules.push(object);
  }

  // add event on delete key to delete currently selected button
  this.deleteEventKeyup = google.maps.event.addDomListener(document, 'keyup', function(e) {
    // prevent backspace from going back
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
      e.preventDefault();
    }
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 46 || code === 8 && !$(e.target).is("input, textarea")) { //last is for input or textarea focus
      lc.DeleteSelectedModules();
    }
  });
  this.deleteEventKeydown = google.maps.event.addDomListener(document, 'keydown', function(e) {
    // prevent backspace from going back
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
      e.preventDefault();
    }
  });

  var onModuleSelected = {
    type: "onModuleSelected",
    selectedModules: lc.selectedModules
  };
  this.dispatchEvent(onModuleSelected);

  google.maps.event.addListener(object.primitive, 'drag', function() {
    lc.ModuleDraged(object);
  });
};

/**
 * Deselects currently selected modules
 *
 */
LayoutControl.prototype.DeselectModules = function() {

  var lc = this;
  if (this.selectedModules.length > 0) {
    this.selectedModules.forEach(function(object) {
      var fillColor = "";

      switch (lc.moduleColor) {
        case "blue":
          fillColor = "#000088";
          break;
        case "black":
          fillColor = "#000000";
          break;
        default:
          fillColor = "#000088";
          break;
      }

      var options = {
        fillOpacity: 1,
        fillColor: fillColor,
        draggable: false
      };

      object.primitive.setOptions(options);
      google.maps.event.clearListeners(object.primitive, 'drag');
    });
    this.selectedModules = [];
  }

  var onModuleDeselected = {
    type: "onModuleDeselected"
  };
  this.dispatchEvent(onModuleDeselected);

  google.maps.event.removeListener(this.deleteEventKeyup);
  google.maps.event.removeListener(this.deleteEventKeydown);
};

LayoutControl.prototype.HighlightNeighbours = function(module, highlight) {
  var neighborCount = module.neighbours.length;
  for (var i = 0; i < neighborCount; i++) {
    var foundModule = this.FindModuleByIndex(module, module.neighbours[i]);
    if (!foundModule)
      continue;
    foundModule.primitive.setOptions({
      fillColor: (highlight ? layoutControlElementColors.InstallationArea : '#000088')
    });
  }
};

LayoutControl.prototype.FindModuleByIndex = function(module, index) {
  if (!index)
    return null;

  var roof = this.GetModuleParentRoof(module);
  var moduleNum = roof.modules.length;
  for (var m = 0; m < moduleNum; m++) {
    if (roof.modules[m].index === index)
      return roof.modules[m];
  }

  return null;
};

// Object selection functions

/**
 * Selects object
 */
LayoutControl.prototype.ObjectSelected = function(object, multiSelect, onCreate) {

  if (object == undefined || object == 'undefined')
    return;

  this.DeselectModules();

  if (!this.shiftPressed && !this.ctrlPressed && !this.cmdPressed && !multiSelect) {
    if (this.selectedObjects.length != 1 || object !== this.selectedObjects[0])
      this.SetSnapAzimuth(false);

    this.DeselectObject();
  }

  var mapObjectIdx = this.selectedObjects.indexOf(object);
  if (mapObjectIdx != -1) {
    object.primitive.setEditable(false);
    var options = angular.extend(this.getStandardOptions(object), {
      draggable: false
    });
    object.primitive.setOptions(options);

    this.selectedObjects.splice(mapObjectIdx, 1);
  } else {
    object.primitive.setEditable(true);
    if (object.data instanceof Obstruction) {
      var options = angular.extend(this.getHighlightOptions(object), {
        draggable: true
      });
    } else {
      var options = angular.extend(this.getHighlightOptions(object), {
        draggable: false
      });
    }

    object.primitive.setOptions(options);
    this.selectedObjects.push(object);

    if (object.data instanceof Roof) {
      object.data.RefreshDistanceLabels(this);
    }
  }

  this.UpdatePropertyPanel();

  //Selecting object in object drop-down
  $("#" + this.panelId + "_objectSelect").val(object.type + object.data.index);
  // this.SetObjectColorType(this.selectedObject.type);

  var lc = this;

  // add event on delete key to delete currently selected button
  this.deleteEventKeyup = google.maps.event.addDomListener(document, 'keyup', function(e) {
    // prevent backspace from going back
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
      e.preventDefault();
    }
    var code = (e.keyCode ? e.keyCode : e.which);
    if (code === 46 && !$(e.target).is("input, textarea") || code === 8 && !$(e.target).is("input, textarea")) { //last is for input or textarea focus
      lc.DeleteSelectedObject();
    }
  });

  this.deleteEventKeydown = google.maps.event.addDomListener(document, 'keydown', function(e) {
    // prevent backspace from going back
    if (e.which === 8 && !$(e.target).is("input, textarea")) {
      e.preventDefault();
    }
  });

  var onObjectSelected = {
    type: "onObjectSelected",
    selectedObjects: lc.selectedObjects,
    createdObj: onCreate
  };
  this.dispatchEvent(onObjectSelected);
  //BH
  google.maps.event.addListener(object.primitive, 'drag', function() {
    lc.ObjectDraged(object);
  });
};

/**
 * Deselects currently selected object
 *
 */
LayoutControl.prototype.DeselectObject = function() {
  var lc = this;
  if (this.selectedObjects.length > 0) {
    var selectedObjectsDump = this.selectedObjects;
    this.selectedObjects = [];
    selectedObjectsDump.forEach(function(object) {
      if (object.data instanceof Roof) {
        object.data.RefreshDistanceLabels(lc);
      }
      object.primitive.setEditable(false);
      var options = {
        fillColor: layoutControlElementColors[object.type],
        strokeColor: layoutControlElementColors[object.type],
        fillOpacity: 0.2,
        strokeWeight: 2,
        draggable: false
      };

      if (object.primitive.selectedOpacity) {
        options.fillOpacity = object.primitive.deselectedOpacity;
      }

      if (object.primitive.selectedFill) {
        options.fillColor = object.primitive.deselectedFill;
      }

      object.primitive.setOptions(options);
    });
  }

  this.UpdatePropertyPanel();

  this.SetObjectColorType();

  // Selecting empty object in objects drop-down list
  $("#" + this.panelId + "_objectSelect").val(-1);
  var onObjectDeselected = {
    type: "onObjectDeselected"
  };
  this.dispatchEvent(onObjectDeselected);

  google.maps.event.removeListener(this.deleteEventKeyup);
  google.maps.event.removeListener(this.deleteEventKeydown);
};

/**
 * Selects object from menu on map
 *
 */
LayoutControl.prototype.SelectObjectFromList = function() {
  var selectedIdx = $("#" + this.panelId + "_objectSelect").val();

  if (selectedIdx < 0) {
    this.DeselectObject();
    this.DeselectModules();
  } else {
    var selectedObjectIndex = this.FindMapObjectById(selectedIdx);

    if (selectedObjectIndex >= 0 && this.objects[selectedObjectIndex] !== 'undefined') {
      var objectIndexInArray = this.GetObjectByIndex(selectedObjectIndex);
      this.ObjectSelected(this.objects[objectIndexInArray]);
    }
  }
}

/**
 * Finds mapObject object from given objectString
 * Parameters: objectString - it's Object type and it's index merged(e.g.: Roof3 or InstallationArea0)
 * Returns: founded mapObject
 */
LayoutControl.prototype.FindMapObjectById = function(objectString) {
  var type = objectString.substring(0, objectString.search(/\d/));
  var index = objectString.substring(objectString.search(/\d/), objectString.length);
  var mapObject;
  var bFound = false;
  this.objects.forEach(function(object, key) {
    if (bFound) {
      return;
    }
    if (object.type != type) {
      return;
    }
    if (object.data.index != index) {
      return;
    }
    mapObject = key;
  });
  return mapObject;
};

/**
 * Refreshes object selection menu
 *
 */
LayoutControl.prototype.UpdateObjectSelection = function(preselect) {
  var lc = lc_heap[this.objIdx];
  //var selectBox = $("#" + lc.panelId + "_objectSelect");
  var result = {
    objects: []
  };

  lc.selectedObjects.forEach(function(object) {
    if (!(object.data instanceof Roof))
      return;

    object.data.DrawGeometry(lc);
  });

  //     selectBox.find('option').remove();
  //     selectBox.append($("<option></option>").attr("value", -1).text("-"));
  //
  //     lc = this;
  //     this.GetRoofs().forEach(function (roof) {
  // //        selectBox.append($("<option></option>").attr("value", "Roof" + roof.index).text(roof.name));
  //         result.objects.push({id: 'Roof' + roof.index, name: roof.name, objectType: 'Roof'});
  //         var roofInstallationAreas = [];
  //         var roofObstructions = [];
  //         if (lc.GetInstallationAreas(roof)) {
  //             roofInstallationAreas = lc.GetInstallationAreas(roof);
  //         }
  //         if (lc.GetObstructions(roof)) {
  //             roofObstructions = lc.GetObstructions(roof);
  //         }
  //         var roofObjects = roofInstallationAreas.concat(roofObstructions);
  //         var roofObjectsLength = roofObjects.length;
  //         roofObjects.forEach(function (object, index)
  //         {
  //             var objectType = '';
  //             if (object instanceof InstallationArea)
  //             {
  //                 objectType = 'InstallationArea';
  //             }
  //             else if (object instanceof Obstruction)
  //             {
  //                 objectType = 'Obstruction';
  //             }
  //             var name = object.name;
  // //            selectBox.append($("<option></option>").attr("value", objectType + object.index).html(name));
  //             result.objects.push({id: objectType + object.index, name: name, objectType: objectType});
  //         });
  //     });
  //     this.outsideObstructions.forEach(function(obstruction)
  //     {
  // //        selectBox.append($("<option></option>").attr("value", "Obstruction" + obstruction.index).text(obstruction.name));
  //         result.objects.push({id: "Obstruction" + obstruction.index, name: obstruction.name});
  //     });
  //
  //     if (preselect != undefined && preselect != null) {
  //         selectBox.val(preselect);
  //         result.preselect = preselect;
  //     }

  lc.objects.forEach(function(object) {
    result.objects.push({
      id: object.type + '' + object.data.index,
      name: object.data.name,
      objectType: object.type
    });
  });

  var onObjectListChanged = {
    type: "onObjectListChanged",
    list: result
  };
  lc.dispatchEvent(onObjectListChanged);
};

/**
 * Refreshes property panel
 *
 */
LayoutControl.prototype.UpdatePropertyPanel = function() {
  return;
  // Set proper panel visible
  $("#" + this.panelId + "_noobjectProperties").css("display", this.selectedObject === "undefined" ? "" : "NONE");
  $("#" + this.panelId + "_roofProperties").css("display", (this.selectedObject !== "undefined" && this.selectedObject.type == "Roof" ? "" : "NONE"));
  $("#" + this.panelId + "_obstructionProperties").css("display", (this.selectedObject !== "undefined" && this.selectedObject.type == "Obstruction" ? "" : "NONE"));
  $("#" + this.panelId + "_installationareaProperties").css("display", (this.selectedObject !== "undefined" && this.selectedObject.type == "InstallationArea" ? "" : "NONE"));

  if (this.selectedObject === 'undefined') {
    return;
  }

  var lc = lc_heap[this.objIdx];
  var panelId = this.panelId;
  if (this.selectedObject.type === 'Roof') {
    $("#" + this.panelId + "roofName").val(this.selectedObject.data.name);
    $("#" + this.panelId + "roofName").keyup(function() {
      if ($("#" + panelId + "roofName+#error")) {
        $("#" + panelId + "roofName+#error").remove();
      }
      if (this.value.length <= 0) {
        var errorMsg = "Roof name should not be empty";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
        return;
      }
      lc.selectedObject.data.name = this.value;
      lc.UpdateObjectSelection(lc.selectedObject.type + lc.selectedObject.data.index);
    });

    $("#" + this.panelId + "roofDescription").val(this.selectedObject.data.description);
    $("#" + this.panelId + "roofDescription").keyup(function() {
      lc.selectedObject.data.description = this.value;
    });

    $("#" + this.panelId + "roofAzimuth").val(this.selectedObject.data.roofFaces[0].azimuth);
    if ($("#" + panelId + "roofAzimuth+#error")) {
      $("#" + panelId + "roofAzimuth+#error").remove();
    }
    $("#" + this.panelId + "roofAzimuth").keyup(function() {
      if ($("#" + panelId + "roofAzimuth+#error")) {
        $("#" + panelId + "roofAzimuth+#error").remove();
      }
      var changed = this.value >= -360 && this.value <= 360;
      if (changed) {
        lc.selectedObject.data.roofFaces[0].azimuth = this.value;
        lc.selectedObject.data.ChangeArrowHeading(this.value);
      } else {
        var errorMsg = "Azimuth should be in interval [-360; 360]";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
      }
    });

    //        $("#" + this.panelId + "roofSlope").val(this.selectedObject.data.roofFaces[0].slope);
    //        $("#" + this.panelId + "roofSlope").keyup(function () {
    //            lc.selectedObject.data.roofFaces[0].slope = this.value;
    //        });

    $("#" + this.panelId + "roofHeight").val(this.selectedObject.data.meanHeight);
    if ($("#" + panelId + "roofHeight+#error")) {
      $("#" + panelId + "roofHeight+#error").remove();
    }
    $("#" + this.panelId + "roofHeight").keyup(function() {
      if ($("#" + panelId + "roofHeight+#error")) {
        $("#" + panelId + "roofHeight+#error").remove();
      }
      if (!(this.value >= 0 && this.value < 999)) {
        var errorMsg = "Height should not be negative and not higher than 999.";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
        return;
      }
      var changed = lc.selectedObject.data.setMeanHeight(lc, this.value);
      if (!changed) {
        var errorMsg = "Roof on this height overlaps other roof.";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
      }
    });

    //        $("#" + this.panelId + "roofParapet").prop("checked", this.selectedObject.data.parapetWall);
    //        $("#" + this.panelId + "roofParapet").keyup(function () {
    //            lc.selectedObject.data.parapetWall = this.value;
    //        });

    //        $("#" + this.panelId + "roofParapetHeight").val(this.selectedObject.data.parapetWallHeight);
    //        $("#" + this.panelId + "roofParapetHeight").keyup(function () {
    //            lc.selectedObject.data.parapetWallHeight = this.value;
    //        });
    if ($("#" + panelId + "roofSetback+#error")) {
      $("#" + panelId + "roofSetback+#error").remove();
    }
    $("#" + this.panelId + "roofSetback").val(this.selectedObject.data.setback);
    $("#" + this.panelId + "roofSetback").keyup(function() {
      if ($("#" + panelId + "roofSetback+#error")) {
        $("#" + panelId + "roofSetback+#error").remove();
      }
      if (this.value >= 0 && this.value <= 2000) {
        lc.selectedObject.data.setback = this.value;
      } else {
        var errorMsg = "Setback should not be negative and not higher than 2000.";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
      }
    });
  } else if (this.selectedObject.type === 'Obstruction') {
    $("#" + this.panelId + "obstructionName").val(this.selectedObject.data.name);
    $("#" + this.panelId + "obstructionName").keyup(function() {
      if ($("#" + panelId + "obstructionName+#error")) {
        $("#" + panelId + "obstructionName+#error").remove();
      }
      if (this.value.length <= 0) {
        var errorMsg = "Obstruction name should not be empty";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
        return;
      }
      lc.selectedObject.data.name = this.value;
      lc.UpdateObjectSelection(lc.selectedObject.type + lc.selectedObject.data.index);
    });

    $("#" + this.panelId + "obstructionDescription").val(this.selectedObject.data.description);
    $("#" + this.panelId + "obstructionDescription").keyup(function() {
      lc.selectedObject.data.description = this.value;
    });

    if ($("#" + panelId + "obstructionHeight+#error")) {
      $("#" + panelId + "obstructionHeight+#error").remove();
    }
    $("#" + this.panelId + "obstructionHeight").val(this.selectedObject.data.height);
    $("#" + this.panelId + "obstructionHeight").keyup(function() {
      if ($("#" + panelId + "obstructionHeight+#error")) {
        $("#" + panelId + "obstructionHeight+#error").remove();
      }
      if (this.value >= 0 && this.value < 999) {
        lc.selectedObject.data.height = this.value;
      } else {
        var errorMsg = "Height should not be negative and not higher than 999.";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
      }
    });

    if ($("#" + panelId + "obstructionSetback+#error")) {
      $("#" + panelId + "obstructionSetback+#error").remove();
    }
    $("#" + this.panelId + "obstructionSetback").val(this.selectedObject.data.setback);
    $("#" + this.panelId + "obstructionSetback").keyup(function() {
      if ($("#" + panelId + "obstructionSetback+#error")) {
        $("#" + panelId + "obstructionSetback+#error").remove();
      }
      if (this.value >= 0 && this.value <= 2000) {
        lc.selectedObject.data.setback = this.value;
      } else {
        var errorMsg = "Setback should not be negative and not higher than 2000 inches.";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
      }
    });
  } else if (this.selectedObject.type === 'InstallationArea') {
    $("#" + this.panelId + "installationareaName").val(this.selectedObject.data.name);
    $("#" + this.panelId + "installationareaName").keyup(function() {
      if ($("#" + panelId + "installationareaName+#error")) {
        $("#" + panelId + "installationareaName+#error").remove();
      }
      if (this.value.length <= 0) {
        var errorMsg = "Installation Area name should not be empty";
        $(this).after("<div style=\"color: red; font-size: 0.8em; float: left;\" id=\"error\">" + errorMsg + "</div>");
        return;
      }
      lc.selectedObject.data.name = this.value;
      lc.UpdateObjectSelection(lc.selectedObject.type + lc.selectedObject.data.index);
    });

    $("#" + this.panelId + "installationareaDescription").val(this.selectedObject.data.description);
    $("#" + this.panelId + "installationareaDescription").keyup(function() {
      lc.selectedObject.data.description = this.value;
    });
  }
}

/**
 * Creates new object
 *
 */
// TODO: Need refactoring - generalize object creation logic
LayoutControl.prototype.CreateObject = function(type, primitive) {
  var idx;
  var objectData;
  var lc = this;
  var bAdded = true;
  switch (type) {
    case "Roof":
      primitive.setOptions({
        zIndex: layoutControlElementZIndexes.Roof
      });
      var roofsPolygons2D = [];
      this.GetRoofsWithSameHeight(0).forEach(function(roof) {
        roofsPolygons2D.push(roof.roofFaces[0].polygon2dLocal);
      });
      //            if (this.IsPolygonOverlapOtherPolygons(GetPolygon2D(primitive), roofsPolygons2D)) {
      //                primitive.setMap(null);
      //                bAdded = false;
      //                break;
      //            }
      idx = this.lastIndexes.Roof = this.lastIndexes.Roof + 1;
      var roofFaces = [new RoofFace(GetPolygon2D(primitive), 0, 0)];
      objectData = new Roof(idx, roofFaces, "Roof " + idx, "", 0, false, 0, 0, 0, Array.apply(null, Array(roofFaces[0].polygon2dLocal.vertices.length)).map(function() {
        return 0;
      }), this, primitive);
      // objectData.RefreshDistanceLabels(lc);
      break;
    case "Obstruction":
      primitive.setOptions({
        zIndex: layoutControlElementZIndexes.Obstruction
      });
      var polygon = GetPolygon2D(primitive);
      var firstPoint = new google.maps.LatLng(polygon.vertices[0].y, polygon.vertices[0].x);
      var obstructionsPolygons2D = [];
      this.GetObstructions().forEach(function(obstruction) {
        obstructionsPolygons2D.push(obstruction.polygon2dLocal);
      });
      var bOverlapsObstructions = this.IsPolygonOverlapOtherPolygons(GetPolygon2D(primitive), obstructionsPolygons2D);
      //            if (bOverlapsObstructions) {
      //                primitive.setMap(null);
      //                bAdded = false;
      //                break;
      //            }
      var parentRoof = lc.IsPolygonOnRoof(GetPolygon2D(primitive));
      idx = this.lastIndexes.Obstruction = this.lastIndexes.Obstruction + 1;
      objectData = new Obstruction(idx, primitive, "Obstruction " + idx, "", 0, 0, parentRoof, this);
      if (parentRoof) {
        parentRoof.obstructions.push(objectData);
      } else {
        lc.outsideObstructions.push(objectData);
      }
      break;
    case "InstallationArea":
      var polygon = GetPolygon2D(primitive);
      var firstPoint = new google.maps.LatLng(polygon.vertices[0].y, polygon.vertices[0].x);
      var parentRoof = lc.IsPointInsideRoof(firstPoint);
      if (!parentRoof) {
        primitive.setMap(null);
        bAdded = false;
        break;
      }
      var installationAreasPolygons2D = [];
      this.GetInstallationAreas().forEach(function(installationArea) {
        installationAreasPolygons2D.push(installationArea.polygon2dLocal);
      });
      var bInside = IsPolygonInsideOtherPolygon(GetPolygon2D(primitive), GetPolygon2D(parentRoof.primitive));
      if (!bInside || this.IsPolygonOverlapOtherPolygons(GetPolygon2D(primitive), installationAreasPolygons2D)) {
        primitive.setMap(null);
        bAdded = false;
        break;
      }
      idx = this.lastIndexes.InstallationArea = this.lastIndexes.InstallationArea + 1;
      objectData = new InstallationArea(idx, primitive, "InstallationArea " + idx, "", parentRoof, this);
      parentRoof.installationAreas.push(objectData);
      break;
    case 'Unclassified':
      primitive.setOptions({
        zIndex: layoutControlElementZIndexes.Unclassified++
      });
      idx = this.lastIndexes.Unclassified = this.lastIndexes.Unclassified + 1;
      objectData = new Unclassified(idx, primitive, 'Object ' + idx, '', lc);
      break;
  }

  if (bAdded) {
    idx = this.lastIndexes.MapObject = this.lastIndexes.MapObject + 1;
    var newObj = new mapObject(idx, type, primitive, objectData);
    this.objects.push(newObj);

    this.setObjectZIndex(this.objects, newObj);
    this.UpdateObjectSelection();
    this.ObjectSelected(newObj, undefined, true);

    if (newObj.data instanceof Roof) {
      lc.RefreshProjectMinLinesDistances();
      newObj.data.RefreshDistanceLabels(lc);
    }

    var onObjectCreated = {
      type: "onObjectCreated",
      createdObject: newObj
    };
    this.dispatchEvent(onObjectCreated);
  }

  return newObj;
};

/**
 * Finds object with specified index
 * Properties: index of the object
 * Returns: finded object
 */
LayoutControl.prototype.GetObjectByIndex = function(index) {
  if (this.objects !== 'undefined' && this.objects.length > 0) {
    for (i = 0; i < this.objects.length; i++) {
      if (this.objects[i].index == index) {
        return i;
      }
    }

    return -1;
  } else {
    return -1;
  }
}

/**
 * Returns array with all objects
 *
 */
LayoutControl.prototype.GetObjects = function(type, clone) {
  var result = [];

  this.objects.forEach(function(item) {
    if (type == undefined || type === '' || type === item.type) {
      if (clone) {
        var toPush = $.extend(true, {}, item.data);
        switch (item.type) {
          case "Roof":
            toPush.__proto__ = Roof.prototype;
            break;
          case "Obstruction":
            toPush.__proto__ = Obstruction.prototype;
            break;
          case "InstallationArea":
            toPush.__proto__ = InstallationArea.prototype;
            break;
          case "Shadows":
            toPush.__proto__ = Shadow.prototype;
            break;
        }
      } else {
        toPush = item.data;
      }
      result.push(toPush);
    }
  });

  return result;
};

// Function for object cloning
// Got from http://stackoverflow.com/questions/2261247/exactly-clone-an-object-in-javascript
// Outputs an Clone object, but instanceof works perfectly

function Clone() {}

function clone(obj) {
  Clone.prototype = obj;
  return new Clone();
}

/**
 * Returns all Roof objects
 *
 */
LayoutControl.prototype.GetRoofs = function() {
  return this.GetObjects("Roof");
}

/**
 * Returns Roof object with specified index
 *
 */
LayoutControl.prototype.GetRoofByIndex = function(index) {
  var bFound = false;
  var toReturn = null;
  this.GetRoofs().forEach(function(roof) {
    if (bFound) {
      return;
    }
    if (roof.index === index) {
      toReturn = roof;
      bFound = true;
    }
  });
  return toReturn;
}

/**
 * Returns array of Roofs excluding one with given index
 *
 */
LayoutControl.prototype.GetRoofsExcludeByIndex = function(index) {
  var roofs = this.GetRoofs();
  // getting needed element index inside array
  var foundItemIndex = false;
  roofs.forEach(function(roof, key) {
    if (foundItemIndex) {
      return;
    }
    if (roof.index == index) {
      foundItemIndex = key;
    }
  });
  roofs.splice(foundItemIndex, 1);
  return roofs;
}

/**
 * Returns array of Roofs with same height with given roof
 * Parameters: arg - Roof or meanHeight
 *
 */

LayoutControl.prototype.GetRoofsWithSameHeight = function(arg) {
  var meanHeight, roofs;
  // check, if arg is Roof or meanHeight
  if (arg instanceof Roof) {
    meanHeight = arg.meanHeight;
    roofs = this.GetRoofsExcludeByIndex(arg.index);
  } else {
    meanHeight = arg;
    roofs = this.GetRoofs();
  }
  // get roofs with equals menHeight
  var roofsToCheck = [];
  roofs.forEach(function(lookingRoof) {
    if (meanHeight == lookingRoof.meanHeight) {
      roofsToCheck.push(lookingRoof);
    }
  });
  return roofsToCheck;
}

/**
 * Returns all Obstruction objects
 *
 */
LayoutControl.prototype.GetObstructions = function(roof) {
  if (roof === null || roof === undefined) {
    return this.GetObjects("Obstruction");
  }
  if (roof instanceof Roof) {
    return roof.obstructions;
  }
  console.log("Didn't get obstructions: given object is not a Roof");
  return false;
}

/**
 * Returns Obstruction object with specified index
 *
 */
LayoutControl.prototype.GetObstructionByIndex = function(index) {
  var bFound = false;
  var toReturn = null;
  this.GetObstructions().forEach(function(obstruction) {
    if (bFound) {
      return;
    }
    if (obstruction.index === index) {
      toReturn = obstruction;
      bFound = true;
    }
  });
  return toReturn;
}

/**
 * Returns array of Obstructions excluding one with specified index
 *
 */
LayoutControl.prototype.GetObstructionsExcludeByIndex = function(index) {
  var obstructions = this.GetObstructions();
  // getting needed element index inside array
  var foundItemIndex = false;
  obstructions.forEach(function(obstruction, key) {
    if (foundItemIndex) {
      return;
    }
    if (obstruction.index == index) {
      foundItemIndex = key;
    }
  });
  obstructions.splice(foundItemIndex, 1);
  return obstructions;
}

/**
 * Returns array of Obstructions excluding one with specified index
 *
 */
LayoutControl.prototype.GetShadowByObject = function(object) {
  var res = null;
  this.shadows.forEach(function(shadow, key) {
    if (object.data.index == shadow.data.parentObject.index) {
      res = shadow;
      return;
    }
  });
  return res;
};

/**
 * Returns all InstallationArea objects
 *
 */
LayoutControl.prototype.GetInstallationAreas = function(roof) {
  if (roof === null || roof === undefined) {
    return this.GetObjects("InstallationArea");
  }
  if (roof instanceof Roof) {
    return roof.installationAreas;
  }
  console.log("Didn't get installation areas: given object is not a Roof");
  return false;
}

/**
 * Returns array of Installation Areas excluding one with specified index
 *
 */
LayoutControl.prototype.GetInstallationAreasExcludeByIndex = function(index) {
  var installationAreas = this.GetInstallationAreas();
  // getting needed element index inside array
  var foundItemIndex = false;
  installationAreas.forEach(function(installationArea, key) {
    if (foundItemIndex) {
      return;
    }
    if (installationArea.index == index) {
      foundItemIndex = key;
    }
  });
  installationAreas.splice(foundItemIndex, 1);
  return installationAreas;
}

/**
 * Returns InstallationArea object with specified index
 *
 */
LayoutControl.prototype.GetInstallationAreaByIndex = function(index) {
  var bFound = false;
  var toReturn = null;
  this.GetInstallationAreas().forEach(function(installationAreas) {
    if (bFound) {
      return;
    }
    if (installationAreas.index == index) {
      toReturn = installationAreas;
      bFound = true;
    }
  });
  return toReturn;
}

/**
 * Deletes selected modules
 *
 */
LayoutControl.prototype.DeleteSelectedModules = function() {
  var lc = this;
  if (!lc.selectedModules || lc.selectedModules.length == 0) {
    return;
  }

  const roofModules = [];
  lc.selectedModules
    .map(function(module) {
      return module.data.parentRoof;
    })
    .filter(function(value, index, roofIndexes) {
      return roofIndexes.indexOf(value) === index
    })
    .forEach(function(roofIndex) {
      roofModules.push({
        roofIndex,
        roofModules: angular.copy(lc.GetRoofByIndex(roofIndex).modules)
      });
    });

  lc.selectedModules.forEach(function(module) {
    switch (module.type) {
      case "Module":
        var roof = lc.GetRoofByIndex(module.data.parentRoof);
        if (roof) {
          roof.DeleteModuleFromNeighbours(module.index, function() {
            roof.DeleteModuleByIndex(module);
          });
        }
    }
  });

  var onModuleDeleted = {
    type: "onModuleDeleted",
    data: {
      deletedModules: lc.selectedModules,
      roofModules
    }
  };
  lc.dispatchEvent(onModuleDeleted);

  var onModuleCountChanged = {
    type: "onModuleCountChanged",
    objects: lc.GetProjectModuleCount()
  };
  this.dispatchEvent(onModuleCountChanged);

  this.RefreshModulesClickEvents();
  this.DeselectModules();
};


/**
 * Undoes passed in object from map index
 * @param object
 * @constructor
 */
LayoutControl.prototype.UndoObject = function(object) {
  const lc = this;

  if (!object || object === undefined || object === null)
    return;


  const objectIndex = lc.GetObjectByIndex(object.index);
  if (objectIndex >= 0) {
    object.primitive.setMap(null);
    lc.objects.splice(objectIndex, 1);
  }

  switch (object.type) {
    case "Roof":
      object.data.ClearModules(lc);
      object.data.azimuthArrow.setMap(null);
      object.data.ClearObstructionsPrimitives(lc);
      object.data.ClearInstallationAreasPrimitives(lc);
      object.data.RefreshDistanceLabels(lc, true);
      object.data.ClearGeometry(lc);
      break;
    case "InstallationArea":
      object.data.parentRoof.ClearModules(lc);

      var installationAreaIndex = object.data.index;
      object.data.parentRoof.DeleteInstallationAreaByIndex(installationAreaIndex);
      break;
    case "Obstruction":
      var obstructionIndex = object.data.index;
      if (object.data.parentRoof) {
        object.data.parentRoof.DeleteObstructionByIndex(obstructionIndex);
      } else {
        lc.outsideObstructions.splice(lc.outsideObstructions.indexOf(object.data), 1);
      }
      break;
    case "Obstruction_deletion":
      //var obstructionIndex = object.data.index;

      lc.AddObstruction(angular.copy(object.data.data));
      lc.GetObstructions().forEach(function(obstruction) {
        obstruction.parentRoof = lc.GetRoofByIndex(obstruction.parentRoof);
        if (!obstruction.parentRoof) {
          obstruction.parentRoof = false;
          lc.outsideObstructions.push(obstruction);
        }
      });
      // Reassign all obstructions and installation areas from indexes to objects
      lc.GetRoofs().forEach(function(roof) {
        roof.obstructions.forEach(function(obstruction, key) {
          roof.obstructions[key] = lc.GetObstructionByIndex(obstruction);
        });
      });
      this.UpdateObjectSelection();

      break;
    case "Module":
      var moduleIndex = object.data.index;
      var roof = lc.GetRoofByIndex(object.data.parentRoof);

      if (roof)
        roof.DeleteModuleByIndex(moduleIndex);
      break;
    case 'Module_Deleted':
      object.data.forEach(function(roofData) {
        const roof = lc.GetRoofByIndex(roofData.roofIndex);
        roof.ClearModules(lc);
        roofData.roofModules.forEach(function(module) {
          roof.AddModule(module.index, module.polygon2dLocal, module.groupIndex, module.isOpposite, module.rowIndex, module.neighbours, lc);
        })
      });
      break;
  }

  var onObjectUndo = {
    type: "onObjectUndo",
    object: object
  };
  lc.dispatchEvent(onObjectUndo);

  this.DeselectObject();
  this.DeselectModules();

  this.UpdateObjectSelection();

  this.RefreshProjectMinLinesDistances();
};

/**
 * Deletes selected object
 *
 */
LayoutControl.prototype.DeleteSelectedObject = function(bNotClearObstructions) {
  var lc = this;
  if (!lc.selectedObjects || lc.selectedObjects.length == 0) {
    return;
  }

  lc.selectedObjects.forEach(function(object) {
    var objectIndex = lc.GetObjectByIndex(object.index);
    switch (object.type) {
      case "Roof":
        object.data.ClearModules(lc);
        object.data.azimuthArrow.setMap(null);
        if (!bNotClearObstructions)
          object.data.ClearObstructionsPrimitives(lc);
        object.data.ClearInstallationAreasPrimitives(lc);
        object.data.RefreshDistanceLabels(lc, true);
        object.data.ClearGeometry(lc);
        break;
      case "InstallationArea":
        object.data.parentRoof.ClearModules(lc);
        var installationAreaIndex = object.data.index;
        object.data.parentRoof.DeleteInstallationAreaByIndex(installationAreaIndex);
        break;
      case "Obstruction":
        var obstructionIndex = object.data.index;
        if (object.data.parentRoof) {
          object.data.parentRoof.DeleteObstructionByIndex(obstructionIndex);
        } else {
          lc.outsideObstructions.splice(lc.outsideObstructions.indexOf(object.data), 1);
        }
      case "Module":
        var moduleIndex = object.data.index;
        var roof = lc.GetRoofByIndex(object.data.parentRoof);
        if (roof)
          roof.DeleteModuleByIndex(moduleIndex);
        break;
    }
    if (objectIndex >= 0) {
      object.primitive.setMap(null);
      lc.objects.splice(objectIndex, 1);
    }
  });

  var onObjectDeleted = {
    type: "onObjectDeleted",
    deletedObjects: lc.selectedObjects
  };
  lc.dispatchEvent(onObjectDeleted);

  this.DeselectObject();
  this.DeselectModules();

  this.UpdateObjectSelection();

  this.RefreshProjectMinLinesDistances();
};

LayoutControl.prototype.PrimitiveChanged = function(primitive) {
  var lc = this;
  var onPrimitiveChanged = {
    type: "onPrimitiveChanged",
    primitive: primitive
  };
  lc.dispatchEvent(onPrimitiveChanged);
};

LayoutControl.prototype.roofDataUpdated = function() {
  var lc = this;
  var onRoofDataUpdated = {
    type: "onRoofDataUpdated"
  }
  lc.dispatchEvent(onRoofDataUpdated);
}
/**
 * Deletes all objects
 *
 */
LayoutControl.prototype.ClearObjects = function() {
  if (this.objects === 'undefined' || this.objects.length === 0) {
    return;
  }

  this.objects.forEach(function(item) {
    if (selectedObject.data.primitive !== 'undefined') {
      selectedObject.data.primitive.setMap(null);
    }
    if (selectedObject.data.data instanceof Roof) {
      selectedObject.data.data.azimuthArrow.setMap(null);
      selectedObject.data.data.ClearModules(this);
    }
  });

  this.objects.length = 0;

  this.UpdateObjectSelection();

  this.DeselectObject();

  this.DeselectModules();
}

/**
 * Start polygon drawing
 *
 */
LayoutControl.prototype.StartDrawPolygon = function(drawingManager, callback, type, infinite) {
  var lc = this;
  lc.currentDrawingManager = drawingManager;
  if (type === undefined || type === null) {
    type = "polygon";
  }

  this.bStopPressed = false;
  // cancel drawing if escape button is pressed
  google.maps.event.removeListener(this.startDrawEscapeKeyUp);

  this.startDrawEscapeKeyUp = google.maps.event.addDomListener(document, 'keyup', function(e) {
    var code = (e.keyCode ? e.keyCode : e.which);

    if (code === 27) {
      lc.bStopPressed = true;

      if (!infinite)
        drawingManager.setDrawingMode(null);

      $('#' + lc.containerId).off('mousemove.drawing');
      lc.RefreshDrawDistanceLabel();
    }

    lc.SetActiveBtn();
  });

  // complete drawing on rmb
  google.maps.event.addDomListener(lc.map, 'rightclick', function() {
    if (!infinite)
      lc.currentDrawingManager.setDrawingMode(null);
    lc.SetActiveBtn();
  });

  google.maps.event.addListener(drawingManager, 'polygoncomplete', function(primitive) {
    if (typeof primitive.getPath == 'function' && primitive.getPath().length < 3) {
      primitive.setMap(null);
      return;
    }

    if (typeof primitive.getRadius == 'function' && primitive.getRadius() < CYCLE_MIN_RADIUS) {
      primitive.setMap(null);
      return;
    }

    if (lc.bStopPressed) {
      primitive.setMap(null);
      return;
    }

    if (!infinite) {
      drawingManager.setDrawingMode(null);
    } else {
      lc.lastClickLatLng = null;
    }

    primitive.shapeType = "polygon";

    var newObject = callback(primitive);

    google.maps.event.addListener(newObject.primitive, 'click', function() {
      lc.ObjectSelected(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragstart', function() {
      lc.ObjectDragStart(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'drag', function() {
      lc.ObjectDraged(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragend', function() {
      lc.ObjectDragEnd(newObject);
    });

    lc.SetActiveBtn();

    if (!infinite)
      lc.currentDrawingManager = null;
  });

  google.maps.event.addListener(drawingManager, 'circlecomplete', function(primitive) {
    if (typeof primitive.getPath == 'function' && primitive.getPath().length < 3) {
      primitive.setMap(null);
      return;
    }

    if (typeof primitive.getRadius == 'function' && primitive.getRadius() < CYCLE_MIN_RADIUS) {
      primitive.setMap(null);
      return;
    }

    if (lc.bStopPressed) {
      primitive.setMap(null);
      return;
    }

    if (!infinite) {
      drawingManager.setDrawingMode(null);
    } else {
      lc.lastClickLatLng = null;
    }

    primitive.shapeType = "circle";

    var newObject = callback(primitive);

    google.maps.event.addListener(newObject.primitive, 'click', function() {
      lc.ObjectSelected(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragstart', function() {
      lc.ObjectDragStart(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'drag', function() {
      lc.ObjectDraged(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragend', function() {
      lc.ObjectDragEnd(newObject);
    });

    lc.SetActiveBtn();

    if (!infinite)
      lc.currentDrawingManager = null;
  });

  /*google.maps.event.addListener(drawingManager, type + 'complete', function (primitive)
  {
    primitive.setMap(null);
    return;

    if (typeof primitive.getPath == 'function' && primitive.getPath().length < 3){
      primitive.setMap(null);
      return;
    }

    if (typeof primitive.getRadius == 'function' && primitive.getRadius() < CYCLE_MIN_RADIUS) {
      primitive.setMap(null);
      return;
    }

    if (lc.bStopPressed) {
      primitive.setMap(null);
      return;
    }
    if (!infinite)
    {
      drawingManager.setDrawingMode(null);
    }
    else
    {
      lc.lastClickLatLng = null;
    }
    primitive.shapeType = type;
    var newObject = callback(primitive);

    google.maps.event.addListener(newObject.primitive, 'click', function () {
      lc.ObjectSelected(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragstart', function () {
      lc.ObjectDragStart(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'drag', function () {
      lc.ObjectDraged(newObject);
    });

    google.maps.event.addListener(newObject.primitive, 'dragend', function () {
      lc.ObjectDragEnd(newObject);
    });

    lc.SetActiveBtn();
    if (!infinite)
      lc.currentDrawingManager = null;
  });
*/
};

LayoutControl.prototype.IsPointInsideRoof = function(point) {
  var lc = this;
  var bFound = false;
  lc.GetRoofs().forEach(function(roof) {
    if (bFound) {
      return;
    }
    var contains = google.maps.geometry.poly.containsLocation(point, roof.primitive);
    if (contains) {
      bFound = roof;
    }
  });
  return bFound;
};

LayoutControl.prototype.IsPolygonOnRoof = function(polygon) {
  var lc = this;
  var foundRoof = false;
  lc.GetRoofs().forEach(function(roof) {
    if (foundRoof) {
      return;
    }
    if (lc.IsPolygonOverlapOtherPolygons(polygon, [roof.roofFaces[0].polygon2dLocal])) {
      foundRoof = roof;
    }
  });
  return foundRoof;
};

/**
 * Start Roof polygon drawing
 *
 */
LayoutControl.prototype.StartDrawRoof = function() {
  var lc = this;
  if (lc.currentDrawingManager != null) {
    lc.currentDrawingManager.setDrawingMode(null);
  }
  this.SetActiveBtn('Roof');
  lc.StartDrawPolygon(GetRoofDrawingManager(this.map, lc, 1), function(primitive) {
    SetFullPolygonColor(primitive, layoutControlElementColors.Roof);
    return lc.CreateObject("Roof", primitive);
  });
};

/**
 * Start Obstruction polygon drawing
 *
 */
LayoutControl.prototype.StartDrawObstruction = function(type) {
  var lc = this;
  if (lc.currentDrawingManager != null) {
    lc.currentDrawingManager.setDrawingMode(null);
  }
  this.SetActiveBtn('Obstruction');
  lc.StartDrawPolygon(GetObstructionDrawingManager(this.map, type), function(primitive) {
    SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
    return lc.CreateObject("Obstruction", primitive);
  }, type);
}

/**
 * Start InstallationArea polygon drawing
 *
 */
LayoutControl.prototype.StartDrawInstallationArea = function() {
  var lc = this;
  if (lc.currentDrawingManager != null) {
    lc.currentDrawingManager.setDrawingMode(null);
  }
  this.SetActiveBtn('InstallationArea');
  lc.StartDrawPolygon(GetInstallationAreaDrawingManager(this.map, lc, 2), function(primitive) {
    SetFullPolygonColor(primitive, layoutControlElementColors.InstallationArea);
    return lc.CreateObject("InstallationArea", primitive);
  });
};

LayoutControl.prototype.RefreshDrawDistanceLabel = function(e) {
  var lc = this;

  $('#mapContainer .map-draw-distance-label').remove();

  if (e == undefined || lc.lastClickLatLng == undefined || lc.currentDrawingManager == undefined)
    return;

  var currentPos = pixelToLatlng(e.offsetX, e.offsetY, lc.map);
  var dist = google.maps.geometry.spherical.computeDistanceBetween(lc.lastClickLatLng, currentPos);

  var labelMeasurementSystem = layoutControlMeasurementSystems[lc.measurementSystem].shortName;

  var blockContent = '<div class="map-draw-distance-label" style="top: ' + (e.offsetY - 30) + 'px; left: ' + (e.offsetX + 10) + 'px;">' + +lc.ConvertNumberFromMeters(dist, lc.measurementSystem, 2) + ' ' + labelMeasurementSystem +
    '</div>';

  $('#mapContainer').append(blockContent);
};

LayoutControl.prototype.StartDrawObject = function(shape) {
  if (shape !== 'polygon' && shape !== 'circle')
    throw 'Cannot StartDrawObject - shape must be \'polygon\' or \'circle\'';

  var lc = this;
  if (lc.currentDrawingManager != null) {
    lc.currentDrawingManager.setDrawingMode(null);
    $('#' + lc.containerId).off('mousemove.drawing');
    lc.RefreshDrawDistanceLabel();
  }
  lc.SetActiveBtn(null);
  lc.lastClickLatLng = null;
  if (shape === 'polygon') {
    $('#' + lc.containerId).on('mousemove.drawing', function(e) {
      lc.RefreshDrawDistanceLabel(e);
    });
  }
  lc.StartDrawPolygon(GetObjectDrawingManager(lc.map, shape), function(primitive) {
    //Circly to polygon to calculate area
    var areaPrimitive = primitive;
    if (areaPrimitive.shapeType == 'circle') {
      var color = layoutControlElementColors.Unclassified;
      areaPrimitive = lc.AddPrimitive(GetPolygon2D(primitive), color, color, layoutControlElementZIndexes.Unclassified);
      areaPrimitive.setMap(null);

      //Obstruction by default for circle
      SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
      return lc.CreateObject('Obstruction', primitive);
    }

    if (lc.IsPolygonOnRoof(GetPolygon2D(primitive))) {
      SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
      return lc.CreateObject('Obstruction', primitive);
    } else {
      var newShapeArea = google.maps.geometry.spherical.computeArea(areaPrimitive.getPath());
      if (newShapeArea >= layoutMinRoofArea) {
        SetFullPolygonColor(primitive, layoutControlElementColors.Roof);
        return lc.CreateObject('Roof', primitive);
      } else {
        SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
        return lc.CreateObject('Obstruction', primitive);
      }
    }

    SetFullPolygonColor(primitive, layoutControlElementColors.Unclassified);
    return lc.CreateObject('Unclassified', primitive);
  }, shape, true);
};

function convertPrimitive(lc, primitive) {
  //Circly to polygon to calculate area
  var areaPrimitive = primitive;
  if (areaPrimitive.shapeType == 'circle') {
    var color = layoutControlElementColors.Unclassified;
    areaPrimitive = lc.AddPrimitive(GetPolygon2D(primitive), color, color, layoutControlElementZIndexes.Unclassified);
    areaPrimitive.setMap(null);

    //Obstruction by default for circle
    SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
    return lc.CreateObject('Obstruction', primitive);
  }

  if (lc.IsPolygonOnRoof(GetPolygon2D(primitive))) {
    SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
    return lc.CreateObject('Obstruction', primitive);
  } else {
    var newShapeArea = google.maps.geometry.spherical.computeArea(areaPrimitive.getPath());
    if (newShapeArea >= layoutMinRoofArea) {
      SetFullPolygonColor(primitive, layoutControlElementColors.Roof);
      return lc.CreateObject('Roof', primitive);
    } else {
      SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
      return lc.CreateObject('Obstruction', primitive);
    }
  }

  SetFullPolygonColor(primitive, layoutControlElementColors.Unclassified);
  return lc.CreateObject('Unclassified', primitive);
}

function GetObjectDrawingManager(map, type) {
  var style = {
    strokeWeight: 3,
    strokeColor: layoutControlElementColors.Unclassified,
    strokeOpacity: 1,
    fillColor: layoutControlElementColors.Unclassified,
    fillOpacity: 0.5,
    zIndex: 5000,
    editable: true
  };
  var options = {
    drawingControl: false
  };
  switch (type) {
    case "polygon":
      options.drawingMode = google.maps.drawing.OverlayType.POLYGON;
      options.polygonOptions = style;
      break;
    case "circle":
      options.drawingMode = google.maps.drawing.OverlayType.CIRCLE;
      options.circleOptions = style;
      break;
  }
  var drawingManager = new google.maps.drawing.DrawingManager(options);
  drawingManager.setMap(map);
  return drawingManager;
}

/**
 * Returns array with all objects and all it's attributes
 * Formated for JSON encoding
 *
 */
LayoutControl.prototype.GetProject = function() {
  var center = this.center;
  var lc = this;
  var result = {
    latitude: center.lat,
    longitude: center.lng,
    measurementSystem: this.measurementSystem,
    roofs: this.GetObjects("Roof", true),
    obstructions: this.GetObjects("Obstruction", true),
    installationareas: this.GetObjects("InstallationArea", true)
  };

  // format references to prevent circular error from JSON

  result.roofs.forEach(function(roof) {
    roof.installationAreas.forEach(function(installationArea, installationAreaKey) {
      roof.installationAreas[installationAreaKey] = installationArea.index;
    });
    roof.obstructions.forEach(function(obstruction, obstructionKey) {
      roof.obstructions[obstructionKey] = obstruction.index;
    });
    roof.modules.forEach(function(module, key) {
      roof.modules[key] = $.extend(true, {}, module);
      delete roof.modules[key].parentInstallationArea;
      roof.modules[key].polygon2dLocal = GetPolygon2D(roof.modules[key].primitive);
      delete roof.modules[key].primitive;
      delete roof.modules[key].azimuthArrow;
    });
    delete roof.azimuthArrow;
    delete roof.primitive;
    delete roof.drawSetbackRequest;
    delete roof.setbackPolygon;
    delete roof.cornerZones;
    delete roof.edgeZones;
    delete roof.interiorZone;

    roof.roofFaces[0].area = lc.GetRoofArea(lc.GetRoofByIndex(roof.index));
  });
  result.obstructions.forEach(function(obstruction) {
    if (obstruction.parentRoof)
      obstruction.parentRoof = obstruction.parentRoof.index;
    else
      delete obstruction.parentRoof;
    obstruction.shape = obstruction.primitive.shapeType;
    delete obstruction.primitive;
  });
  result.installationareas.forEach(function(installationarea) {
    if (installationarea.parentRoof)
      installationarea.parentRoof = installationarea.parentRoof.index;
    else
      delete installationarea.parentRoof;
    delete installationarea.primitive;
    delete installationarea.modules;
  });
  return result;
}

/**
 * Load project onto map from specified array
 *
 */
LayoutControl.prototype.LoadProject = function(project) {
  if (typeof project === 'undefined' || project === '') {
    return;
  }

  this.ClearObjects();
  //this.SetCenter(project.latitude, project.longitude);

  this.map.setZoom(20);

  if (project.roofs !== 'undefined') {
    this.AddRoofs(project.roofs);
  }

  if (project.obstructions !== 'undefined') {
    this.AddObstructions(project.obstructions);
  }

  if (project.installationareas !== 'undefined') {
    this.AddInstallationAreas(project.installationareas);
  }

  var lc = this;

  // Recreate all modules
  lc.GetRoofs().forEach(function(roof) {
    var moduleNum = roof.modules.length;
    roof.DrawGeometry(lc);

    for (var i = 0; i < moduleNum; i++) {
      if (lc.lastIndexes.Module < roof.modules[i].index) {
        lc.lastIndexes.Module = roof.modules[i].index;
      }

      if (roof.modules[i].neighbours === null || roof.modules[i].neighbours === undefined) {
        roof.modules[i].neighbours = [];
      }

      var fillColor = "";

      switch (lc.moduleColor) {
        case "blue":
          fillColor = "#000088";
          break;
        case "black":
          fillColor = "#000000";
          break;
        default:
          fillColor = "#000088";
          break;
      }

      var modulePrimitive = lc.AddPrimitive(roof.modules[i].polygon2dLocal, "#888888", fillColor, layoutControlElementZIndexes.Module, 0.5, 1, 1);

      function addClickEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'click', function() {
          lc.ModuleSelected(roof.modules[key]);
        });
      }
      addClickEventListener(modulePrimitive, i);

      function addDragEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'drag', function() {
          lc.ModuleDraged(roof.modules[key]);
        });
      }
      addDragEventListener(modulePrimitive, i);

      function addDragEndEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'dragend', function() {
          lc.ModuleDragEnd(roof.modules[key]);
        });
      }
      addDragEndEventListener(modulePrimitive, i);

      function addDragStartEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'dragstart', function() {
          lc.ModuleDragStart(roof.modules[key]);
        });
      }
      addDragStartEventListener(modulePrimitive, i);

      function addMouseOverEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'mouseover', function() {
          roof.primitive.hover = true;
          roof.RefreshDistanceLabels(lc);
        });
      }
      addMouseOverEventListener(modulePrimitive, i);

      function addMouseOutEventListener(primitive, key) {
        google.maps.event.addListener(primitive, 'mouseout', function() {
          roof.primitive.hover = false;
          roof.RefreshDistanceLabels(lc);
        });
      }
      addMouseOutEventListener(modulePrimitive, i);

      var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
      var ModuleData = {
        objectType: roof.modules[i].type,
        index: idx,
        parentRoof: roof.index
      };

      roof.modules[i].type = "Module";
      roof.modules[i].data = ModuleData;

      roof.modules[i].primitive = modulePrimitive;
      roof.modules[i].__proto__ = LayoutModule.prototype;

      if (!roof.modules[i].azimuth)
        roof.modules[i].azimuth = roof.roofFaces[0].azimuth;
    }
    var i = 0,
      modulesCount = roof.modules.length,
      bFirstTime = true;

    function moduleAzimuthArrowCallback(arrow, key) {
      roof.modules[key].azimuthArrow = arrow.marker;
      if (bFirstTime) {
        for (i = key + 1; i < modulesCount; i++) {
          if (roof.modules[i].azimuth != roof.modules[i - 1].azimuth) {
            bFirstTime = true;
            roof.modules[i].GetAzimuthArrow(lc, moduleAzimuthArrowCallback, i);
            break;
          }
          roof.modules[i].GetAzimuthArrow(lc, function(arrow, key) {
            roof.modules[key].azimuthArrow = arrow.marker;
          }, i);
        }
      }
    }

    if (roof.modules[i])
      roof.modules[i].GetAzimuthArrow(lc, moduleAzimuthArrowCallback, i);

    // var cache = layoutControlCache.moduleAzimuthArrows[roof.roofFaces[0].azimuth];
  });
  // Reassign obstruction's and installation area's parentRoofs from indexes to objects
  lc.GetObstructions().forEach(function(obstruction) {
    obstruction.parentRoof = lc.GetRoofByIndex(obstruction.parentRoof);
    if (!obstruction.parentRoof) {
      obstruction.parentRoof = false;
      lc.outsideObstructions.push(obstruction);
    }
  });
  lc.GetInstallationAreas().forEach(function(installationArea) {
    installationArea.parentRoof = lc.GetRoofByIndex(installationArea.parentRoof);
  });
  // Reassign all obstructions and installation areas from indexes to objects
  lc.GetRoofs().forEach(function(roof) {
    roof.obstructions.forEach(function(obstruction, key) {
      roof.obstructions[key] = lc.GetObstructionByIndex(obstruction);
    });
    roof.installationAreas.forEach(function(installationArea, key) {
      roof.installationAreas[key] = lc.GetInstallationAreaByIndex(installationArea);
    });
  });
  this.UpdateObjectSelection();
};

function GetDoLayoutInputsFromSelectedObject(selectedObject, lc) {
  var roofArea;
  var installationArea;

  if (selectedObject instanceof Roof) {
    roofArea = selectedObject;
    if (roofArea.installationAreas.length > 0) {
      console.log('Can\'t generate layout: if Roof have Installation Area, layout should be generated on it.');
      return false;
    }
  } else if (selectedObject instanceof InstallationArea) {
    installationArea = selectedObject;
    roofArea = installationArea.parentRoof;
  } else if (!selectedObject && lc.GetRoofs().length == 1) {
    roofArea = lc.GetRoofs()[0];
  } else {
    console.log('Can\'t generate layout: selected object is not a Roof or Installation Area');
    return false;
  }

  var azimuth = 180 - parseFloat(roofArea.roofFaces[0].azimuth);
  if (azimuth > 180)
    azimuth -= 360;

  var result = {
    roofArea: roofArea,
    installationArea: installationArea,
    // obstructions: roofArea.obstructions,
    // outerObstructions: lc.outsideObstructions,
    azimuth: azimuth.toString(),
    roofHeight: roofArea.meanHeight == "" ? 0 : roofArea.meanHeight,
    tilt: roofArea.roofFaces[0].slope == "" ? 0 : roofArea.roofFaces[0].slope,
    setback: roofArea.setback == "" ? 0 : roofArea.setback
    // measurementSystem: lc.measurementSystem
  };

  return result;
}

function GetDoLayoutInputsFromArgs(args, lc) {
  if (typeof args != 'object') {
    return false;
  }

  var roofArea = args['Roof'];

  if (roofArea == undefined) {
    console.log('Can\'t generate layout: roof is not specified');
    return false;
  }

  var azimuth = 180 - parseFloat(roofArea.roofFaces[0].azimuth);
  if (azimuth > 180)
    azimuth -= 360;

  var result = {
    roofArea: args['Roof'],
    installationArea: args['InstallationArea'],
    // obstructions: args['Obstructions'] instanceof Array ? args['Obstructions'] : roofArea.obstructions,
    // outerObstructions: args['OuterObstructions'] instanceof Array ? args['OuterObstructions'] : lc.outsideObstructions,
    azimuth: args['Azimuth'] == undefined ? azimuth : args['Azimuth'],
    tilt: args['Tilt'] == undefined ? roofArea.roofFaces[0].slope : args['Tilt'],
    setback: args['Setback'] == undefined ? roofArea.setback : args['Setback'],
    roofHeight: args['RoofHeight'] == undefined ? roofArea.meanHeight : args['RoofHeight']
    // measurementSystem: args['MeasurementSystem'] == undefined ? lc.measurementSystem : args['MeasurementSystem']
  };

  return result;
}

function ValidateDoLayoutInputs(inputs) {
  if (inputs == false) {
    return false;
  }

  if (inputs.azimuth == undefined || inputs.azimuth === '') {
    console.log('Can\'t generate layout: roof azimuth is not defined');
    return false;
  }

  if (inputs.tilt == undefined || inputs.tilt === '') {
    console.log('Can\'t generate layout: roof tilt is not defined');
    return false;
  }

  if (inputs.setback == undefined || inputs.setback === '') {
    console.log('Can\'t generate layout: roof setback is not defined');
    return false;
  }

  if (inputs.roofHeight == undefined || inputs.roofHeight === '') {
    console.log('Can\'t generate layout: roof height is not defined');
    return false;
  }

  return true;
}

/**
 * Sends request to layout service and renders module layout
 * Parameters: N2Nspacing - North to North spacing, EWspacing - East West spacing, PVModuleId - PV Module ID from db
 * Returns: array of added modules
 */
LayoutControl.prototype.DoLayout = function(N2Nspacing, EWspacing, PVModuleId, rackingId, orientation, ModuleTilt, desiredSystemSize, isStaggered, moduleColor, args, callback, errCb) {
  var inputs = [],
    parapetObstructions = [],
    selectedObject,
    lc = this;

  if (args != undefined && typeof args == "object") {
    inputs.push(GetDoLayoutInputsFromArgs(args, lc));
  } else {
    if (this.selectedObjects.length == 0) {
      this.GetRoofs().forEach(function(roof) {
        inputs.push(GetDoLayoutInputsFromSelectedObject(roof, lc));
      });
    } else {
      var allRoofs = true;
      this.selectedObjects.forEach(function(object) {
        if (allRoofs && object.data instanceof Roof == false) {
          allRoofs = false;
        }
      });
      if (!allRoofs) {
        this.GetRoofs().forEach(function(roof) {
          inputs.push(GetDoLayoutInputsFromSelectedObject(roof, lc));
        });
      } else {
        this.selectedObjects.forEach(function(object) {
          selectedObject = object.data;
          inputs.push(GetDoLayoutInputsFromSelectedObject(selectedObject, lc));
        });
      }

    }
  }

  lc.moduleColor = moduleColor;

  var valid = true;
  inputs.forEach(function(input) {
    if (!valid)
      return;
    if (!ValidateDoLayoutInputs(input)) {
      console.log('Invalid input parameters for DoLayout operation');
      valid = false;
      return;
    }
  });
  if (!valid)
    return false;

  var lc = this;
  var project = lc.GetProject();
  project.roofs.forEach(function(roof) {
    delete roof.modules;

    delete roof.rails;

    delete roof.splices;

    delete roof.attachments;

    if (roof.parapetWallObjects) {
      parapetObstructions = parapetObstructions.concat(angular.copy(roof.parapetWallObjects));
    }
    delete roof.parapetWallObjects;

    if (roof.roofFaces[0].layouts) {
      roof.roofFaces[0].layouts.forEach(function(layout) {
        delete layout.layoutUnits;
      });
    }
  });

  var project = angular.copy(project);

  project.roofs.forEach(function(roof) {

    roof.modules = [];

    //Convert azimuth from North directed to South
    var azimuth = 180 - parseFloat(roof.roofFaces[0].azimuth);
    if (azimuth > 180)
      azimuth -= 360;

    if ((isNaN(roof.meanHeight) || roof.meanHeight == undefined || roof.meanHeight == '') && roof.meanHeight !== 0) {
      roof.meanHeight = 0;
    }

    if ((isNaN(roof.setback) || roof.meanHeight == setback || roof.setback == '') && roof.setback !== 0) {
      roof.setback = 0;
    }

    if ((isNaN(roof.roofFaces[0].slope) || roof.roofFaces[0].slope == undefined || roof.roofFaces[0].slope == '') && roof.roofFaces[0].slope !== 0) {
      roof.roofFaces[0].slope = 0;
    }

    //Convert feet to inches
    var setback = roof.setback;
    if (lc.measurementSystem == 3) {
      setback = lc.ConvertNumberFromTo(roof.setback, 2, 3, 5);
    }

    roof.roofFaces[0].azimuth = azimuth;
    roof.setback = setback;
  });

  parapetObstructions.forEach(function(item) {
    var parapet = item.data;
    parapet.parentRoof = parapet.parentRoof.index;
    parapet.shape = parapet.primitive.shapeType;
    delete parapet.primitive;
    project.obstructions.push(parapet);
  });
  project.obstructions.forEach(function(obstruction) {
    if ((isNaN(obstruction.height) || obstruction.height == undefined || obstruction.height == '') && obstruction.height !== 0) {
      obstruction.height = 0;
    }

    if ((isNaN(obstruction.setback) || obstruction.setback == undefined || obstruction.setback == '') && obstruction.setback !== 0) {
      obstruction.setback = 0;
    }

    //Convert feet to inches
    var setback = obstruction.setback;
    if (lc.measurementSystem == 3) {
      setback = lc.ConvertNumberFromTo(obstruction.setback, 2, 3, 5);
    }
    obstruction.setback = setback;
  });

  var waitingForRequestsNumber = inputs.length;

  // If no roofs selected or all roof selected,
  // remove roof name from request to calculate
  if (this.selectedObjects.length == 0 || this.selectedObjects.length == this.GetRoofs().length) {

    var input = inputs[0];
    delete input.roofArea;

    inputs = [
      input
    ];

    waitingForRequestsNumber = this.GetRoofs().length;
  }

  var affectedRoofs = [];
  inputs.forEach(function(input) {
    var dataToSend = {
      Project: project,
      Roofname: input.roofArea != undefined ? input.roofArea.name : undefined,
      //      InstallAreaName: input.installationArea != undefined ? input.installationArea.name : undefined,
      ModuleId: PVModuleId,
      RackingId: rackingId,
      EastWestSpacing: EWspacing,
      RowSpacing: N2Nspacing,
      Orientation: orientation,
      ModuleTilt: ModuleTilt,
      DesiredSystemSizeW: desiredSystemSize,
      IsStaggered: isStaggered,
      IsRequestFromPVSketch: true
    };

    // console.log(JSON.stringify(dataToSend));
    $.ajax({
      url: lc.APIServiceUrl,
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(dataToSend),
      dataType: 'json',
      success: function(data) {
        var roofsToReturn = [];
        data.roofs.forEach(function(roof) {
          if (dataToSend.Roofname != null && (roof.name != dataToSend.Roofname)) {
            return;
          }
          var projectRoof = lc.GetRoofByIndex(roof.index);

          if (roof.Rails) {
            var railsNum = roof.Rails.length;
            for (var r = 0; r < railsNum; r++) {
              var line = [
                new google.maps.LatLng(roof.Rails[r][0].y, roof.Rails[r][0].x),
                new google.maps.LatLng(roof.Rails[r][1].y, roof.Rails[r][1].x)
              ];
              new google.maps.Polyline({
                path: line,
                geodesic: true,
                strokeColor: layoutControlElementColors.Obstruction,
                strokeOpacity: 1,
                strokeWeight: 2,
                zIndex: 15000,
                map: lc.map
              });
            }
          }
          projectRoof.ClearModules(lc);
          var moduleCount = roof.modules.length;
          for (var i = 0; i < moduleCount; i++) {
            roof.modules[i].azimuth = projectRoof.roofFaces[0].azimuth;
            projectRoof.AddModule(roof.modules[i].index, roof.modules[i].polygon2dLocal, roof.modules[i].groupIndex, roof.modules[i].isOpposite, roof.modules[i].RowIndex, roof.modules[i].Neighbours, lc);
          }
          roofsToReturn.push(roof);
        });
        affectedRoofs = affectedRoofs.concat(roofsToReturn);
        if (callback != undefined && affectedRoofs.length == waitingForRequestsNumber) {
          callback(affectedRoofs);
        }
      },
      complete: function() {
        lc.DeselectObject();
        lc.DeselectModules();
      },
      error: function(error) {
        console.log(error);
        if (typeof errCb == 'function')
          errCb(error);
      }
    });
  });
};

function CheckDoLayoutArgs(args) {
  return typeof args == "object" && args['Roof'] != undefined;
}

LayoutControl.prototype.DoRails = function(tilt, rackingId, moduleId, deselect, callback, errCb) {
  var lc = this;

  lc.GetRoofs().forEach(function(roof) {
    roof.ClearRails(lc);

    lc.GetRackingConfiguration(roof, tilt, rackingId, moduleId,
      function(data) {
        if (data.Rails) {
          data.Rails.forEach(function(rail) {
            roof.AddRail(lc, rail);

            //Add Splices
            if (rail.Splices) {
              rail.Splices.forEach(function(splice) {
                roof.AddSplice(lc, splice);
              });
            }

            //Add Mount Points
            if (rail.MountPoints) {
              rail.MountPoints.forEach(function(mountPoint) {
                roof.AddAttachment(lc, mountPoint);
              });
            }
          });

          if (callback != undefined) {
            callback(data);
          }
        }
      },
      function() {
        //completeCb
        if (!deselect) {
          lc.DeselectObject();
          lc.DeselectModules();
        }
      }
    );
  });
};

LayoutControl.prototype.GetRackingConfiguration = function(roof, tilt, rackingId, moduleId, callback, completeCb, errCb) {
  var lc = this;

  var formattedModules = [];

  for (var i = 0; i < roof.modules.length; i++) {
    var moduleToPush = {
      index: roof.modules[i].index,
      polygon2dLocal: roof.modules[i].polygon2dLocal,
      groupIndex: roof.modules[i].groupIndex,
      isOpposite: roof.modules[i].isOpposite,
      RowIndex: roof.modules[i].rowIndex,
      Neighbours: roof.modules[i].neighbours
    };
    formattedModules.push(moduleToPush);
  }

  var dataToSend = {
    RoofContour: roof.roofFaces[0].polygon2dLocal.vertices,
    Layout: formattedModules,
    MeanHeight: roof.meanHeight,
    Setback: roof.setback,
    RailType: "IronRidge",
    MountingSystemId: rackingId,
    ModuleId: moduleId,
    RafterSpacing: 0,
    TiltAngle: tilt,
    RoofSlope: roof.roofFaces[0].slope,
    DoRails: true,
    DoMountingPoints: true,
    DoSplices: true,
    DoCouplers: true
  };

  $.ajax({
    url: lc.rackingConfigurationServiceUrl,
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(dataToSend),
    dataType: 'json',
    success: function(data) {
      if (callback != undefined) {
        callback(data);
      }
    },
    complete: function() {
      if (typeof completeCb == 'function')
        completeCb();
    },
    error: function(error) {
      console.log(error);
      if (typeof errCb == 'function')
        errCb(error);
    }
  });
};

/**
 * Converts latlong points of given primitive to meters
 * Prameters: primitive to calculate
 * Return: object with startingPoint coordinates and points in meters
 */
LayoutControl.prototype.GetPointsInMetersFromPolygon2D = function(polygon2d, startingPoint) {
  var lc = this;
  if (startingPoint == undefined || startingPoint == null) {
    // Getting startingPoint
    // get x for starting point
    var startingPointX = polygon2d.vertices[0].x;
    polygon2d.vertices.forEach(function(vertex) {
      if (vertex.x < startingPointX) {
        startingPointX = parseFloat(vertex.x);
      }
    });
    // get y for starting point
    var startingPointY = polygon2d.vertices[0].y;
    polygon2d.vertices.forEach(function(vertex) {
      if (vertex.y > startingPointY) {
        startingPointY = parseFloat(vertex.y);
      }
    });
  } else {
    startingPointX = parseFloat(startingPoint.x);
    startingPointY = parseFloat(startingPoint.y);
  }
  var p1, p2;
  var pointsInMeters = [];
  polygon2d.vertices.forEach(function(vertex) {
    p1 = new google.maps.LatLng(startingPointY, startingPointX);
    p2 = new google.maps.LatLng(startingPointY, vertex.x);
    var x = lc.ConvertNumberFromMeters(google.maps.geometry.spherical.computeDistanceBetween(p1, p2), lc.measurementSystem);
    if (p2.lng() < p1.lng())
      x = -x;
    p1 = new google.maps.LatLng(startingPointY, startingPointX);
    p2 = new google.maps.LatLng(vertex.y, startingPointX);
    var y = lc.ConvertNumberFromMeters(google.maps.geometry.spherical.computeDistanceBetween(p1, p2), lc.measurementSystem);
    if (p2.lat() > p1.lat())
      y = -y;
    pointsInMeters.push({
      x: x,
      y: y
    });
  });
  return {
    startingPoint: {
      x: startingPointX,
      y: startingPointY
    },
    points: pointsInMeters
  };
}

LayoutControl.prototype.GetPointsInLatLngFromMeters = function(startingPoint, points) {
  var latLngSP = new google.maps.LatLng(startingPoint.y, startingPoint.x);
  var toReturn = [];
  var lc = this;
  points.forEach(function(point, index) {
    point.x = lc.ConvertNumberTo(point.x, 1);
    point.y = lc.ConvertNumberTo(point.y, 1);
    var new_latitude = google.maps.geometry.spherical.computeOffset(latLngSP, point.y, 180).lat();
    var new_longitude = google.maps.geometry.spherical.computeOffset(latLngSP, point.x, 90).lng();
    //new google.maps.Marker({
    //    position: new google.maps.LatLng(new_latitude, new_longitude),
    //    map: lc.map,
    //    title: 'Hello World!'
    //});
    toReturn.push({
      y: new_latitude,
      x: new_longitude
    });
  });
  return toReturn;
}

/**
 * Set's module number of given installation area
 *
 */
LayoutControl.prototype.SetModuleNumber = function(number, parentInstallationArea) {
  // exit function with false response, if given number is lower than 0
  if (number < 0) {
    console.log("Can't set module number: given number is lower than 0");
    return false;
  }
  // Set parentInstallationArea var to default - currently selected object
  // If null given from function call
  if (parentInstallationArea === null || parentInstallationArea === undefined) {
    parentInstallationArea = this.selectedObject.data;
  }
  // exit function with false response, if given object is not installation area
  if (parentInstallationArea instanceof InstallationArea === false) {
    console.log("Can't set module number: Given object is not Installation Area");
    return false;
  }
  // exit function with false response, if given installation area doesn't
  // have generated modules
  if (parentInstallationArea.modules.length === 0) {
    console.log("Can't set module number: Given Installation Area doesn't have modules");
    return false;
  }
  // exit function with false response, if given installation area's module number
  // is lower, than given number
  if (parentInstallationArea.modules.length < number) {
    console.log("Can't set module number: number of modules is to high(maximum is " + parentInstallationArea.modules.length + ")");
    return false;
  }
  var lc = this;
  for (i = 0; i < parentInstallationArea.modules.length; i++) {
    var module = parentInstallationArea.modules[i];
    module.UnfillModule(lc);
  }
  for (i = 0; i < number; i++) {
    var module = parentInstallationArea.modules[i];
    module.FillModule(lc);
  }
}


/**
 * Add Roofs on to map from given array
 *
 */
LayoutControl.prototype.AddRoofs = function(roofs) {
  var lc = this;
  if (roofs === 'undefined' || roofs.length == 0) {
    return;
  }

  roofs.forEach(function(item) {
    lc.AddRoof(item, false);
  });

  this.RefreshProjectMinLinesDistances();
  this.UpdateObjectSelection();
};

LayoutControl.prototype.AddRoof = function(roof, updateSelection) {
  var lc = this;

  if (roof === 'undefined') {
    return;
  }

  roof.__proto__ = Roof.prototype;
  var color = layoutControlElementColors.Roof;
  var primitive = lc.AddPrimitive(roof.roofFaces[0].polygon2dLocal, color, color, layoutControlElementZIndexes.Roof, null, null, null, roof.shapeType);
  roof.primitive = primitive;
  roof.azimuthArrow = roof.GetAzimuthArrow(lc, primitive, roof.roofFaces[0].azimuth);
  roof.AddChangeEventListeners(primitive, lc);
  roof.primitive.shapeType = roof.primitive.shapeType;
  delete roof.shapeType;
  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
  var roofObj = new mapObject(idx, "Roof", primitive, roof);
  google.maps.event.addListener(roofObj.primitive, 'click', function() {
    lc.ObjectSelected(roofObj);
  });

  google.maps.event.addListener(roofObj.primitive, 'drag', function() {
    lc.ObjectDraged(roofObj);
  });

  google.maps.event.addListener(roofObj.primitive, 'dragstart', function() {
    lc.ObjectDragStart(roofObj);
  });

  google.maps.event.addListener(roofObj.primitive, 'dragend', function() {
    lc.ObjectDragEnd(roofObj);
  });

  roof.index = roof.index == undefined ? lc.lastIndexes.Roof = lc.lastIndexes.Roof + 1 : roof.index;
  if (roof.index > lc.lastIndexes.Roof)
    lc.lastIndexes.Roof = roof.index;
  lc.objects.push(roofObj);

  lc.setObjectZIndex(lc.objects, roofObj);

  if (updateSelection) {
    this.UpdateObjectSelection();
  }

  return roof;
};

/**
 * Add Obstruction onto map
 *
 */
LayoutControl.prototype.AddObstruction = function(obstruction, updateSelection) {
  var lc = this;
  if (obstruction === 'undefined') {
    return;
  }

  //console.log('add obstruction');

  obstruction.__proto__ = Obstruction.prototype;
  obstruction.shapeType = "circle";
  var color = layoutControlElementColors.Obstruction;
  var primitive = lc.AddPrimitive(obstruction.polygon2dLocal, color, color, layoutControlElementZIndexes.Obstruction, 2, null, null, obstruction.shapeType);
  obstruction.primitive = primitive;
  delete obstruction.shapeType;
  obstruction.AddChangeEventListeners(primitive, lc);
  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
  var obs = new mapObject(idx, "Obstruction", primitive, obstruction);
  google.maps.event.addListener(obs.primitive, 'click', function() {
    lc.ObjectSelected(obs);
  });

  google.maps.event.addListener(obs.primitive, 'drag', function() {
    lc.ObjectDraged(obs);
  });

  google.maps.event.addListener(obs.primitive, 'dragstart', function() {
    lc.ObjectDragStart(obs);
  });

  google.maps.event.addListener(obs.primitive, 'dragend', function() {
    lc.ObjectDragEnd(obs);
  });

  obstruction.index = obstruction.index == undefined ? lc.lastIndexes.Obstruction = lc.lastIndexes.Obstruction + 1 : obstruction.index;
  if (obstruction.index > lc.lastIndexes.Obstruction)
    lc.lastIndexes.Obstruction = obstruction.index;
  lc.objects.push(obs);

  if (updateSelection) {
    this.UpdateObjectSelection();
  }
  return obstruction;
};

/**
 * Add Obstructions onto map from given array
 *
 */
LayoutControl.prototype.AddObstructions = function(obstructions) {
  var lc = this;
  if (obstructions === 'undefined' || obstructions.length == 0) {
    return;
  }


  obstructions.forEach(function(item) {
    lc.AddObstruction(item);
  });
}

/**
 * Add InstallationArea onto map
 *
 */
LayoutControl.prototype.AddInstallationArea = function(installationArea, updateSelection) {
  var lc = this;
  if (installationArea === 'undefined') {
    return;
  }

  installationArea.__proto__ = InstallationArea.prototype;
  installationArea.modules = [];
  var color = layoutControlElementColors.InstallationArea;
  var primitive = lc.AddPrimitive(installationArea.polygon2dLocal, color, color, 1000);
  installationArea.primitive = primitive;
  installationArea.primitive.shapeType = "polygon";
  installationArea.AddChangeEventListeners(primitive, lc);
  var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
  var installationArea = new mapObject(idx, "InstallationArea", primitive, installationArea);
  google.maps.event.addListener(installationArea.primitive, 'click', function() {
    lc.ObjectSelected(installationArea);
  });
  installationArea.index = installationArea.index == undefined ? lc.lastIndexes.InstallationArea = lc.lastIndexes.InstallationArea + 1 : installationArea.index;
  if (installationArea.index > lc.lastIndexes.InstallationArea)
    lc.lastIndexes.InstallationArea = installationArea.index;

  lc.objects.push(installationArea);
  if (updateSelection) {
    this.UpdateObjectSelection();
  }
  return installationArea;
}

/**
 * Add Shadow onto map
 *
 */
LayoutControl.prototype.AddShadows = function(data) {
  var lc = this;
  if (data === 'undefined') {
    return;
  }

  lc.ClearShadows();

  if (data) {
    var shadowNum = data.length;
    for (var i = 0; i < shadowNum; i++) {
      var color = layoutControlElementColors.Shadow;
      var shadow = data[i].Shadow;
      shadow.__proto__ = Shadow.prototype;

      if (data[i].Roof) {
        //Do not draw shadows for roofs
        continue;

        shadow.parentObject = data[i].Roof;
      }
      if (data[i].Obstruction) {
        shadow.parentObject = data[i].Obstruction;
      }

      var shadowPointCount = shadow.vertices.length;
      var polygonPoints = [];
      for (var k = 0; k < shadowPointCount; k++) {
        var point = shadow.vertices[k];
        polygonPoints.push(new google.maps.LatLng(point.y, point.x));
      }

      var polygon = lc.canvas.createPolygon({
        path: polygonPoints,
        strokeColor: layoutControlElementColors.Shadow,
        fillColor: layoutControlElementColors.Shadow,
        strokeOpacity: 0.5,
        strokeWeight: 0,
        fillOpacity: 0.5,
        zIndex: layoutControlElementZIndexes.Shadow
      });

      //      var primitive = lc.AddPrimitive(shadow, color, color, layoutControlElementZIndexes.Shadow);
      //      shadow.primitive = primitive;
      //      shadow.primitive.shapeType = "polygon";
      //      shadow.primitive.setOptions({clickable: false});

      var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
      var shadow = new mapObject(idx, "Shadow", polygon, shadow);
      shadow.index = shadow.index == undefined ? lc.lastIndexes.Shadow = lc.lastIndexes.Shadow + 1 : shadow.index;
      if (shadow.index > lc.lastIndexes.Shadow)
        lc.lastIndexes.Shadow = shadow.index;

      lc.shadows.push(shadow);
    }
  }
};

LayoutControl.prototype.AddRails = function(rails, roofIndex) {
  var lc = this;
  if (rails === 'undefined') {
    return;
  }

  if (rails) {
    var railsNum = rails.length;
    for (var r = 0; r < railsNum; r++) {
      var line = [
        new google.maps.LatLng(rails[r][0].y, rails[r][0].x),
        new google.maps.LatLng(rails[r][1].y, rails[r][1].x)
      ];
      new google.maps.Polyline({
        path: line,
        geodesic: true,
        strokeColor: layoutControlElementColors.Rails,
        strokeOpacity: 1,
        strokeWeight: 2,
        zIndex: 15000,
        map: lc.map
      });
    }
  }
}

/**
 * Add InstallationAreas onto map from given array
 *
 */
LayoutControl.prototype.AddInstallationAreas = function(installationAreas) {
  var lc = this;
  if (installationAreas == undefined || installationAreas.length == 0) {
    return;
  }

  installationAreas.forEach(function(item) {
    lc.AddInstallationArea(item);
  });
};

/**
 * Returns primitive object
 *
 */
LayoutControl.prototype.AddPrimitive = function(polygon2D, strokeColor, fillColor, zOrder, strokeWeight, strokeOpacity, fillOpacity, shapeType) {
  var lc = this;
  // Set default strokeWeight if nothing given
  if (strokeWeight == null || strokeWeight == undefined) {
    strokeWeight = 2;
  }
  // Set default shapeType if nothing given
  if (shapeType == null || shapeType == undefined) {
    shapeType = "polygon";
  }
  //set default opacity variables
  if (strokeOpacity == null || strokeOpacity == undefined) {
    strokeOpacity = 0.9;
  }
  if (fillOpacity == null || fillOpacity == undefined) {
    fillOpacity = 0.3;
  }
  //set default zOrder
  if (zOrder == null || zOrder == undefined) {
    zOrder = 1;
  }

  var options = {
    strokeColor: strokeColor,
    strokeOpacity: strokeOpacity,
    strokeWeight: strokeWeight,
    fillColor: fillColor,
    fillOpacity: fillOpacity,
    zIndex: zOrder
  };

  var primitive;
  switch (shapeType) {
    case "polygon":
      options.paths = GetPrimitivePath(polygon2D);
      primitive = new google.maps.Polygon(options);
      break;
    case "rectangle":
      var northEast = new google.maps.LatLng(polygon2D.vertices[1].y, polygon2D.vertices[1].x);
      var southWest = new google.maps.LatLng(polygon2D.vertices[3].y, polygon2D.vertices[3].x);
      var bounds = new google.maps.LatLngBounds(southWest, northEast);
      options.bounds = bounds;
      primitive = new google.maps.Rectangle(options);
      break;
    case "circle":
      var centerLat = polygon2D.vertices[0].y - (polygon2D.vertices[0].y - polygon2D.vertices[8].y) / 2;
      var centerPoint = new google.maps.LatLng(centerLat, polygon2D.vertices[0].x);
      var zeroPoint = new google.maps.LatLng(polygon2D.vertices[0].y, polygon2D.vertices[0].x);
      var radius = google.maps.geometry.spherical.computeDistanceBetween(centerPoint, zeroPoint);
      options.center = centerPoint;
      options.radius = radius;
      primitive = new google.maps.Circle(options);
      break;
  }
  primitive.shapeType = shapeType;
  primitive.setMap(this.map);
  return primitive;
};

LayoutControl.prototype.IsPolygonOverlapOtherPolygons = function(checkingPolygon2D, otherPolygons2D) {
  var bOverlaps = false;
  otherPolygons2D.forEach(function(polygon) {
    if (bOverlaps) {
      return;
    }
    var p1 = ConvertPolygon2DToVELatLong(checkingPolygon2D);
    var p2 = ConvertPolygon2DToVELatLong(polygon);

    if (ArePolygonsOverlapped(p1, p2)) {
      bOverlaps = true;
      return;
    }
    if (IsPolygonInsideOtherPolygon(checkingPolygon2D, polygon)) {
      bOverlaps = true;
    }
  });
  return bOverlaps;
};

LayoutControl.prototype.setObjectZIndex = function(objects, newObject, newObjecyType) {
  if (!objects || objects.length === 0)
    return;

  var lc = this;

  if (!newObjecyType) {
    newObjecyType = newObject.type;
  }
  if (!newObjecyType) {
    if (newObject instanceof Roof)
      newObjecyType = 'Roof';
    else if (newObject instanceof Obstruction)
      newObjecyType = 'Obstruction';
  }

  var index = newObject.primitive.zIndex;
  if (newObjecyType.toLowerCase() === 'roof') {
    index = layoutControlElementZIndexes.Roof;
  }

  var newObjectAreaPrimitive = newObject.primitive;
  if (newObjectAreaPrimitive.shapeType == 'circle') {
    newObjectAreaPrimitive = lc.AddPrimitive(GetPolygon2D(newObjectAreaPrimitive), '#ffffff', '#ffffff', 0, 1, 1, 1, 'polygon');
    newObjectAreaPrimitive.setMap(null);
  }

  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];

    var objectAreaPrimitive = object.primitive;
    if (objectAreaPrimitive.shapeType == 'circle') {
      objectAreaPrimitive = lc.AddPrimitive(GetPolygon2D(newObjectAreaPrimitive), '#ffffff', '#ffffff', 0, 1, 1, 1, 'polygon');
      objectAreaPrimitive.setMap(null);
    }

    var newObjectArea = google.maps.geometry.spherical.computeArea(newObjectAreaPrimitive.getPath());
    var currentObjectArea = google.maps.geometry.spherical.computeArea(objectAreaPrimitive.getPath());

    if (newObjectArea < currentObjectArea && index < object.primitive.zIndex) {
      index = object.primitive.zIndex;
    }
  }

  for (var i = 0; i < objects.length; i++) {
    var object = objects[i];
    if (object.type.toLowerCase() === newObjecyType.toLowerCase() === 'roof') {
      var zIndex = object.primitive.zIndex;
      if (zIndex > index) {
        object.primitive.setOptions({
          zIndex: zIndex + 1
        });
      }
    }
  }

  newObject.primitive.setOptions({
    zIndex: index + 1
  });
};

LayoutControl.prototype.IsPolygonInsideOtherPolygon = function(checkingPolygon2D, otherPolygon2D) {
  return IsPolygonInsideOtherPolygon(checkingPolygon2D, otherPolygon2D);
};

LayoutControl.prototype.GetPolygon2DListFromObjectList = function(objects) {
  var polygon2dList = [];
  objects.forEach(function(object) {
    if (object instanceof Roof) {
      polygon2dList.push(object.roofFaces[0].polygon2dLocal);
      return;
    }
    polygon2dList.push(object.polygon2dLocal);
  });
  return polygon2dList;
}

LayoutControl.prototype.DeleteObjectByDataInstance = function(instance) {
  var found = false;
  var objects = this.objects;
  objects.forEach(function(object, index) {
    if (found) {
      return;
    }
    if (object.data == instance) {
      objects.splice(index, 1);
    }
  });
};

LayoutControl.prototype.RefreshMarker = function() {
  this.marker.setPosition(new google.maps.LatLng(this.center.lat, this.center.lng));
  this.marker.setVisible(this.markerVisible);
};

LayoutControl.prototype.SetMarkerVisible = function(visible) {
  if (typeof visible !== 'boolean') {
    return null;
  }
  this.markerVisible = visible;
  this.RefreshMarker();
};

LayoutControl.prototype.SetObjectColorType = function(objectType) {
  if (objectType == undefined) {
    $(".layoutToolPanel span.objectColor").css('background-color', 'transparent');
    return;
  }
  $(".layoutToolPanel span.objectColor").css('background-color', layoutControlElementColors[objectType]);
};

LayoutControl.prototype.SetActiveBtn = function(objectType) {
  var lc = this;
  var onDrawingModeChange = {
    type: "onDrawingModeChange",
    mode: objectType
  };
  lc.dispatchEvent(onDrawingModeChange);
  $('.addRoofBtn').removeClass('active');
  $('.addObstructionBtn').removeClass('active');
  $('.addInstallationAreaBtn').removeClass('active');
  if (objectType === 'Roof' || objectType === 'Obstruction' || objectType === 'InstallationArea')
    $('.add' + objectType + 'Btn').addClass('active');
};

LayoutControl.prototype.UpdateForMeasurementSystem = function(newSystem) {
  var lc = this;
  this.GetRoofs().forEach(function(roof) {
    roof.meanHeight = lc.ConvertNumberTo(roof.meanHeight, newSystem);
    //Convert feet to inches
    if (lc.measurementSystem != newSystem) {
      if (newSystem == 1) {
        roof.setback = lc.ConvertNumberFromTo(roof.setback, 2, newSystem, 4);
        roof.purlinSpacing = lc.ConvertNumberFromTo(roof.purlinSpacing, 2, newSystem, 4);
        roof.parapetWallHeight = lc.ConvertNumberFromTo(roof.parapetWallHeight, 2, newSystem, 4);
        roof.leastHorizontalDistance = lc.ConvertNumberFromTo(roof.leastHorizontalDistance, 2, newSystem, 4);
        roof.aZone = lc.ConvertNumberFromTo(roof.aZone, 2, newSystem, 4);
        roof.customSetbacks.forEach(function(item, index, array) {
          array[index] = lc.ConvertNumberFromTo(item, 2, newSystem, 4)
        });
      } else {
        roof.setback = lc.ConvertNumberFromTo(roof.setback, lc.measurementSystem, 2, 0);
        roof.purlinSpacing = lc.ConvertNumberFromTo(roof.purlinSpacing, lc.measurementSystem, 2, 0);
        roof.parapetWallHeight = lc.ConvertNumberFromTo(roof.parapetWallHeight, lc.measurementSystem, 2, 0);
        roof.leastHorizontalDistance = lc.ConvertNumberFromTo(roof.leastHorizontalDistance, lc.measurementSystem, 2, 0);
        roof.aZone = lc.ConvertNumberFromTo(roof.aZone, 2, lc.measurementSystem, 2, 0);
        roof.customSetbacks.forEach(function(item, index, array) {
          array[index] = lc.ConvertNumberFromTo(item, lc.measurementSystem, 2, 0)
        });
      }
    }
  });
  this.GetObstructions().forEach(function(obstruction) {
    obstruction.height = lc.ConvertNumberTo(obstruction.height, newSystem);
    //Convert feet to inches
    if (lc.measurementSystem != newSystem) {
      if (newSystem == 1) {
        obstruction.setback = lc.ConvertNumberFromTo(obstruction.setback, 2, newSystem, 0);
      } else {
        obstruction.setback = lc.ConvertNumberFromTo(obstruction.setback, lc.measurementSystem, 2, 0);
      }
    }
  });

  $('.layoutToolPanel_panel .measurementSystem').each(function() {
    $(this).text('(' + layoutControlMeasurementSystems[newSystem].displayName + ')');
  });
  this.UpdatePropertyPanel();
};

LayoutControl.prototype.SetMeasurementSystem = function(newSystem) {
  if (layoutControlMeasurementSystems[newSystem] == undefined) {
    return false;
  }
  this.UpdateForMeasurementSystem(newSystem);
  this.measurementSystem = newSystem;
  var lc = this;
  var onMeasurementSystemChanged = {
    type: "onMeasurementSystemChanged",
    newSystem: lc.measurementSystem
  };
  this.dispatchEvent(onMeasurementSystemChanged);
  lc.ObjectSelected(lc.selectedObject);

  if (lc.mapLoaded)
    lc.GetRoofs().forEach(function(roof) {
      roof.RefreshDistanceLabels(lc);
    });
};

LayoutControl.prototype.ConvertNumberTo = function(number, targetSystem) {
  return this.ConvertNumberFromTo(number, this.measurementSystem, targetSystem);
};

LayoutControl.prototype.ConvertNumberFromMeters = function(number, targetSystem, rounding) {
  return this.ConvertNumberFromTo(number, 1, targetSystem, rounding);
};

LayoutControl.prototype.ConvertNumberFromTo = function(number, oldSystem, targetSystem, rounding) {
  if (rounding == undefined)
    rounding = 4;

  if (typeof number == 'string') {
    number = parseFloat(number);
  }
  if (oldSystem == targetSystem) {
    return parseFloat(number.toFixed(rounding));
  }
  if (oldSystem != 1) {
    if (targetSystem != 1) {
      numberInMeters = number * (layoutControlMeasurementSystems[1].convertFactor / layoutControlMeasurementSystems[oldSystem].convertFactor);
      return parseFloat((numberInMeters * layoutControlMeasurementSystems[targetSystem].convertFactor).toFixed(rounding));
    }
    return parseFloat((number * (layoutControlMeasurementSystems[1].convertFactor / layoutControlMeasurementSystems[oldSystem].convertFactor)).toFixed(rounding));
  }
  return parseFloat((number * layoutControlMeasurementSystems[targetSystem].convertFactor).toFixed(rounding));
};

LayoutControl.prototype.InitializeDrawingPoly = function(drawingManager) {
  this.drawingPoly = new google.maps.Polygon({
    strokeWeight: 0,
    fillColor: layoutControlElementColors.Drawing,
    fillOpacity: 0.4
  });

  this.drawingPoly.setMap(this.map);
  this.drawingPath = new google.maps.MVCArray;
  this.drawingPoly.setPaths(new google.maps.MVCArray([this.drawingPath]));

  var lc = this;
  var map = this.map;
  $(map.container).mousedown(function(e) {
    map.cursorMoved = false;
    $(map.container).mousemove(function(e) {
      if (!map.cursorMoved) {
        map.cursorMoved = true;
      }
    });
  });
  $(map.container).mouseup(function() {
    if (map.cursorMoved) {
      return;
    }
    lc.AddDrawingPolyPoint();
  });
};

LayoutControl.prototype.AddDrawingPolyPoint = function() {
  this.drawingPath.insertAt(this.drawingPath.length, this.map.cursorPos);
};

LayoutControl.prototype.RemoveDrawingPolygon = function() {
  this.drawingPoly.setMap(null);
};

LayoutControl.prototype.GetProjectBounds = function() {
  var lc = this;
  var bounds = new google.maps.LatLngBounds();
  var roofs = this.GetRoofs();
  var outsideObstructions = this.outsideObstructions;

  roofs.forEach(function(roof) {
    var boundsPrimitive = roof.primitive;
    if (boundsPrimitive.shapeType == 'circle') {
      boundsPrimitive = lc.AddPrimitive(GetPolygon2D(boundsPrimitive), '#ffffff', '#ffffff', 0, 1, 1, 1, 'polygon');
      boundsPrimitive.setMap(null);
    }
    boundsPrimitive.getPath().forEach(function(path) {
      bounds.extend(path);
    });
  });

  outsideObstructions.forEach(function(obstruction) {
    var boundsPrimitive = obstruction.primitive;
    if (boundsPrimitive.shapeType == 'circle') {
      boundsPrimitive = lc.AddPrimitive(GetPolygon2D(boundsPrimitive), '#ffffff', '#ffffff', 0, 1, 1, 1, 'polygon');
      boundsPrimitive.setMap(null);
    }
    boundsPrimitive.getPath().forEach(function(path) {
      bounds.extend(path);
    });
  });

  return bounds;
};

LayoutControl.prototype.CenterByProject = function(callback, boundHeight, boundWidth) {
  var bounds = this.GetProjectBounds();
  var lat = bounds.getCenter().lat();
  var lng = bounds.getCenter().lng();

  if (boundHeight == undefined) {
    boundHeight = 50;
  }

  if (boundWidth == undefined) {
    boundWidth = 50;
  }

  if (lat != 0 || lng != -180) {
    this.SetCenter(lat, lng);

    //Set Zoom by bounds
    var $mapDiv = $('#' + this.containerId);
    var mapDim = {
      height: $mapDiv.height() - boundHeight,
      width: $mapDiv.height() - boundWidth
    };
    this.map.setZoom(getBoundsZoomLevel(bounds, mapDim));

    if (typeof callback == 'function') {
      var timeout = setTimeout(function() {
        callback();
      }, 3000);
      google.maps.event.addListenerOnce(this.map, 'idle', function() {
        clearTimeout(timeout);
        callback();
      });
    }
  } else {
    callback();
  }
};

LayoutControl.prototype.GetCaptureZone = function(offset) {
  var bounds = this.GetProjectBounds();

  var nePoint = fromLatLngToPoint(bounds.getNorthEast(), this.map);
  var swPoint = fromLatLngToPoint(bounds.getSouthWest(), this.map);

  var width = nePoint.x - swPoint.x + (offset * 2);
  var height = swPoint.y - nePoint.y + (offset * 2);

  var x = swPoint.x - offset;
  var y = nePoint.y - offset;

  if (height > width) {
    var mx = x + (width / 2);
    width = height;
    x = mx - (width / 2);
  } else {
    var my = y + (height / 2);
    height = width;
    y = my - (height / 2);
  }

  return {
    x: parseFloat(x.toFixed(0)),
    y: parseFloat(y.toFixed(0)),
    width: parseFloat(width.toFixed(0)),
    height: parseFloat(height.toFixed(0))
  }
};

LayoutControl.prototype.BlockInteraction = function() {
  if ($('#interaction-block').length == 0)
    $('#mapContainer').parent().prepend('<div id="interaction-block" style="position: absolute; width: 100%; height: 100%; opacity: 0; z-index: 500;cursor:crosshair"></div>');
};

LayoutControl.prototype.UnblockInteraction = function() {
  if ($('#interaction-block').length != 0)
    $('#interaction-block').remove();
};

LayoutControl.prototype.ChangeInteractionMode = function(interactionMode, bDoNotDeselect) {
  var lc = this;
  var found = lc.interactionModes.indexOf(interactionMode);
  if (found == -1) {
    var excMsg = 'Cannot change interaction mode - Interaction mode can be: ';
    lc.interactionModes.forEach(function(mode, key) {
      excMsg = excMsg + mode;
      if (key != lc.interactionModes.length - 1)
        excMsg = excMsg + ', ';
    });
    throw excMsg;
  }
  lc.interactionMode = interactionMode;

  lc.SetSnapAzimuth(false);

  lc.DisableRectangularSelection();

  //lc.UnblockInteraction();

  lc.map.setOptions({
    draggableCursor: null,
    draggable: true
  });

  lc.enableClickable(lc.objects);

  var roofs = lc.GetRoofs();
  var roofCount = roofs.length;

  if (lc.modulesVisible) {
    for (var k = 0; k < roofCount; k++) {
      var modules = roofs[k].modules;
      //lc.enableDraggable(modules);
      lc.enableClickable(modules);
    }
  }

  if (!bDoNotDeselect)
  //Deselects current selected object
    lc.DeselectObject();
  switch (interactionMode) {
    // SELECTION
    case lc.interactionModes[0]:
      if (lc.currentDrawingManager != null) {
        lc.currentDrawingManager.setDrawingMode(null);
        $('#' + lc.containerId).off('mousemove.drawing');
        lc.RefreshDrawDistanceLabel();
      }
      break;
    // DRAW_LINE
    case lc.interactionModes[1]:
      lc.StartDrawObject('polygon');
      break;
    // DRAW_CIRCLE
    case lc.interactionModes[2]:
      lc.StartDrawObject('circle');
      break;
    // PASTE_OBJECTS
    case lc.interactionModes[3]:
      if (lc.currentDrawingManager != null) {
        lc.currentDrawingManager.setDrawingMode(null);
      }
      lc.map.setOptions({
        draggableCursor: 'crosshair'
      });
      lc.disableClickable(lc.objects);
      break;
    // RECTANGULAR_SELECTION
    case lc.interactionModes[4]:
      if (lc.currentDrawingManager != null) {
        lc.currentDrawingManager.setDrawingMode(null);
        $('#' + lc.containerId).off('mousemove.drawing');
        lc.RefreshDrawDistanceLabel();
      }
      lc.InitRectangularSelection();
      break;
  }

  var onInteractionModeChanged = {
    type: "onInteractionModeChanged",
    interactionMode: interactionMode
  };
  this.dispatchEvent(onInteractionModeChanged);
};

LayoutControl.prototype.DisableRectangularSelection = function() {
  var lc = this;
  lc.enableClickable(lc.objects);
  var container = $('#' + lc.containerId);

  container.off('mousedown.rectSelection');
  container.off('mouseup.rectSelection');

  if (lc.rectSelectionRectangle != undefined) {
    lc.rectSelectionRectangle.setMap(null);
    lc.rectSelectionRectangle = null;
  }
  container.off('mousemove.rectSelection');
  container.off('mousemove.rectSelection');
};

LayoutControl.prototype.SelectObjectsByRectangularSelection = function() {
  var lc = this;
  if (lc.rectSelectionRectangle == undefined)
    return;

  var objects = lc.objects;
  var objectCount = objects.length;
  var selectionPolygon = GetPolygon2D(lc.rectSelectionRectangle);

  // if(!lc.modulesVisible){
  // for (var i = 0; i < objectCount; i++)
  // {
  //     if(objects[i].type === 'Obstruction'){
  //       var checkingPolygon = GetPolygon2D(objects[i].primitive);
  //       if (lc.IsPolygonOverlapOtherPolygons(selectionPolygon, [checkingPolygon]) || lc.IsPolygonInsideOtherPolygon(checkingPolygon, selectionPolygon))
  //       {
  //         lc.ObjectSelected(objects[i], true);
  //       }
  //     }
  //   }
  //   console.time('obstruction selection');
  // }

  var roofs = lc.GetRoofs();
  var roofCount = roofs.length;

  //if(lc.modulesVisible) {
  for (var k = 0; k < roofCount; k++) {
    var modules = roofs[k].modules;
    var modulesCount = modules.length;
    for (var i = 0; i < modulesCount; i++) {
      var checkingPolygon = GetPolygon2D(modules[i].primitive);
      if (lc.IsPolygonInsideOtherPolygon(checkingPolygon, selectionPolygon) || lc.IsPolygonOverlapOtherPolygons(selectionPolygon, [checkingPolygon])) {
        if (lc.shiftPressed) {
          if (lc.selectedModules.includes(modules[i])) {
            lc.ModuleSelected(modules[i], true, false);
          }
        } else {
          if (!lc.selectedModules.includes(modules[i])) {
            lc.ModuleSelected(modules[i], true, false);
          }
        }
      }
    }
  }

  console.timeEnd('modules selection');
  //}
};

LayoutControl.prototype.InitRectangularSelection = function() {
  var lc = this;

  lc.map.setOptions({
    draggableCursor: 'crosshair',
    gestureHandling: 'greedy'
  });

  //lc.BlockInteraction();

  lc.disableDraggable(lc.objects);
  lc.disableClickable(lc.objects);

  var roofs = lc.GetRoofs();
  var roofCount = roofs.length;

  // if(!lc.modulesVisible) {
  //   lc.disableDraggable(lc.objects);
  //   lc.disableClickable(lc.objects);
  // }
  //
  // if(lc.modulesVisible) {
  for (var k = 0; k < roofCount; k++) {
    var modules = roofs[k].modules;
    lc.disableDraggable(modules);
    lc.disableClickable(modules);
  }
  // }

  var container = $('#' + lc.containerId);

  lc.rectSelectionClicked = false;
  lc.rectSelectionStartLatLng = null;
  lc.rectSelectionRectangle = null;
  lc.rectSelectionStartX = null;
  lc.rectSelectionObjectsDeselected = false;
  container.off('mousedown.rectSelection').on('mousedown.rectSelection', function(e) {
    lc.rectSelectionClicked = true;
    enableMouseMove(lc);
    lc.rectSelectionStartLatLng = pixelToLatlng(e.offsetX, e.offsetY, lc.map);
    lc.rectSelectionStartX = e.offsetX;
    lc.rectSelectionObjectsDeselected = false;
  });

  container.off('mouseup.rectSelection').on('mouseup.rectSelection', function(e) {
    lc.rectSelectionClicked = false;
    disableMouseMove(lc);

    //setTimeout(function(){
    //lc.ChangeInteractionMode('SELECTION');
    //lc.UpdateObjectSelection();
    //lc.UnblockInteraction();
    //}, 2);
  });

  var enableMouseMove = function(lc) {
    lc.map.setOptions({
      draggable: false
    });
    var container = $('#' + lc.containerId);
    container.off('mousemove.rectSelection').on('mousemove.rectSelection', function(e) {
      if (!lc.rectSelectionClicked)
        return;

      if (!lc.rectSelectionObjectsDeselected) {
        lc.DeselectObject();
        lc.rectSelectionObjectsDeselected = true;
      }

      //lc.disableClickable(lc.objects);

      var currentLatLng = pixelToLatlng(e.offsetX, e.offsetY, lc.map);

      var start = null,
        end = null;



      if (e.offsetX > lc.rectSelectionStartX) {
        start = lc.rectSelectionStartLatLng;
        end = currentLatLng;
      } else {
        start = currentLatLng;
        end = lc.rectSelectionStartLatLng;
      }

      if (lc.rectSelectionRectangle == undefined) {
        lc.rectSelectionRectangle = new google.maps.Rectangle({
          strokeColor: layoutControlElementColors.RectangleArea,
          fillColor: layoutControlElementColors.RectangleArea,
          strokeWeight: 1,
          zIndex: 10000,
          map: lc.map,
          clickable: false,
          bounds: new google.maps.LatLngBounds(start, end)
        });
        lc.rectSelectionRectangle.shapeType = 'rectangle';
      } else {
        lc.rectSelectionRectangle.setBounds(new google.maps.LatLngBounds(start, end));
      }
    });

  };

  var disableMouseMove = function(lc) {
    lc.map.setOptions({
      draggable: true
    });
    // lc.enableClickable(lc.objects);
    var container = $('#' + lc.containerId);
    setTimeout(function() {
      if (lc.rectSelectionRectangle != undefined) {
        lc.SelectObjectsByRectangularSelection();
        lc.rectSelectionRectangle.setMap(null);
        lc.rectSelectionRectangle = null;
      }
    }, 2);

    container.off('mousemove.rectSelection');
  };

};

LayoutControl.prototype.ChangeSelectedObjectsType = function(newObjectType) {
  var lc = this;
  lc.bObjectTypeChanging = true;
  var objects = lc.selectedObjects;
  //force copy for all object
  objects.forEach(function(object, key) {
    objects[key] = $.extend({}, object, true);
  });

  var objectData, primitive;
  lc.DeleteSelectedObject(true);

  objects.forEach(function(object) {
    if (object.type == newObjectType)
      return;
    switch (newObjectType) {
      case 'Roof':
        if (object.data instanceof Obstruction)
          lc.outsideObstructions.splice(lc.outsideObstructions.indexOf(object.data), 1);

        var idx = lc.lastIndexes.Roof = lc.lastIndexes.Roof + 1;

        var roofFaces = [new RoofFace(GetPolygon2D(object.primitive), 0, 0)];

        var name = object.data.name;
        if (name == object.type + ' ' + object.data.index)
          name = 'Roof ' + idx;
        // Unclassified objects have name 'Object [id]', not the '[object_type] [id]'
        if (object.type == 'Unclassified' && name == 'Object ' + object.data.index)
          name = 'Roof ' + idx;

        var description = object.data.description;
        if (description == undefined)
          description = '';

        var meanHeight = object.data.height;
        if (meanHeight == undefined)
          meanHeight = 0;

        var setback = object.data.setback;
        if (setback == undefined)
          setback = 0;

        var leastHorizontalDistance = object.data.leastHorizontalDistance;
        if (leastHorizontalDistance == undefined)
          leastHorizontalDistance = 0;

        var customSetbacks = object.data.customSetbacks;
        if (customSetbacks == undefined)
          customSetbacks = Array.apply(null, Array(roofFaces[0].polygon2dLocal.vertices.length)).map(function() {
            return 0;
          });

        //LayoutControl.prototype.AddPrimitive = function (polygon2D, strokeColor, fillColor, zOrder, strokeWeight, strokeOpacity, fillOpacity, shapeType)
        var color = layoutControlElementColors.Roof;
        primitive = lc.AddPrimitive(GetPolygon2D(object.primitive), color, color, layoutControlElementZIndexes.Roof, null, null, null, object.primitive.shapeType);

        SetFullPolygonColor(primitive, layoutControlElementColors.Roof);
        //                primitive.setOptions({zIndex: layoutControlElementZIndexes.Roof});

        objectData = new Roof(idx, roofFaces, name, description, meanHeight, false, 0, setback, leastHorizontalDistance, customSetbacks, lc, primitive);
        objectData.RefreshDistanceLabels(lc);

        //Getting root area in sq meters, convert to sq ft
        //                objectData.area = google.maps.geometry.spherical.computeArea(primitive.getPath().getArray()) * 10.7639;
        break;
      case 'Obstruction':
        if (object.data instanceof Roof) {
          object.data.ClearModules(lc);
          object.data.azimuthArrow.setMap(null);
        }

        var idx = lc.lastIndexes.Obstruction = lc.lastIndexes.Obstruction + 1;

        var color = layoutControlElementColors.Roof;
        primitive = lc.AddPrimitive(GetPolygon2D(object.primitive), color, color, layoutControlElementZIndexes.Obstruction, 2, null, null, object.primitive.shapeType);

        var name = object.data.name;
        if (name == object.type + ' ' + object.data.index)
          name = 'Obstruction ' + idx;
        // Unclassified objects have name 'Object [id]', not the '[object_type] [id]'
        if (object.type == 'Unclassified' && name == 'Object ' + object.data.index)
          name = 'Obstruction ' + idx;

        var description = object.data.description;
        if (description == undefined)
          description = '';

        var height = object.data instanceof Roof ? object.data.meanHeight : object.data.height;
        if (height == undefined)
          height = 0;

        var setback = object.data.setback;
        if (setback == undefined)
          setback = 0;

        var parentRoof = lc.IsPolygonOnRoof(GetPolygon2D(primitive));

        SetFullPolygonColor(primitive, layoutControlElementColors.Obstruction);
        primitive.setOptions({
          zIndex: layoutControlElementZIndexes.Obstruction
        });

        objectData = new Obstruction(idx, primitive, name, description, height, setback, parentRoof, lc);

        if (parentRoof) {
          parentRoof.obstructions.push(objectData);
        } else {
          lc.outsideObstructions.push(objectData);
        }
        break;
    }

    idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
    var newObj = new mapObject(idx, newObjectType, primitive, objectData);
    lc.objects.push(newObj);
    lc.setObjectZIndex(lc.objects, newObj);

    google.maps.event.addListener(newObj.primitive, 'click', function() {
      lc.ObjectSelected(newObj);
    });

    google.maps.event.addListener(newObj.primitive, 'drag', function() {
      lc.ObjectDraged(newObj);
    });

    google.maps.event.addListener(newObj.primitive, 'dragstart', function() {
      lc.ObjectDragStart(newObj);
    });

    google.maps.event.addListener(newObj.primitive, 'dragend', function() {
      lc.ObjectDragEnd(newObj);
    });


    lc.ObjectSelected(newObj, true);
  });

  lc.GetObstructions().forEach(function(obstruction) {
    obstruction.ChangeShape(lc, obstruction.primitive);
    lc.setObjectZIndex(lc.objects, obstruction);
  });

  var onObjectTypeChanged = {
    type: "onObjectTypeChanged",
    objects: lc.selectedObjects
  };
  this.dispatchEvent(onObjectTypeChanged);

  lc.UpdateObjectSelection();
  lc.bObjectTypeChanging = false;
};

LayoutControl.prototype.clearObstructions = function(roof) {
  roof.ClearObstructions(this);
};

LayoutControl.prototype.returnModule = function(module, startPoints2d) {
  var lc = this;
  module.primitive.setOptions({
    paths: GetPrimitivePath(startPoints2d)
  });
  lc.ModuleSelected(module);
};

LayoutControl.prototype.ChangeSelectedModulesGroupIndex = function(index) {
  var lc = this;
  if (lc.selectedModules.length == 0)
    throw 'LayoutControl: Can\'t change modules indexes - no modules selected';

  lc.selectedModules.forEach(function(module, key) {
    lc.selectedModules[key].groupIndex = index;
  });
};

LayoutControl.prototype.SetModuleAzimuthArrowsVisible = function(bVisible) {
  var save = true,
    check = true;
  if (bVisible == undefined) {
    bVisible = this.moduleAzimuthArrowVisible;
    check = false;
  }
  if (this.blockModuleAzimuthArrowsVisible == true) {
    bVisible = false;
    // save = false;
  }
  if (bVisible == this.moduleAzimuthArrowVisible && check)
    return;
  var lc = this;
  lc.GetRoofs().forEach(function(roof) {
    var modules = roof.modules,
      moduleCount = modules.length;

    for (var i = 0; i < moduleCount; i++) {
      if (modules[i].azimuthArrow)
        modules[i].azimuthArrow.setVisible(bVisible);
    }
  });
  if (save)
    lc.moduleAzimuthArrowVisible = bVisible;
};

LayoutControl.prototype.ShowModuleAzimuthArrows = function() {
  this.SetModuleAzimuthArrowsVisible(true);
};

LayoutControl.prototype.HideModuleAzimuthArrows = function() {
  this.SetModuleAzimuthArrowsVisible(false);
};

LayoutControl.prototype.BlockModuleAzimuthArrows = function() {
  this.blockModuleAzimuthArrowsVisible = true;
  this.SetModuleAzimuthArrowsVisible();
};

LayoutControl.prototype.UnblockModuleAzimuthArrows = function() {
  this.blockModuleAzimuthArrowsVisible = false;
  this.SetModuleAzimuthArrowsVisible();
};

LayoutControl.prototype.SetAllModulesVisibility = function(visible) {
  if (typeof visible != 'boolean')
    throw 'SetAllModulesVisibility - 1st parameter(visible) should be boolean';

  var lc = this;
  lc.GetRoofs().forEach(function(roof) {
    var modulesCount = roof.modules.length;
    for (var i = 0; i < modulesCount; i++) {
      roof.modules[i].SetVisible(visible);
    }
  });
};

LayoutControl.prototype.SetAllModulesClickabitility = function(clickable) {
  if (typeof clickable != 'boolean')
    throw 'SetAllModulesClickabitility - 1st parameter(clickable) should be boolean';

  var lc = this;
  lc.GetRoofs().forEach(function(roof) {
    var modulesCount = roof.modules.length;
    for (var i = 0; i < modulesCount; i++) {
      roof.modules[i].primitive.setOptions({
        clickable: clickable
      });
    }
  });
};

LayoutControl.prototype.ShowModules = function() {
  if (this.map.getZoom() >= 21)
    this.ShowModuleAzimuthArrows();
  this.SetAllModulesVisibility(true);
  this.modulesVisible = true;
  return this.modulesVisible;
};

LayoutControl.prototype.HideModules = function() {
  this.HideModuleAzimuthArrows();
  this.SetAllModulesVisibility(false);
  this.modulesVisible = false;
  return this.modulesVisible;
};

LayoutControl.prototype.ToggleModulesVisible = function() {
  if (this.modulesVisible)
    return this.HideModules();
  else
    return this.ShowModules();
};

LayoutControl.prototype.GetModulesVisible = function() {
  return this.modulesVisible;
};

LayoutControl.prototype.SetSnapAzimuth = function(snapAzimuth) {
  if (typeof snapAzimuth != 'boolean')
    throw 'LayoutControl[SetSnapAzimuth]: Cannot set snapAzimuth - 1st parameter should be boolean';

  if (snapAzimuth == this.snapAzimuth)
    return;

  if (snapAzimuth == true) {
    var lc = this;
    lc.ChangeInteractionMode('SELECTION', true);
    lc.snapAzimuthInitObj = lc.selectedObjects[0];
    lc.snapAzimuthListener = [
      google.maps.event.addListener(this.map, 'mousemove', function(e) {
        lc.HighlightEdge(e.latLng);
      }),
      google.maps.event.addListener(this.selectedObjects[0].data.primitive, 'mousemove', function(e) {
        lc.HighlightEdge(e.latLng);
      }),
      google.maps.event.addDomListener(document.getElementById(lc.containerId), 'click', function(e) {
        lc.SetSnapAzimuth(false);
        lc.SnapAzimuthToEdge();
      }),
      google.maps.event.addDomListener(document, 'keyup', function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code === 27) {
          lc.SetSnapAzimuth(false);
        }
      })
    ];
    lc.selectedObjects[0].primitive.setOptions({
      editable: false,
      strokeWeight: 4
    });
  } else {
    if (this.snapAzimuthListener) {
      this.snapAzimuthListener[0].remove();
      this.snapAzimuthListener[1].remove();
      this.snapAzimuthListener[2].remove();
      this.snapAzimuthListener[3].remove();
    }
    if (this.snapAzimuthPath)
      this.snapAzimuthPath.setMap(null);
    this.snapAzimuthListener = null;
    this.snapAzimuthInitObj.data.ChangeArrowHeading(this.snapAzimuthInitObj.data.roofFaces[0].azimuth);
    this.selectedObjects[0].primitive.setOptions({
      editable: true,
      strokeWeight: 3
    });
  }

  this.snapAzimuth = snapAzimuth;
  var onModulesAdded = {
    type: "onSnapAzimuthSet",
    newSnapAzimuth: snapAzimuth
  };
  this.dispatchEvent(onModulesAdded);
};

LayoutControl.prototype.HighlightEdge = function(point) {
  var lc = this;
  var path = GetObjectEdgeByLatLng(lc.selectedObjects[0].data.primitive, point);
  // console.log('path', path);
  if (path) {
    var snapAzimuthPath = lc.snapAzimuthPath ? lc.snapAzimuthPath.getPath() : null;
    // console.log('changed', SnapAzimuthPointsChanged(snapAzimuthPath, path));
    if (SnapAzimuthPointsChanged(snapAzimuthPath, path) || lc.snapAzimuthPath == null) {
      if (lc.snapAzimuthPath)
        lc.snapAzimuthPath.setMap(null);
      lc.snapAzimuthPath = new google.maps.Polyline({
        path: path,
        strokeColor: layoutControlElementColors.SnapAzimuth,
        strokeOpacity: 1,
        strokeWeight: 4,
        map: lc.map,
        zIndex: lc.selectedObjects[0].data.primitive.zIndex + 1
      });
      var pos1 = path.getAt(0);
      var pos2 = path.getAt(1);
      var heading = parseFloat(google.maps.geometry.spherical.computeHeading(pos1, pos2).toFixed(0));
      heading += 90;
      if (heading < 0)
        heading += 360;

      if (IsPolygonPointsClockwise(lc.selectedObjects[0].data.primitive)) {
        if (heading + 180 > 360)
          heading -= 180;
        else
          heading += 180;
      }

      lc.selectedObjects[0].data.ChangeArrowHeading(heading);
    }
  } else {
    if (lc.snapAzimuthPath) {
      if (lc.snapAzimuthPath)
        lc.snapAzimuthPath.setMap(null);
      lc.snapAzimuthPath = null;
      lc.selectedObjects[0].data.ChangeArrowHeading(lc.selectedObjects[0].data.roofFaces[0].azimuth);
    }
  }
};

var SnapAzimuthPointsChanged = function(snapAzimuthPath, checkingSnapAzimuthPath) {
  if (snapAzimuthPath == undefined)
    return false;

  if (checkingSnapAzimuthPath == undefined && snapAzimuthPath != checkingSnapAzimuthPath)
    return true;

  var p1 = snapAzimuthPath.getAt(0).lat() + snapAzimuthPath.getAt(0).lng(),
    c1 = checkingSnapAzimuthPath.getAt(0).lat() + checkingSnapAzimuthPath.getAt(0).lng(),
    p2 = snapAzimuthPath.getAt(1).lat() + snapAzimuthPath.getAt(1).lng(),
    c2 = checkingSnapAzimuthPath.getAt(1).lat() + checkingSnapAzimuthPath.getAt(1).lng();

  if (p1 != c1 || p2 != c2) {
    return true;
  }
  return false;
};

LayoutControl.prototype.SnapAzimuthToEdge = function() {
  var lc = this;

  var roof = lc.selectedObjects[0];
  if (!roof instanceof Roof)
    throw 'LayoutControl[SnapAzimuthToEdge]: Cannot snap azimuth to edge - selected object is not Roof';

  var minPolyLine = lc.snapAzimuthPath.getPath();

  if (!minPolyLine) {
    lc.SetSnapAzimuth(false);
    return;
  }

  var pos1 = minPolyLine.getAt(0);
  var pos2 = minPolyLine.getAt(1);
  var heading = parseFloat(google.maps.geometry.spherical.computeHeading(pos1, pos2).toFixed(0));
  heading += 90;
  if (heading < 0)
    heading += 360;

  if (IsPolygonPointsClockwise(roof.data.primitive)) {
    if (heading + 180 > 360)
      heading -= 180;
    else
      heading += 180;
  }

  roof.data.roofFaces[0].azimuth = heading;
  roof.data.ChangeArrowHeading(heading);
};

LayoutControl.prototype.BlockRoofGeometry = function() {
  var lc = this;
  lc.GetRoofs().forEach(function(roof) {
    if (roof.interiorZone)
      lc.canvas.setStyle(roof.interiorZone, {
        fillOpacity: 0,
        strokeOpacity: 0
      });
    if (roof.cornerZones)
      roof.cornerZones.forEach(function(zone) {
        lc.canvas.setStyle(zone, {
          fillOpacity: 0,
          strokeOpacity: 0
        });
      });
  });
  lc.roofGeometryBlocked = true;
  var onRoofGeometryBlocked = {
    type: "onRoofGeometryBlocked",
    roofGeometryBlocked: lc.roofGeometryBlocked
  };
  this.dispatchEvent(onRoofGeometryBlocked);
};

LayoutControl.prototype.UnblockRoofGeometry = function() {
  var lc = this;
  lc.GetRoofs().forEach(function(roof) {
    if (roof.interiorZone)
      lc.canvas.setStyle(roof.interiorZone, {
        fillOpacity: 0.5,
        strokeOpacity: 1
      });
    if (roof.cornerZones)
      roof.cornerZones.forEach(function(zone) {
        lc.canvas.setStyle(zone, {
          fillOpacity: 0.5,
          strokeOpacity: 1
        });
      });
  });
  lc.roofGeometryBlocked = false;
  var onRoofGeometryBlocked = {
    type: "onRoofGeometryBlocked",
    roofGeometryBlocked: lc.roofGeometryBlocked
  };
  this.dispatchEvent(onRoofGeometryBlocked);
};

LayoutControl.prototype.setRailsVisible = function(isVisible) {
  var lc = this;
  lc.railsVisible = isVisible;
  lc.GetRoofs().forEach(function(roof) {
    if (roof.rails)
      roof.rails.forEach(function(rail) {
        //rail.visible = true;
        rail.setVisible(isVisible);
      });

    if (roof.attachments)
      roof.attachments.forEach(function(attachment) {
        attachment.setVisible(isVisible);
      });

    if (roof.splices)
      roof.splices.forEach(function(splice) {
        splice.setVisible(isVisible);
      });
  });
};

function GetObjectEdgeByLatLng(object, latLng) {

  if (object.getPath == undefined)
    throw 'LayoutControl[GetObjectEdgeByLatLng] - 1st parameter should be Google Maps API Polygon or Polyline';
  if (!latLng instanceof google.maps.LatLng)
    throw 'LayoutControl[GetObjectEdgeByLatLng] - 2nd parameter should be Google Maps API LatLng object';

  var objectPath = object.getPath();
  var objectPathLength = objectPath.getLength();
  var minPolyLine = null;
  for (var i = 0; i < objectPathLength; i++) {
    var paths = {};
    if (i == objectPathLength - 1) {
      paths = [{
        lat: objectPath.getAt(i).lat(),
        lng: objectPath.getAt(i).lng()
      }, {
        lat: objectPath.getAt(0).lat(),
        lng: objectPath.getAt(0).lng()
      }];
    } else {
      paths = [{
        lat: objectPath.getAt(i).lat(),
        lng: objectPath.getAt(i).lng()
      }, {
        lat: objectPath.getAt(i + 1).lat(),
        lng: objectPath.getAt(i + 1).lng()
      }];
    }
    var polyline = new google.maps.Polyline({
      path: paths,
      strokeColor: '#ff0000',
      strokeOpacity: 1,
      strokeWeight: 2
    });

    var onEdge = google.maps.geometry.poly.isLocationOnEdge(latLng, polyline, 1E-5);
    polyline.setMap(null);
    if (onEdge) {
      minPolyLine = polyline.getPath();
      break;
    }
  }
  return minPolyLine;
}

/**
 * Get path points from given Polygon2D
 *
 */

function GetPrimitivePath(polygon2D) {
  var path = [];
  polygon2D.vertices.forEach(function(vertex) {
    path.push(new google.maps.LatLng(vertex.y, vertex.x));
  });
  return path;
}

function GetDrawingManager(map, lc, zIndex, type) {
  if (type == undefined || type == '') {
    type = 'polygon';
  }

  var style = {
    strokeWeight: 3,
    strokeColor: layoutControlElementColors.Drawing,
    strokeOpacity: 1,
    fillColor: layoutControlElementColors.Drawing,
    fillOpacity: 0.5,
    zIndex: zIndex,
    editable: true
  };
  var options = {
    drawingControl: false
  };
  switch (type) {
    case "polygon":
      options.drawingMode = google.maps.drawing.OverlayType.POLYGON;
      options.polygonOptions = style;
      break;
    case "rectangle":
      options.drawingMode = google.maps.drawing.OverlayType.RECTANGLE;
      options.rectangleOptions = style;
      break;
    case "circle":
      options.drawingMode = google.maps.drawing.OverlayType.CIRCLE;
      options.circleOptions = style;
      break;
  }
  var drawingManager = new google.maps.drawing.DrawingManager(options);
  drawingManager.setMap(map);
  if (type == 'polygon')
    lc.InitializeDrawingPoly(map, drawingManager);
  return drawingManager;
}

/**
 * Returns created drawing manager for Roof
 *
 */
// TODO: Drawing manager creation methods require refactoring

function GetRoofDrawingManager(map) {
  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    drawingControl: false,
    polygonOptions: {
      strokeWeight: 3,
      strokeColor: layoutControlElementColors.Roof,
      strokeOpacity: 1,
      fillColor: layoutControlElementColors.Roof,
      fillOpacity: 0.3,
      zIndex: 0,
      editable: true
    }
  });
  drawingManager.setMap(map);

  return drawingManager;
}

/**
 * Returns created drawing manager for Obstruction
 *
 */

function GetObstructionDrawingManager(map, type) {
  var style = {
    strokeWeight: 3,
    strokeColor: layoutControlElementColors.Obstruction,
    strokeOpacity: 1,
    fillColor: layoutControlElementColors.Obstruction,
    fillOpacity: 0.5,
    zIndex: 5000,
    editable: true
  };
  var options = {
    drawingControl: false
  };
  switch (type) {
    case "polygon":
      options.drawingMode = google.maps.drawing.OverlayType.POLYGON;
      options.polygonOptions = style;
      break;
    case "rectangle":
      options.drawingMode = google.maps.drawing.OverlayType.RECTANGLE;
      options.rectangleOptions = style;
      break;
    case "circle":
      options.drawingMode = google.maps.drawing.OverlayType.CIRCLE;
      options.circleOptions = style;
      break;
  }
  var drawingManager = new google.maps.drawing.DrawingManager(options);
  drawingManager.setMap(map);
  return drawingManager;
}

/**
 * Returns created drawing manager for InstallationArea
 *
 */

function GetInstallationAreaDrawingManager(map) {
  var drawingManager = new google.maps.drawing.DrawingManager({
    drawingMode: google.maps.drawing.OverlayType.POLYGON,
    drawingControl: false,
    polygonOptions: {
      strokeWeight: 3,
      strokeColor: layoutControlElementColors.InstallationArea,
      strokeOpacity: 1,
      fillColor: layoutControlElementColors.InstallationArea,
      fillOpacity: 0.5,
      zIndex: 1000,
      editable: true
    }
  });
  drawingManager.setMap(map);
  return drawingManager;
}

/**
 * Deletes drawed object if given Roof/Obstruction/InstallationArea
 * Is on top of same object type
 *
 */
LayoutControl.prototype.UndoIfOnTopOfOtherObject = function(checkingPolygon2D, toCheck) {
  var polygons2D = [];
  toCheck.forEach(function(item) {
    var toPush;
    if (item instanceof Roof) {
      toPush = selectedObject.data.roofFaces[0].polygon2dLocal;
    } else {
      toPush = selectedObject.data.polygon2dLocal;
    }
    polygons2D.push(toPush);
  });
  if (this.IsPolygonOverlapOtherPolygons(checkingPolygon2D, polygons2D)) {
    setTimeout(function() {
      $('img[src$="undo_poly.png"]').click();
    }, 100);
    return true;
  }
  return false;
};

LayoutControl.prototype.CopyObjectsToClipboard = function(objects) {
  this.objectsClipboard = [];

  var lc = this;

  if (objects) {
    objects.forEach(function(object) {
      switch (object.type) {
        case "Roof":
          var roofData = {
            description: object.data.description,
            hasParapetWall: object.data.hasParapetWall,
            index: object.data.index,
            installationAreas: [],
            meanHeight: object.data.meanHeight,
            modules: [],
            name: object.data.name,
            obstructions: [],
            parapetWallHeight: object.data.parapetWallHeight,
            leastHorizontalDistance: object.data.leastHorizontalDistance,
            projectId: object.data.projectId,
            setback: object.data.setback,
            customSetbacks: object.data.customSetbacks,
            roofFaces: [{
              azimuth: object.data.roofFaces[0].azimuth,
              slope: object.data.roofFaces[0].slope,
              polygon2dLocal: object.data.roofFaces[0].polygon2dLocal
            }]
          };

          lc.objectsClipboard.push({
            type: object.type,
            index: object.index,
            data: angular.copy(roofData)
          });

          break;
        case "Obstruction":
          var parentRoof = object.data.parentRoof;

          delete object.data.primitive;
          delete object.data.parentRoof;

          object.data.primitive = object.primitive;
          object.data.parentRoof = parentRoof;

          lc.objectsClipboard.push({
            type: object.type,
            index: object.index,
            data: angular.copy(object.data)
          });

          break;
        case "Unclassified":
          lc.objectsClipboard.push({
            type: object.type,
            index: object.index,
            data: {
              polygon2dLocal: object.data.polygon2dLocal,
              index: object.data.index
            }
          });
          break;

        case "Module":
          lc.objectsClipboard.push({
            type: object.type,
            azimuth: object.azimuth,
            index: object.index,
            groupIndex: object.groupIndex,
            isOpposite: object.isOpposite,
            rowIndex: object.rowIndex,
            neighbours: [null, null, null, null, null, null, null, null],
            data: object.data,
            polygon2dLocal: GetPolygon2D(object.primitive)
          });
          break;
      }
    });
  }

  return this.objectsClipboard;
};

LayoutControl.prototype.PasteObjectsFromClipboard = function(latLng) {

  var lc = this;

  var modules = [];
  var roofsModulesMaxGroupIndex = [];
  var roofsModulesMaxIndex = [];

  var bOnlyModules = true;

  this.objectsClipboard.forEach(function(object) {
    if (object.type == 'Module') {

      var roof = lc.GetRoofByIndex(object.data.parentRoof);
      roof.modules.forEach(function(module) {
        if (!roofsModulesMaxGroupIndex[module.data.parentRoof])
          roofsModulesMaxGroupIndex[module.data.parentRoof] = module.groupIndex;

        if (roofsModulesMaxGroupIndex[module.data.parentRoof] < module.groupIndex) {
          roofsModulesMaxGroupIndex[module.data.parentRoof] = module.groupIndex;
        }

        if (!roofsModulesMaxIndex[module.data.parentRoof])
          roofsModulesMaxIndex[module.data.parentRoof] = module.index;

        if (roofsModulesMaxIndex[module.data.parentRoof] < module.index) {
          roofsModulesMaxIndex[module.data.parentRoof] = module.index;
        }
      });
    } else {
      bOnlyModules = false;
    }
  });

  //Getting bounding box top left poit
  var topLeftPoint = null;
  this.objectsClipboard.forEach(function(object) {

    var vertices;
    if (object.type == 'Obstruction' || object.type == 'Unclassified')
      vertices = object.data.polygon2dLocal.vertices;
    if (object.type == 'Roof')
      vertices = object.data.roofFaces[0].polygon2dLocal.vertices;
    if (object.type == 'Module')
      vertices = object.polygon2dLocal.vertices;

    vertices.forEach(function(vertice) {
      if (!topLeftPoint) {
        topLeftPoint = {
          x: vertice.x,
          y: vertice.y
        };
      } else {
        if (vertice.x <= topLeftPoint.x) {
          topLeftPoint = {
            x: vertice.x,
            y: vertice.y
          };
        }
      }
    });
  });

  var lastGIndex = 0;
  var lastModuleGIndex = null;

  this.objectsClipboard.forEach(function(object) {

    var offsetX = parseFloat(latLng.lng()) - topLeftPoint.x;
    var offsetY = parseFloat(latLng.lat()) - topLeftPoint.y;

    var vertices;
    if (object.type == 'Obstruction' || object.type == 'Unclassified')
      vertices = object.data.polygon2dLocal.vertices;
    if (object.type == 'Roof')
      vertices = object.data.roofFaces[0].polygon2dLocal.vertices;
    if (object.type == 'Module')
      vertices = object.polygon2dLocal.vertices;

    vertices.forEach(function(vertex) {
      vertex.x += offsetX;
      vertex.y += offsetY;
    });

    if (object.type == 'Roof') {

      var roofDataCopy = angular.copy(object.data);
      var newRoofData = lc.AddRoof(roofDataCopy);

      var idx = lc.lastIndexes.Roof = lc.lastIndexes.Roof + 1;
      newRoofData.index = idx;
      newRoofData.name = 'Roof ' + idx;

      newRoofData.primitive.setOptions({
        clickable: false
      });
      newRoofData.DrawGeometry(lc);
    }

    if (object.type == 'Obstruction') {
      var obstruction = lc.AddObstruction(angular.copy(object.data));
      obstruction.polygon2dLocal = GetPolygon2D(obstruction.primitive);

      var idx = lc.lastIndexes.Obstruction = lc.lastIndexes.Obstruction + 1;
      var parentRoof = lc.IsPolygonOnRoof(GetPolygon2D(obstruction.primitive));

      obstruction.parentRoof = parentRoof;
      obstruction.index = idx;
      obstruction.name = 'Obstruction ' + idx;

      obstruction.primitive.setOptions({
        clickable: false
      });

      if (parentRoof) {
        parentRoof.obstructions.push(obstruction);
      } else {
        lc.outsideObstructions.push(obstruction);
      }
    }

    if (object.type == 'Unclassified') {
      var newObject = angular.copy(object);
      var color = layoutControlElementColors.Unclassified;
      var primitive = lc.AddPrimitive(object.data.polygon2dLocal, color, color, layoutControlElementZIndexes.Unclassified, 2);
      newObject.primitive = primitive;
      SetFullPolygonColor(primitive, color);
      newObject = lc.CreateObject(object.type, primitive);

      google.maps.event.addListener(newObject.primitive, 'click', function() {
        lc.ObjectSelected(newObject);
      });

      newObject.primitive.setOptions({
        clickable: false
      });
    }

    if (object.type == 'Module') {

      var newObject = angular.copy(object);
      var roof = lc.GetRoofByIndex(newObject.data.parentRoof);

      //Get roof max groupIndex
      if (lastGIndex === 0)
        lastGIndex = roofsModulesMaxGroupIndex[newObject.data.parentRoof] + 1;

      if (roof && lastGIndex && lastGIndex !== 0) {
        if (lastModuleGIndex === null)
          lastModuleGIndex = newObject.groupIndex;
        if (lastModuleGIndex !== newObject.groupIndex) {
          lastGIndex++;
          lastModuleGIndex = newObject.groupIndex;
        }
        var index = roofsModulesMaxIndex[newObject.data.parentRoof] = roofsModulesMaxIndex[newObject.data.parentRoof] + 1;
        var addedModule = roof.AddModule(index, newObject.polygon2dLocal, lastGIndex, newObject.isOpposite, newObject.rowIndex, newObject.neighbours, lc);

        modules.push(addedModule);
      }
    }

    //    lc.objectsClipboard = [];
    //    var onObjectsClipboardChanged = {type: "onObjectsClipboardChanged", objectsClipboard: lc.objectsClipboard};
    //    lc.dispatchEvent(onObjectsClipboardChanged);
    //    lc.ChangeInteractionMode('SELECTION');
  });

  if (bOnlyModules)
    lc.ChangeInteractionMode('SELECTION');

  lc.UpdateObjectSelection();

  if (modules.length > 0) {
    var onModulesAdded = {
      type: "onModulesAdded",
      addedModules: modules
    };
    lc.dispatchEvent(onModulesAdded);
  }

  lc.dispatchEvent({
    type: "onObjectsPasted"
  });
};

LayoutControl.prototype.disableClickable = function(objects) {
  objects.forEach(function(object) {
    object.primitive.setOptions({
      clickable: false
    });
  });
};

LayoutControl.prototype.enableClickable = function(objects) {
  objects.forEach(function(object) {
    object.primitive.setOptions({
      clickable: true
    });
  });
};

LayoutControl.prototype.disableDraggable = function(objects) {
  objects.forEach(function(object) {
    object.primitive.setOptions({
      draggable: false
    });
  });
};

LayoutControl.prototype.enableDraggable = function(objects) {
  objects.forEach(function(object) {
    object.primitive.setOptions({
      draggable: true
    });
  });
};

LayoutControl.prototype.RefreshModulesClickEvents = function() {
  var modules = [];
  this.GetRoofs().forEach(function(roof) {
    if (roof.modules instanceof Array)
      Array.prototype.push.apply(modules, roof.modules);
  });

  var lc = this;
  var modulesCount = modules.length;
  for (var i = 0; i < modulesCount; i++) {
    var module = modules[i];
    google.maps.event.clearListeners(module.primitive, 'click');

    module.AddListeners(lc);
  }
};

LayoutControl.prototype.GetSetbackForPolygon2D = function(polygon2d, setback, meanHeight, leastHorizontalDistance, customSetbacks, azimuth, successCb, errorCb) {
  if (this.setbackServiceUrl == undefined)
    throw 'LayoutControl[GetSetbackForPolygon2D]: Setback Service URL not set';

  if (!polygon2d instanceof Polygon2D)
    throw 'LayoutControl[GetSetbackForPolygon2D]: 1st parameter should Polygon2D object';

  if (typeof setback != 'number')
    setback = parseFloat(setback);

  var dataToSend = {
    Vertices: polygon2d.vertices,
    Setback: setback,
    MeanHeight: parseFloat(meanHeight),
    LeastHorizontalDimension: parseFloat(leastHorizontalDistance),
    CustomSetback: customSetbacks,
    Azimuth: azimuth,
    IsRequestFromPVSketch: customSetbacks.length > 0
  };

  return $.ajax({
    url: this.setbackServiceUrl, // services/project/layout/roofdata
    method: 'POST',
    data: JSON.stringify(dataToSend),
    success: function(response) {
      successCb(response);
    },
    error: function(response) {
      errorCb(response);
    }
  });
};

LayoutControl.prototype.IsObjectSelected = function(object) {
  //  var bMapObject = true;
  //  if (!(object instanceof mapObject))
  //    bMapObject = false;
  //
  //  if (this.selectedObjects.indexOf(object) === -1)
  //    return false;
  //
  //  return true;

  if (!this.selectedObjects || !object) {
    return false;
  }
  if (isObjectEmpty(this.selectedObjects) || isObjectEmpty(object)) {
    return false;
  }
  for (var key in this.selectedObjects) {
    if (isObjectEmpty(this.selectedObjects[key]) || !(this.selectedObjects[key] instanceof mapObject) || !this.selectedObjects[key].data || isObjectEmpty(this.selectedObjects[key].data)) {
      continue;
    }
    if (this.selectedObjects[key].data === object) {
      return true;
    }
    if (object instanceof mapObject) {
      if (this.selectedObjects.indexOf !== undefined && this.selectedObjects.indexOf(object) > -1) {
        return true;
      }
    }
  }
  return false;
};

LayoutControl.prototype.RefreshProjectMinLinesDistances = function() {
  var roofs = this.GetRoofs();
  var roofsNum = roofs.length;

  if (roofsNum == 0)
    return 0;

  var minDist = roofs[0].ComputeAverageLineDistance();
  for (var i = 0; i < roofsNum; i++) {
    var aveDist = roofs[i].ComputeAverageLineDistance();
    if (aveDist < minDist)
      minDist = aveDist;
  }
  layoutControlProjectMinRoofLinesAverage = minDist;
  return layoutControlProjectMinRoofLinesAverage;
};

LayoutControl.prototype.DehighlightObject = function() {

  //If object is seleted, keep it highlighted
  if (this.highlightedObject !== undefined)
    if (!this.IsObjectSelected(this.highlightedObject)) {
      this.highlightedObject.primitive.setOptions(this.getStandardOptions(this.highlightedObject));
    }

  this.highlightedObject = undefined;
};

LayoutControl.prototype.HighlightObject = function(object) {
  if (object == undefined) {
    this.DehighlightObject();
  } else {
    if (this.highlightedObject != undefined) {
      this.DehighlightObject();
    }
    this.highlightedObject = object;
    this.highlightedObject.primitive.setOptions(this.getHighlightOptions(this.highlightedObject));
  }
};

LayoutControl.prototype.SetHighlightObject = function(object) {
  if (object == undefined) {
    this.DehighlightObject();
  } else {
    object.primitive.setOptions(this.getHighlightOptions(object));
  }
};

LayoutControl.prototype.RemoveHighlightObject = function(object) {
  if (object == undefined) {
    this.DehighlightObject();
  } else {
    object.primitive.setOptions(this.getStandardOptions(object));
  }
};

LayoutControl.prototype.SwitchSelectedModulesOrientation = function() {
  const lc = this;

  if (lc.selectedModules.length === 0) {
    console.warn('[LayoutControl] Cannot switch module orientation: no modules selected!');
  }

  for (let i = 0; i < lc.selectedModules.length; i++) {
    lc.selectedModules[i].polygon2dLocal = rotatePrimitive(lc.selectedModules[i].primitive, lc);
  }
};

LayoutControl.prototype.RotateSelection = function(moduleId) {
  const lc = this;

  if (lc.selectedModules.length > 0) {
    lc.SwitchSelectedModulesOrientation();
  }

  if (lc.selectedModules.length === 0 && lc.selectedObjects.length === 0) {
    console.warn('[LayoutControl] Cannot switch object orientation: no objects selected!');
    return;
  }

  lc.selectedObjects.forEach(function(object) {
    if ('Obstruction' === object.type) {
      object.polygon2dLocal = rotatePrimitive(object.primitive, lc);
      object.data.polygon2dLocal = rotatePrimitive(object.data.primitive, lc);
      lc.DoLayoutShadows(moduleId);
      lc.UpdateObjectSelection();
    }
  });
};

function rotatePrimitive(primitive, lc) {
  const polygon = GetPolygon2D(primitive);
  const polygonInMeters = lc.GetPointsInMetersFromPolygon2D(polygon);
  const rotatedPolygonInMeters = rotatePolygon(polygonInMeters.points, -90);
  const rotatedPolygon2d = {
    vertices: lc.GetPointsInLatLngFromMeters(polygonInMeters.startingPoint, rotatedPolygonInMeters)
  };
  primitive.setPath(GetPrimitivePath(rotatedPolygon2d));

  return rotatedPolygon2d;
}

function rotatePolygon(polygon2d, angle) {
  var center = GetCentroid(polygon2d);
  var origin = {
    x: center.lng(),
    y: center.lat()
  };
  var polygon2dCount = polygon2d.length;
  for (var i = 0; i < polygon2dCount; i++) {
    var point = polygon2d[i];
    polygon2d[i] = rotatePoint(point, origin, angle);
  }
  return polygon2d;
}

function rotatePoint(point, origin, angle) {
  var angleRad = angle * Math.PI / 180;
  var translatedX = point.x - origin.x;
  var translatedY = point.y - origin.y;
  var rotatedX = translatedX * Math.cos(angleRad) - translatedY * Math.sin(angleRad);
  var rotatedY = translatedX * Math.sin(angleRad) + translatedY * Math.cos(angleRad);
  return new Vertex(rotatedX + origin.x, rotatedY + origin.y);
}

LayoutControl.prototype.GetProjectModuleCount = function() {
  var moduleCount = 0;

  var roofs = this.GetRoofs();

  if (roofs) {
    roofs.forEach(function(roof) {
      moduleCount += roof.modules.length;
    });
  }

  return moduleCount;
};

LayoutControl.prototype.CheckMaxRoofSlope = function(slope) {
  if (slope == undefined)
    return;

  var roofs = this.GetRoofs();

  if (!roofs)
    return;

  slope = parseFloat(slope);

  var roofCount = roofs.length;

  for (var i = 0; i < roofCount; i++) {
    if (roofs[i].roofFaces[0].slope > slope)
      roofs[i].roofFaces[0].slope = slope;
  }
};

LayoutControl.prototype.GetRoofArea = function(roof) {

  switch (roof.primitive.shapeType) {
    case "polygon":
      return google.maps.geometry.spherical.computeArea(roof.primitive.getPath());
      break;
    case "circle":
      return Math.PI * Math.pow(roof.primitive.radius, 2);
      break;
  }
};

/**
 * Init Google Map / Render Google Map
 *
 */

function initMap(placeholderId, aCenter) {
  var higherZoomMap = new higherZoomMapType();
  var mapOptions = {
    center: aCenter,
    mapTypeControl: false,
    streetViewControl: false,
    scaleControl: true,
    rotateControl: false,
    mapTypeId: google.maps.MapTypeId.SATELLITE
  };
  var container = document.getElementById(placeholderId);
  var map = new google.maps.Map(container, mapOptions);
  map.container = container;

  map.mapTypes.set(higherZoomMap.id, higherZoomMap);
  map.setMapTypeId(higherZoomMap.id);
  var lastZoom = 0;
  var mouseScroll = function() {
    var zoomDelta = map.getZoom() - lastZoom;
    var toZoom = map.getZoom() + (zoomDelta > 0 ? 1 : -1);
    var newTileSize = 256;
    if (layoutControlMaxZoomRes <= toZoom) {
      var delta = (map.getZoom() - layoutControlMaxZoomRes);
      newTileSize = 256 << delta;
    }
    if (zoomDelta > 0 && lastZoom != 0) {
      if (newTileSize == 0) {
        newTileSize = 256;
      }
      var higherZoomMap = new higherZoomMapType(newTileSize, map.getZoom());
      map.mapTypes.set(higherZoomMap.id, higherZoomMap);
      map.setMapTypeId(higherZoomMap.id);
      //      if (map.getZoom() >= layoutControlMaxZoomRes)
      //      {
      //        var higherZoomMap = new higherZoomMapType(newTileSize, map.getZoom());
      //        map.mapTypes.set(higherZoomMap.id, higherZoomMap);
      //        map.setMapTypeId(higherZoomMap.id);
      //      }
    } else {
      if (map.getZoom() < layoutControlMaxZoomRes) {
        var higherZoomMap = new higherZoomMapType(newTileSize, map.getZoom());
        map.mapTypes.set(higherZoomMap.id, higherZoomMap);
        map.setMapTypeId(higherZoomMap.id);
        //        map.setMapTypeId(google.maps.MapTypeId.SATELLITE);

      } else {
        var higherZoomMap = new higherZoomMapType(newTileSize, map.getZoom());
        map.mapTypes.set(higherZoomMap.id, higherZoomMap);
        map.setMapTypeId(higherZoomMap.id);
      }
    }
    lastZoom = map.getZoom();
  };
  map.addListener('zoom_changed', mouseScroll);
  var maxZoomTimer = null;
  var maxZoomRespFunc = function() {
    if (maxZoomTimer) {
      clearTimeout(maxZoomTimer);
    }
    maxZoomTimer = setTimeout(function() {
      try {
        var maxZoomService = new google.maps.MaxZoomService();
        maxZoomService.getMaxZoomAtLatLng(map.getCenter(), function(response) {
          map.maxZoomResp = response.zoom;
        });
      } catch (e) {
        console.log(e.message);
      }
    }, 200);
  };
  map.addListener('center_changed', maxZoomRespFunc);
  //map.addListener('mousemove', function () {console.log('Mouse moved');});
  map.cursorMoved = false;
  google.maps.event.addListenerOnce(map, 'projection_changed', function() {
    $(map.container).mousemove(function(event) {
      map.cursorPos = pixelToLatlng(event.offsetX, event.offsetY, map);
    });
  });

  //map.addListener('dragstart', function ()
  //{
  //    console.log('dragstart');
  //});
  return map;
}

// Event dispatcher
/**
 * @author mrdoob / http://mrdoob.com/
 */

var EventDispatcher = function() {}

EventDispatcher.prototype = {
  constructor: EventDispatcher,
  apply: function(object) {

    object.addEventListener = EventDispatcher.prototype.addEventListener;
    object.hasEventListener = EventDispatcher.prototype.hasEventListener;
    object.removeEventListener = EventDispatcher.prototype.removeEventListener;
    object.dispatchEvent = EventDispatcher.prototype.dispatchEvent;

  },
  addEventListener: function(type, listener) {

    if (this._listeners === undefined)
      this._listeners = {};

    var listeners = this._listeners;

    if (listeners[type] === undefined) {

      listeners[type] = [];

    }

    if (listeners[type].indexOf(listener) === -1) {

      listeners[type].push(listener);

    }

  },
  hasEventListener: function(type, listener) {

    if (this._listeners === undefined)
      return false;

    var listeners = this._listeners;

    if (listeners[type] !== undefined && listeners[type].indexOf(listener) !== -1) {

      return true;

    }

    return false;

  },
  removeEventListener: function(type, listener) {

    if (this._listeners === undefined)
      return;

    var listeners = this._listeners;
    var listenerArray = listeners[type];

    if (listenerArray !== undefined) {

      var index = listenerArray.indexOf(listener);

      if (index !== -1) {

        listenerArray.splice(index, 1);

      }

    }

  },
  dispatchEvent: function(event) {

    if (this._listeners === undefined)
      return;

    var listeners = this._listeners;
    var listenerArray = listeners[event.type];

    if (listenerArray !== undefined) {

      event.target = this;

      var array = [];
      var length = listenerArray.length;

      for (var i = 0; i < length; i++) {

        array[i] = listenerArray[i];

      }

      for (var i = 0; i < length; i++) {

        array[i].call(this, event);

      }

    }

  }

};

EventDispatcher.prototype.apply(LayoutControl.prototype);

/**
 * Deletes drawn object if given primitive is outside parentRoofPrimitive
 *
 */

function UndoIfOutsideRoof(primitive, parentRoofPrimitive) {
  if (!IsPolygonInsideOtherPolygon(GetPolygon2D(primitive), GetPolygon2D(parentRoofPrimitive))) {
    setTimeout(function() {
      $('img[src$="undo_poly.png"]').click();
    }, 150);
    return true;
  }
  return false;
}

/**
 * Checks if given polygon(inside) is inside "outside" polygon
 *
 */

function IsPolygonInsideOtherPolygon(insidePolygon2D, outsidePolygon2D) {
  var bInside = true;
  insidePolygon2D.vertices.forEach(function(insidePolygonPoint) {
    // for skipping other points if one point is already outside
    if (!bInside) {
      return false;
    }
    var intersectionNum = 0;
    outsidePolygon2D.vertices.forEach(function(roofPoint, key) {
      var current = roofPoint;
      if (key == outsidePolygon2D.vertices.length - 1) {
        var next = outsidePolygon2D.vertices[0];
      } else {
        var next = outsidePolygon2D.vertices[key + 1];
      }
      // Detecting lower and upper points
      var upper;
      var lower;
      if (current.y > next.y) {
        upper = current;
        lower = next;
      } else {
        upper = next;
        lower = current;
      }
      if (!isPointLeft(lower, upper, insidePolygonPoint)) {
        return false;
      }
      if (!(insidePolygonPoint.y > lower.y && insidePolygonPoint.y < upper.y)) {
        return false;
      }
      intersectionNum++;
    });
    if (intersectionNum % 2 == 0) {
      bInside = false;
    }
  });
  return bInside;
}

/**
 * Checks if given point(c) is on left side from ab
 *
 */

function isPointLeft(a, b, c) {
  return ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0;
}

// GUI functions

/**
 * Returns icon options for marker to create arrow
 *
 */

function GetAzimuthArrowIcon(heading) {

  heading = 180 - heading;

  return {
    path: "M397,190.734L228.738,73.444c-6.599-5.632-11.996-3.147-11.996,5.528v49.068c0,8.672-7.099,15.77-15.77,15.77     l-104.176,0.156H15.69v0.125C7.051,144.139,0.002,151.214,0,159.857l0.002,82.789c0,8.673,7.095,15.771,15.765,15.771     l183.426-0.275h1.834c8.647,0.028,15.717,7.107,15.717,15.765v49.067c0,8.675,5.397,11.163,11.993,5.535l168.265-117.294     C403.598,205.579,403.598,196.367,397,190.734z",
    fillColor: "#ffffff",
    fillOpacity: 0.5,
    strokeColor: "#333333",
    strokeWeight: 1.5,
    anchor: new google.maps.Point(200.974, 200.974),
    scale: .05,
    rotation: parseFloat(-heading + 90)
  };
}

/**
 * Initialize GUI rendering
 *
 */

function initGUI(obj, isPanelVisible) {
  var panel = "";
  //var panel = renderPanel(obj, isPanelVisible);
  //var mapContainer = "<div style=\"position: relative; height: 100%;\" ><div style=\"height:calc(100% - 20px); width:100%\" id=\"mapContainer\"></div>" + panel.html + "<div id=\"" + obj.containerId + "_layoutControl_copyright\">" + layoutControlCopyright + "</div></div>";
  var mapContainer = "<div style=\"position: relative; height: 100%;\" ><div style=\"height:100%; width:100%\" id=\"mapContainer\"></div></div>";
  $("#" + obj.containerId).html(mapContainer);
  // Add button events
  initObstructionDrawerDropdownEvents();

  // Initialize autocomplete
  //panel['addressAutocompleteObj'] = new google.maps.places.Autocomplete($(".layoutToolPanel_addressBar #address").get(0));
  return panel;
}

/**
 * Renders main panel
 *
 */

function renderPanel(obj, bVisible) {
  var idx = obj.objIdx;
  addPanelCss(obj.containerId);
  var panelId = obj.containerId + "_panel";
  var panelDIV = "<div id=\"" + panelId + "\" class=\"layoutToolPanel\"" + (bVisible ? "" : "style=\"display:none;\"") + ">" +
    "<div class=\"layoutToolPanel_addressBar\">" +
    AddEditBox("", "address", "text", "Input an address") +
    "</div>" +
    "<div class=\"layoutToolPanel_panel\">" +
    "<div class=\"layoutToolPanel_header\">Layout</div>" +
    "<div class=\"layoutToolPanel_tabs\">" +
    "<a class=\"lcbtn addRoofBtn\" onclick=\"lc_heap[" + idx + "].StartDrawRoof()\" title=\"Draw Roof\">Roof</a>" +
    "<a class=\"lcbtn addInstallationAreaBtn\" onclick=\"lc_heap[" + idx + "].StartDrawInstallationArea()\" title=\"Draw Installation Area\">Installation Area</a>" +
    "<a class=\"lcbtn addObstructionBtn\" title=\"Draw Obstruction\">Obstruction</a>" +
    "<div class=\"layoutToolPanel_obstructionDrawerDropdown\">" +
    "<a onclick=\"lc_heap[" + idx + "].StartDrawObstruction('polygon')\" >Line</a>" +
    "<a onclick=\"lc_heap[" + idx + "].StartDrawObstruction('rectangle')\" >Rectangle</a>" +
    "<a onclick=\"lc_heap[" + idx + "].StartDrawObstruction('circle')\" >Circle</a>" +
    "</div>" +
    "</div>" +
    "<div class=\"layoutToolPanel_selectPanel\">" +
    "<a class=\"addDeleteBtn\" onclick=\"lc_heap[" + idx + "].DeleteSelectedObject()\" title=\"Delete selected object\"></a>" +
    "<div class=\"layoutToolPanel_SelectValue\">" +
    "<select id=\"" + panelId + "_objectSelect\" style=\"width: 100%\" onchange=\"lc_heap[" + idx + "].SelectObjectFromList()\"/>" +
    "</div>" +
    "<span class=\"objectColor\"></span>" +
    "</div>" +
    "<div class=\"layoutToolPanel_propertyPanel\" style=\"display: none\" id=\"" + panelId + "_roofProperties\">" +
    AddEditBox("Name: ", panelId + "roofName", "text") +
    AddEditBox("Description: ", panelId + "roofDescription", "text") +
    AddEditBox("Azimuth: ", panelId + "roofAzimuth", "decimal") +
    //            AddEditBox("Slope: ", panelId + "roofSlope", "decimal") +
    AddEditBox("Height: <span class=\"hint measurementSystem\">(inches)</span>", panelId + "roofHeight", "decimal") +
    //            AddCheckBox("Parapet: ", panelId + "roofParapet") +
    //            AddEditBox("Parapet height: ", panelId + "roofParapetHeight", "decimal") +
    AddEditBox("Setback: <span class=\"hint measurementSystem\">(inches)</span>", panelId + "roofSetback", "decimal") +
    "</div>" +
    "<div class=\"layoutToolPanel_propertyPanel\" style=\"display: none\" id=\"" + panelId + "_obstructionProperties\">" +
    AddEditBox("Name: ", panelId + "obstructionName", "text") +
    AddEditBox("Description: ", panelId + "obstructionDescription", "text") +
    AddEditBox("Height: <span class=\"hint measurementSystem\">(inches)</span>", panelId + "obstructionHeight", "decimal") +
    AddEditBox("Setback: <span class=\"hint measurementSystem\">(inches)</span>", panelId + "obstructionSetback", "decimal") +
    "</div>" +
    "<div class=\"layoutToolPanel_propertyPanel\" style=\"display: none\" id=\"" + panelId + "_installationareaProperties\">" +
    AddEditBox("Name: ", panelId + "installationareaName", "text") +
    AddEditBox("Description: ", panelId + "installationareaDescription", "text") +
    "</div>" +
    "<div class=\"layoutToolPanel_propertyPanel\" style=\"text-align: center;\" id=\"" + panelId + "_noobjectProperties\">" +
    "No object selected" +
    "</div>" +
    "</div>" +
    "</div>";
  return {
    html: panelDIV,
    id: panelId
  };
}

/**
 * Returns HTML code of edit box with given parameters
 *
 */

function AddEditBox(label, id, type, placeholder) {
  var placeholderToPut = "";
  if (placeholder != null && placeholder != "undefined") {
    placeholderToPut = "placeholder=\"" + placeholder + "\"";
  }
  var html = "<div class=\"layoutToolPanel_EditBox\"><div class=\"layoutToolPanel_Label\">" + label + "</div><div class=\"layoutToolPanel_EditValue\"><input style=\"width: 100%; \" type=\"edit\" id=\"" + id + "\" " + placeholderToPut + "/></div></div>";
  return html;
}

/**
 * Returns HTML code of check box with given parameters
 *
 */

function AddCheckBox(label, id) {
  var html = "<div class=\"layoutToolPanel_EditBox\"><div class=\"layoutToolPanel_Label\">" + label + "</div><div class=\"layoutToolPanel_CheckBoxValue\"><input type=\"checkbox\" id=\"" + id + "\"/></div></div>";
  return html;
}

/**
 * Inserts LayoutControl CSS styles onto the page
 *
 */

function addPanelCss(parentContainerId) {
  var css = document.createElement("style");
  var maxHeight = $("#" + parentContainerId).height() - 20;
  css.type = "text/css";
  css.innerHTML =
    "#" + parentContainerId + "_layoutControl_copyright {font-weight: bold; display: block; height: 15px; background: rgb(2, 127, 193); color: white; line-height: 15px; font-size: 10px; padding: 2px; border: 1px solid rgba(0,0,0,0.2); border-top: none;font-family: HelveticaNeue, Helvetica, Arial, sans-serif;}" +
    "#" + parentContainerId + " {margin-bottom: 19px;}" +
    ".layoutToolPanel {position: absolute; top: 10px; left: 10px; z-index: 99; max-height: " + maxHeight + "px; width: 310px; font-family: HelveticaNeue, Helvetica, Arial, sans-serif; margin: 0; overflow-x: hidden; font-size: 16px;}" +
    ".layoutToolPanel_panel {background-color: rgba(255,255,255,0.9);box-shadow: rgba(0,0,0,.5) 1px 0 5px;}" +
    ".layoutToolPanel_header{ padding: 0.8em; width: 100%; font-size:0.8em; color: black; line-height: 1.2; font-weight: bold; box-sizing: border-box;}" +
    ".layoutToolPanel_tabs {width: 100%; background-color: rgb(2, 127, 193); height: 56px; box-sizing: border-box; display: table;}" +
    ".layoutToolPanel_tabs a.lcbtn {width: 33.5%; display: table-cell; color: white; font-size: 0.7em; text-align: center; height: 56px; vertical-align: bottom; cursor: pointer; line-height: 24px; font-style: normal;}" +
    ".layoutToolPanel_tabs a.lcbtn:hover {background-color: rgba(0,95,145,1);}" +
    ".layoutToolPanel_tabs a.lcbtn.active {background-color: rgba(0,95,145,1);}" +
    ".layoutToolPanel_tabPanel {width: 100%;}" +
    ".layoutToolPanel_propertyPanel {font-size: 0.9em; padding: 0.2em; padding-bottom: 1em;}" +
    ".layoutToolPanel_selectPanel {padding: 0.8em; overflow: hidden;}" +
    ".layoutToolPanel_EditBox {padding: 1em; border-bottom: 1px solid #a9a9a9;}" +
    ".layoutToolPanel_propertyPanel .layoutToolPanel_selectPanel, .layoutToolPanel_propertyPanel .layoutToolPanel_EditBox {border-bottom: none;}" +
    ".layoutToolPanel_Label {font-size: 0.8em;}" +
    ".layoutToolPanel_EditValue {width: 100%; overflow: hidden;}" +
    ".layoutToolPanel_SelectValue {box-sizing: border-box; width: calc(100% - 72px); padding: 0 5px; float: left; overflow: hidden;}" +
    ".layoutToolPanel_CheckBoxValue {width: 140px; overflow: hidden;}" +
    ".layoutToolPanel_propertyPanel .layoutToolPanel_Label {color: #333; line-height: 30px; font-size: 1em;}" +
    ".layoutToolPanel_propertyPanel .layoutToolPanel_EditValue input, .layoutToolPanel_propertyPanel .layoutToolPanel_SelectValue input, .layoutToolPanel_propertyPanel .layoutToolPanel_CheckBoxValue input {background-color: rgba(255,255,255,1); border: #d2d2d2 solid 1px; color: #666;height: 32px; line-height: 20px; padding: 5px 10px; font-size: 1em;}" +
    ".layoutToolPanel_propertyPanel .layoutToolPanel_EditBox {padding: 2px 1em;}" +
    ".layoutToolPanel_addressBar .layoutToolPanel_EditBox {border-bottom: none; padding: 16px 0;}" +
    ".layoutToolPanel_addressBar .layoutToolPanel_EditBox input {background: rgba(255,255,255,0.9); font-size: 15px; line-height: 32px; padding: 0 11px; border: 1px solid transparent; border-radius: 2px; box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3); transition: .3s;}" +
    ".layoutToolPanel_addressBar .layoutToolPanel_EditBox input:focus {background: rgba(255,255,255,1); }" +
    ".addRoofBtn {background: url(\"data:image/svg+xml;utf8,%3Csvg%0A%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20id%3D%22Layer_1%22%0A%20%20%20x%3D%220px%22%0A%20%20%20y%3D%220px%22%0A%20%20%20width%3D%2230%22%0A%20%20%20height%3D%2230%22%0A%20%20%20viewBox%3D%220%200%2029.999998%2030%22%3E%0A%20%20%20%3Cpath%20d%3D%22m%209.5068354%2C6.9837084%2020.4931626%2C0%200%2C2.0740148%20-20.4931626%2C0%200%2C-2.0740148%200%2C0%20z%20M%208.0771479%2C30%20l%204.3095701%2C0%200%2C-6.9749%20-4.3095701%2C0%200%2C6.9749%200%2C0%20z%20m%209.2226561%2C-11.612139%200%2C1.359241%200%2C10.252898%20-1.274414%2C0%200%2C-10.252898%20-12.858398%2C0%200%2C10.252898%20-1.274414%2C0%200%2C-10.252898%200%2C-1.359241%201.274414%2C0%2012.858398%2C0%201.274414%2C0%200%2C0%20z%20m%20-5.897461%2C-7.387946%200%2C-1.3563119%201.274414%2C0%2014.156249%2C0%201.274414%2C0%200%2C1.3563119%200%2C19.000085%20-1.274414%2C0%200%2C-19.000085%20-14.156249%2C0%200%2C3.143246%20-1.274414%2C0%200%2C-3.143246%200%2C0%20z%20M%200%2C15.730895%20l%200%2C2.076944%2019.195311%2C0%200%2C-2.076944%20-19.195311%2C0%20z%22%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23ffffff%3Bfill-rule%3Aevenodd%22%20%2F%3E%3C%2Fsvg%3E\") no-repeat center 5px;}" +
    ".addObstructionBtn {background: url(\"data:image/svg+xml;utf8,%3Csvg%0A%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20x%3D%220px%22%0A%20%20%20y%3D%220px%22%0A%20%20%20width%3D%2230%22%0A%20%20%20height%3D%2230%22%0A%20%20%20viewBox%3D%220%200%2030%2029.999998%22%0A%20%20%20enable-background%3D%22new%200%200%20595.275%20841.89%22%3E%0A%3Cpath%0A%20%20%20%20%20d%3D%22m%201.0311489%2C7.1102836%2027.9347601%2C0%201.031149%2C0%200%2C1.0324657%200%2C11.0704797%200%2C2.059079%20-2.062298%2C0%20-2.821012%2C0%200%2C7.478789%201.426617%2C0%200%2C1.248903%20-5.665459%2C0%200%2C-1.248903%200.77922%2C0%200%2C-7.478789%20-13.3082649%2C0%200%2C7.431991%201.1014544%2C0%200%2C1.248904%20-5.6654599%2C0%200%2C-1.248904%201.1014545%2C0%200%2C-7.431991%20-2.8210124%2C0%20-2.0622977%2C0%200%2C-2.059079%200%2C-11.0704797%200%2C-1.0324657%201.0311489%2C0%200%2C0%20z%20m%201.0311488%2C12.1000204%2025.8724623%2C0%200%2C-0.541094%200%2C-9.5027725%20-7.250265%2C0%20-2.062298%2C0%20-7.250266%2C0%20-2.0622973%2C0%20-7.2502654%2C0%200%2C9.5027725%200%2C0.541094%20z%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23ffffff%3Bfill-rule%3Aevenodd%22%20%2F%3E%3Cpath%0A%20%20%20%20%20d%3D%22m%203.169611%2C14.591998%204.6167346%2C3.51272%20-0.6239622%2C0%20-3.9927724%2C-3.035973%200%2C-0.476747%200%2C0%20z%20m%2011.828918%2C-0.637614%205.460402%2C4.153259%20-0.623962%2C0%20-4.83644%2C-3.676511%200%2C-0.0029%20-5.4604021%2C-4.153258%200.6268921%2C0%204.83351%2C3.679435%200%2C0%20z%20m%200%2C3.211463%20-9.0577055%2C-6.890898%20-0.6239622%2C0%209.6816677%2C7.37057%200%2C0%200.612244%2C0.462124%200.623963%2C0%20-1.236207%2C-0.941796%200%2C0%20z%20m%2011.831847%2C-6.592565%20-0.395469%2C-0.301258%200.395469%2C0%200%2C0.301258%200%2C0%20z%20m%20-3.995702%2C-0.298333%20-0.623962%2C0%204.619664%2C3.51272%200%2C-0.473823%20-3.995702%2C-3.038897%200%2C0%20z%20m%20-4.224195%2C0%20-0.626891%2C0%208.846788%2C6.727107%200%2C-0.473823%20-8.219897%2C-6.253284%200%2C0%20z%20m%20-3.61195%2C0.465048%200%2C0%20-0.609315%2C-0.465048%20-0.623963%2C0%201.236207%2C0.938871%200%2C0%209.060635%2C6.890898%200.623962%2C0%20-9.687526%2C-7.364721%200%2C0%20z%20m%20-11.828918%2C7.069313%200%2C0.298333%200.3925396%2C0%20-0.3925396%2C-0.298333%200%2C0%20z%20m%200%2C-6.428775%200%2C0.476748%208.216967%2C6.25036%200.623963%2C0%20-8.84093%2C-6.727108%20z%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23ffffff%3Bfill-rule%3Aevenodd%22%20%2F%3E%3Crect%0A%20%20%20%20%20x%3D%220.0029429884%22%0A%20%20%20%20%20y%3D%220%22%0A%20%20%20%20%20width%3D%2229.997057%22%0A%20%20%20%20%20height%3D%2229.950254%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3Anone%3Bfill-rule%3Aevenodd%22%20%2F%3E%3C%2Fsvg%3E\") no-repeat center 5px;}" +
    ".layoutToolPanel_obstructionDrawerDropdown {display: none; position: absolute; width: 100px; right: 0; background: white; font-size: 0.8em; top: 136px; border: 1px solid rgb(0, 83, 128); z-index: 1;}" +
    ".layoutToolPanel_obstructionDrawerDropdown a {display: block; padding: 3px 10px; cursor: pointer; color: black !important; font-style: normal !important;}" +
    ".layoutToolPanel_obstructionDrawerDropdown a:hover {background: rgb(2, 127, 193); color: white !important;}" +
    ".addInstallationAreaBtn {background: url(\"data:image/svg+xml;utf8,%3Csvg%0A%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20x%3D%220px%22%0A%20%20%20y%3D%220px%22%0A%20%20%20width%3D%2230%22%0A%20%20%20height%3D%2230%22%0A%20%20%20viewBox%3D%220%200%2029.999999%2029.999999%22%0A%20%20%20enable-background%3D%22new%200%200%20595.275%20841.89%22%0A%3E%3Cpath%0A%20%20%20%20%20d%3D%22m%201.0311492%2C3.5762783e-6%2027.9376978%2C0%201.031149%2C0%200%2C1.0311493237217%200%2C27.9376981%200%2C1.031149%20-1.031149%2C0%20L%201.0311492%2C30%200%2C30%200%2C28.968851%200%2C1.0311529%200%2C3.5762783e-6%20l%201.0311492%2C0%200%2C0%20z%20M%209.312566%2C2.0593727%20l%20-7.2502676%2C0%200%2C7.0774337%200%2C2.0622986%200%2C7.250265%200%2C2.062299%200%2C7.426032%207.2502676%2C0%202.062298%2C0%207.250268%2C0%202.062298%2C0%207.250268%2C0%200%2C-7.426032%200%2C-2.062299%200%2C-7.250265%200%2C-2.0622986%200%2C-7.0774337%20-7.250268%2C0%20-2.062298%2C0%20-7.250268%2C0%20-2.062298%2C0%20z%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23ffffff%3Bfill-rule%3Aevenodd%22%20%2F%3E%3Cpath%0A%20%20%20%20%20d%3D%22m%203.4449757%2C14.201741%2012.3503553%2C12.350354%20-1.119032%2C0%20-11.2313233%2C-11.234252%200%2C-1.116102%200%2C0%20z%20m%200%2C7.546135%200%2C1.119032%203.6881159%2C3.688116%201.1161018%2C0%20-4.8042177%2C-4.807148%200%2C0%20z%20M%2022.866905%2C3.4420502%20l%203.688116%2C3.6881163%200%2C1.1161019%20-4.804218%2C-4.8042182%201.116102%2C0%200%2C0%20z%20m%20-7.543208%2C0%20-1.116102%2C0%2012.347426%2C12.3474268%200%2C-1.116102%20-11.231324%2C-11.2313248%200%2C0%20z%20m%20-7.5461371%2C0%20-1.1161019%2C0%2019.893563%2C19.8935618%200%2C-1.116102%20-18.7774611%2C-18.7774598%200%2C0%20z%20m%20-4.3325842%2C3.2135533%200%2C1.1190313%2018.7774613%2C18.7774602%201.119031%2C0%20L%203.4449757%2C6.6556035%20Z%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23ffffff%3Bfill-rule%3Aevenodd%22%20%2F%3E%3C%2Fsvg%3E\") no-repeat center 5px;}" +
    ".addDeleteBtn {background: url(\"data:image/svg+xml;utf8,%3Csvg%0A%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%0A%20%20%20version%3D%221.1%22%0A%20%20%20x%3D%220px%22%0A%20%20%20y%3D%220px%22%0A%20%20%20width%3D%2220%22%0A%20%20%20height%3D%2220%22%0A%20%20%20viewBox%3D%220%200%2019.999999%2020%22%0A%20%20%20enable-background%3D%22new%200%200%20595.275%20841.89%22%0A%3E%3Cpath%0A%20%20%20%20%20d%3D%22M%202.5994584%2C0%20C%201.9308477%2C0%201.262237%2C0.25410691%200.75817538%2C0.756093%200.25411373%2C1.2601521%200%2C1.9287658%200%2C2.5994568%200%2C3.2701521%200.25203083%2C3.9408475%200.75192668%2C4.4407432%20l%200.006249%2C0%20L%205.2697358%2C8.9502173%206.3174342%2C10%205.2697358%2C11.049783%200.76025858%2C15.559257%20C%200.25619663%2C16.06332%200.0020829%2C16.73193%200.0020829%2C17.400543%20c%200%2C0.672773%200.25203083%2C1.343468%200.75192668%2C1.841282%20l%200.006249%2C0.0061%20C%201.2601541%2C19.74797%201.9287648%2C20%202.6015413%2C20%203.2701521%2C20%203.9387628%2C19.745893%204.4428244%2C19.241825%20l%204.5115601%2C-4.509478%201.0476985%2C-1.049783%201.047698%2C1.049783%204.51156%2C4.509478%200%2C0.0061%20C%2016.061237%2C19.74797%2016.729848%2C20%2017.402624%2C20%2018.071235%2C20%2018.739846%2C19.745893%2019.243908%2C19.241825%2019.747969%2C18.739848%2020%2C18.069153%2020%2C17.400543%2020%2C16.73193%2019.745886%2C16.059153%2019.24599%2C15.557175%20l%20-0.0021%2C0.0021%20L%2014.732329%2C11.049783%2013.684631%2C10%2014.732329%2C8.9502173%2019.24389%2C4.4407432%20C%2019.747969%2C3.9366797%2020%2C3.2680704%2020%2C2.5994568%2020%2C1.9266841%2019.745886%2C1.2580704%2019.24599%2C0.7540113%20l%20-0.0021%2C0.002082%200%2C0%20C%2018.739828%2C0.2541156%2018.071217%2C0%2017.404689%2C0%2016.736079%2C0%2016.067468%2C0.25410691%2015.563406%2C0.756093%20l%200%2C0%20-4.51156%2C4.5115602%20L%2010.004148%2C6.3174359%208.9543665%2C5.2676532%204.4428064%2C0.756093%204.4448864%2C0.7540113%20C%203.9408457%2C0.2541156%203.272235%2C0%202.5994584%2C0%20Z%22%0A%20%20%20%20%20style%3D%22clip-rule%3Aevenodd%3Bfill%3A%23027fc1%3Bfill-rule%3Aevenodd%22%20%2F%3E%3C%2Fsvg%3E\") no-repeat center; width: 32px; height: 32px; float: left; display: block; cursor: pointer; background-size: 12px;}" +
    ".layoutToolPanel .layoutToolPanel_SelectValue select {height: 20px; background-color: #FFF; border: #D2D2D2 solid 1px; height: 32px; transition: .3s; width: 100%;}" +
    "span.objectColor {width: 40px; height: 32px; float: right;}" +
    "span.hint {color: #999;}";
  document.body.appendChild(css);
}

function initObstructionDrawerDropdownEvents() {
  $(".addObstructionBtn").click(function() {
    var dropdown = $(this).next(".layoutToolPanel_obstructionDrawerDropdown");
    $(dropdown).show();
    // close if clicked
    setTimeout(function() {
      $(document).on('click', function(e) {
        $(dropdown).hide();
        $(document).unbind("click");
      });
    }, 100);
  });
}

// =========== HELPER FUNCTIONS ================

// Edge sum negative - clockwise

function Area2DEC(a, b, c) {
  return (b.x - a.x) * (c.y - a.y) - (c.x - a.x) * (b.y - a.y);
}

function AreaPoly2(polygon2d) {
  if (!polygon2d instanceof Polygon2D)
    throw 'LayoutControl[AreaPoly2]: 1st parameter should Polygon2D object';

  var sum = 0,
    points = polygon2d.vertices,
    count = points.length;

  for (var i = 1; i < count - 1; i++) {
    sum += Area2DEC(points[0], points[i], points[i + 1]);
  }

  return sum;
}

function IsPolygonPointsClockwise(object) {
  if (!object instanceof google.maps.Polygon)
    throw 'LayoutControl[IsPolygonPointsClockwise]: 1st parameter should be google.maps.Polygon object';

  var AreaPoly = AreaPoly2(GetPolygon2D(object));
  return AreaPoly < 0;
}

function ConvertPolygon2DToVELatLong(polygon2d) {
  var toReturn = [];
  polygon2d.vertices.forEach(function(vertex) {
    toReturn.push(new VELatLong(vertex.y, vertex.x));
  });
  return toReturn;
}

//https://rbrundritt.wordpress.com/2008/10/20/determine-if-two-polygons-overlap/
//poly1 and poly2 are arrays of VELatlongs that represent polygons

function ArePolygonsOverlapped(poly1, poly2) {
  if (poly1.length >= 3 && poly2.length >= 3) {
    //close polygons
    poly1.push(poly1[0]);
    poly2.push(poly2[0]);

    for (var i = 0; i < poly1.length - 1; i++) {
      for (var k = 0; k < poly2.length - 1; k++) {
        if (SimplePolylineIntersection(poly1[i], poly1[i + 1], poly2[k], poly2[k + 1]) != null)
          return true;
      }
    }

    return false;
  }

  return null;
}

// https://rbrundritt.wordpress.com/2008/10/20/approximate-points-of-intersection-of-two-line-segments/

function SimplePolylineIntersection(latlong1, latlong2, latlong3, latlong4) {
  //Line segment 1 (p1, p2)
  var A1 = latlong2.Latitude - latlong1.Latitude;
  var B1 = latlong1.Longitude - latlong2.Longitude;
  var C1 = A1 * latlong1.Longitude + B1 * latlong1.Latitude;

  //Line segment 2 (p3,  p4)
  var A2 = latlong4.Latitude - latlong3.Latitude;
  var B2 = latlong3.Longitude - latlong4.Longitude;
  var C2 = A2 * latlong3.Longitude + B2 * latlong3.Latitude;

  var determinate = A1 * B2 - A2 * B1;

  var intersection;
  if (determinate != 0) {
    var x = (B2 * C1 - B1 * C2) / determinate;
    var y = (A1 * C2 - A2 * C1) / determinate;

    var intersect = new VELatLong(y, x);

    if (inBoundedBox(latlong1, latlong2, intersect) &&
      inBoundedBox(latlong3, latlong4, intersect))
      intersection = intersect;
    else
      intersection = null;
  } else //lines are parrallel
    intersection = null;

  return intersection;
}

var VELatLong = function(y, x) {
  this.Latitude = y;
  this.Longitude = x;
}

//latlong1 and latlong2 represent two coordinates that make up the bounded box
//latlong3 is a point that we are checking to see is inside the box

function inBoundedBox(latlong1, latlong2, latlong3) {
  var betweenLats;
  var betweenLons;

  if (latlong1.Latitude < latlong2.Latitude)
    betweenLats = (latlong1.Latitude <= latlong3.Latitude &&
      latlong2.Latitude >= latlong3.Latitude);
  else
    betweenLats = (latlong1.Latitude >= latlong3.Latitude &&
      latlong2.Latitude <= latlong3.Latitude);

  if (latlong1.Longitude < latlong2.Longitude)
    betweenLons = (latlong1.Longitude <= latlong3.Longitude &&
      latlong2.Longitude >= latlong3.Longitude);
  else
    betweenLons = (latlong1.Longitude >= latlong3.Longitude &&
      latlong2.Longitude <= latlong3.Longitude);

  return (betweenLats && betweenLons);
}

function GetCentroid(paths) {
  var bounds = new google.maps.LatLngBounds();
  var i;

  for (i = 0; i < paths.length; i++) {
    bounds.extend(new google.maps.LatLng(paths[i].y, paths[i].x));
  }

  return bounds.getCenter();
}

function SetPolygonFillColor(polygon, color) {
  polygon.setOptions({
    fillColor: color
  });
}

function SetPolygonStrokeColor(polygon, color) {
  polygon.setOptions({
    strokeColor: color
  });
}

function SetFullPolygonColor(polygon, color) {
  SetPolygonFillColor(polygon, color);
  SetPolygonStrokeColor(polygon, color);
}

function computeMidpoint(p1, p2) {
  if (!(p1 instanceof google.maps.LatLng) || !(p2 instanceof google.maps.LatLng))
    throw 'computeMidpoint: p1 and p2 should be Google maps LatLng object';

  var lat = (p1.lat() + p2.lat()) / 2;
  var lng = (p1.lng() + p2.lng()) / 2;

  return new google.maps.LatLng(lat, lng);
}

function pixelToLatlng(xcoor, ycoor, map) {
  var ne = map.getBounds().getNorthEast();
  var sw = map.getBounds().getSouthWest();
  var projection = map.getProjection();
  var topRight = projection.fromLatLngToPoint(ne);
  var bottomLeft = projection.fromLatLngToPoint(sw);
  var scale = 1 << map.getZoom();
  var newLatlng = projection.fromPointToLatLng(new google.maps.Point(xcoor / scale + bottomLeft.x, ycoor / scale + topRight.y));
  return newLatlng;
};

function fromLatLngToPoint(latLng, map) {
  var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
  var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
  var scale = Math.pow(2, map.getZoom());
  var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
  return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
}


function getBoundsZoomLevel(bounds, mapDim) {
  var WORLD_DIM = {
    height: 256,
    width: 256
  };
  var ZOOM_MAX = 21;

  function latRad(lat) {
    var sin = Math.sin(lat * Math.PI / 180);
    var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
    return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
  }

  function zoom(mapPx, worldPx, fraction) {
    return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
  }

  var ne = bounds.getNorthEast();
  var sw = bounds.getSouthWest();

  var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

  var lngDiff = ne.lng() - sw.lng();
  var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

  var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
  var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

  return Math.min(latZoom, lngZoom, ZOOM_MAX);
}

/**
 *   Snap marker to closest point on a line.
 *
 *   Based on Distance to line example by
 *   Marcelo, maps.forum.nu - http://maps.forum.nu/gm_mouse_dist_to_line.html
 *   Then
 *   @ work of BjÃ¶rn Brala - Swis BV who wrapped the algorithm in a class operating on GMap Objects
 *   And now
 *   Bill Chadwick, who factored the basic algorithm out of the class (removing much intermediate storage of results)
 *       and added distance along line to nearest point calculation
 *   Followed by
 *   Robert Crowe, who ported it to v3 of the Google Maps API and factored out the marker to make it more general.
 *
 *   Usage:
 *
 *   Create the class
 *       var oSnap = new cSnapToRoute();
 *
 *   Initialize the subjects
 *       oSnap.init(oMap, oPolyline);
 *
 **/

function cSnapToRoute() {

  this.routePoints = Array();
  this.routePixels = Array();
  this._oMap;
  this._oPolyline;

  /**
   *   @desc Initialize the objects.
   *   @param Map object
   *   @param GPolyline object - the 'route'
   **/
  this.init = function(oMap, oPolyline) {
    this._oMap = oMap;
    this._oPolyline = oPolyline;

    this.loadRouteData(); // Load needed data for point calculations
  }

  /**
   *   @desc internal use only, Load route points into RoutePixel array for calculations, do this whenever zoom changes
   **/
  this.loadRouteData = function() {
    this.routePixels = new Array();
    var proj = this._oMap.getProjection();
    for (var i = 0; i < this._oPolyline.getPath().getLength(); i++) {
      var Px = proj.fromLatLngToPoint(this._oPolyline.getPath().getAt(i));
      this.routePixels.push(Px);
    }
  }

  /**
   *   @desc Get closest point on route to test point
   *   @param GLatLng() the test point
   *   @return new GLatLng();
   **/
  this.getClosestLatLng = function(latlng) {
    var r = this.distanceToLines(latlng);
    var proj = this._oMap.getProjection();
    return proj.fromPointToLatLng(new google.maps.Point(r.x, r.y));
  }

  /**
   *   @desc Get distance along route in meters of closest point on route to test point
   *   @param GLatLng() the test point
   *   @return distance in meters;
   **/
  this.getDistAlongRoute = function(latlng) {
    var r = this.distanceToLines(latlng);
    return this.getDistToLine(r.i, r.fTo);
  }

  /**
   *   @desc internal use only, gets test point xy and then calls fundamental algorithm
   **/
  this.distanceToLines = function(thisLatLng) {
    var tm = this._oMap;
    var proj = this._oMap.getProjection();
    var thisPx = proj.fromLatLngToPoint(thisLatLng);
    var routePixels = this.routePixels;
    return getClosestPointOnLines(thisPx, routePixels);
  }

  /**
   *   @desc internal use only, find distance along route to point nearest test point
   **/
  this.getDistToLine = function(iLine, fTo) {

    var routeOverlay = this._oPolyline;
    var d = 0;
    for (var n = 1; n < iLine; n++) {
      // d += routeOverlay.getPath().getAt(n - 1).distanceFrom(routeOverlay.getPath().getAt(n));
      d += google.maps.geometry.spherical.computeDistanceBetween(routeOverlay.getPath().getAt(n - 1), routeOverlay.getPath().getAt(n));
    }
    // d += routeOverlay.getPath().getAt(iLine - 1).distanceFrom(routeOverlay.getPath().getAt(iLine)) * fTo;
    d += google.maps.geometry.spherical.computeDistanceBetween(routeOverlay.getPath().getAt(iLine - 1), routeOverlay.getPath().getAt(iLine)) * fTo;

    return d;
  }


}

/* desc Static function. Find point on lines nearest test point
 test point pXy with properties .x and .y
 lines defined by array aXys with nodes having properties .x and .y
 return is object with .x and .y properties and property i indicating nearest segment in aXys
 and property fFrom the fractional distance of the returned point from aXy[i-1]
 and property fTo the fractional distance of the returned point from aXy[i]   */

function getClosestPointOnLines(pXy, aXys) {

  var minDist;
  var fTo;
  var fFrom;
  var x;
  var y;
  var i;
  var dist;

  if (aXys.length > 1) {

    for (var n = 1; n < aXys.length; n++) {

      if (aXys[n].x != aXys[n - 1].x) {
        var a = (aXys[n].y - aXys[n - 1].y) / (aXys[n].x - aXys[n - 1].x);
        var b = aXys[n].y - a * aXys[n].x;
        dist = Math.abs(a * pXy.x + b - pXy.y) / Math.sqrt(a * a + 1);
      } else
        dist = Math.abs(pXy.x - aXys[n].x)

      // length^2 of line segment
      var rl2 = Math.pow(aXys[n].y - aXys[n - 1].y, 2) + Math.pow(aXys[n].x - aXys[n - 1].x, 2);

      // distance^2 of pt to end line segment
      var ln2 = Math.pow(aXys[n].y - pXy.y, 2) + Math.pow(aXys[n].x - pXy.x, 2);

      // distance^2 of pt to begin line segment
      var lnm12 = Math.pow(aXys[n - 1].y - pXy.y, 2) + Math.pow(aXys[n - 1].x - pXy.x, 2);

      // minimum distance^2 of pt to infinite line
      var dist2 = Math.pow(dist, 2);

      // calculated length^2 of line segment
      var calcrl2 = ln2 - dist2 + lnm12 - dist2;

      // redefine minimum distance to line segment (not infinite line) if necessary
      if (calcrl2 > rl2)
        dist = Math.sqrt(Math.min(ln2, lnm12));

      if ((minDist == null) || (minDist > dist)) {
        if (calcrl2 > rl2) {
          if (lnm12 < ln2) {
            fTo = 0; //nearer to previous point
            fFrom = 1;
          } else {
            fFrom = 0; //nearer to current point
            fTo = 1;
          }
        } else {
          // perpendicular from point intersects line segment
          fTo = ((Math.sqrt(lnm12 - dist2)) / Math.sqrt(rl2));
          fFrom = ((Math.sqrt(ln2 - dist2)) / Math.sqrt(rl2));
        }
        minDist = dist;
        i = n;
      }
    }

    var dx = aXys[i - 1].x - aXys[i].x;
    var dy = aXys[i - 1].y - aXys[i].y;

    x = aXys[i - 1].x - (dx * fTo);
    y = aXys[i - 1].y - (dy * fTo);

  }

  return {
    'x': x,
    'y': y,
    'i': i,
    'fTo': fTo,
    'fFrom': fFrom
  };
}

/**
 * Utility function for highlighting objects on the map
 */
LayoutControl.prototype.getHighlightOptions = function(object) {

  return {
    fillColor: layoutControlElementColors[object.type],
    strokeColor: layoutControlElementColors.Unclassified,
    strokeWeight: 2,
    fillOpacity: 0.6
  }
};

/**
 * Utility function for removing highlight from objects on the map
 */
LayoutControl.prototype.getStandardOptions = function(object) {

  return {
    fillColor: layoutControlElementColors[object.type],
    strokeColor: layoutControlElementColors[object.type],
    strokeWeight: 2,
    fillOpacity: 0.2
  }
};

LayoutControl.prototype.setScrollwheel = function(scroll) {
  this.map.setOptions({
    scrollwheel: scroll
  });
};

/**
 * Sends request to layout service and renders shadows
 * Returns: array of shadows points
 */
LayoutControl.prototype.DoLayoutShadows = function(PVModuleId, callback, errCb) {
  var inputs = [],
    parapetObstructions = [],
    lc = this;

  this.GetRoofs().forEach(function(roof) {
    inputs.push(GetDoLayoutInputsFromSelectedObject(roof, lc));
  });

  var lc = this;
  var project = lc.GetProject();
  project.roofs.forEach(function(roof) {
    delete roof.modules;

    delete roof.rails;

    delete roof.splices;

    delete roof.attachments;

    if (roof.parapetWallObjects) {
      parapetObstructions = parapetObstructions.concat(angular.copy(roof.parapetWallObjects));
    }
    delete roof.parapetWallObjects;

    if (roof.roofFaces[0].layouts) {
      roof.roofFaces[0].layouts.forEach(function(layout) {
        delete layout.layoutUnits;
      });
    }
  });

  var project = angular.copy(project);

  if (project.roofs.length === 0 && project.obstructions.length === 0) {
    lc.ClearShadows();
    return;
  }

  project.roofs.forEach(function(roof) {

    roof.modules = [];

    //Convert azimuth from North directed to South
    var azimuth = 180 - parseFloat(roof.roofFaces[0].azimuth);
    if (azimuth > 180)
      azimuth -= 360;

    //Convert feet to inches
    var setback = roof.setback;
    if (lc.measurementSystem == 3) {
      setback = lc.ConvertNumberFromTo(roof.setback, 2, 3, 5);
    }

    roof.roofFaces[0].azimuth = azimuth;
    roof.setback = setback;

    if ((isNaN(roof.meanHeight) || roof.meanHeight == undefined || roof.meanHeight == '') && roof.meanHeight !== 0) {
      roof.meanHeight = 0;
    }

    if ((isNaN(roof.setback) || roof.meanHeight == setback || roof.setback == '') && roof.setback !== 0) {
      roof.setback = 0;
    }

    if ((isNaN(roof.roofFaces[0].slope) || roof.roofFaces[0].slope == undefined || roof.roofFaces[0].slope == '') && roof.roofFaces[0].slope !== 0) {
      roof.roofFaces[0].slope = 0;
    }
  });

  parapetObstructions.forEach(function(item) {
    var parapet = item.data;
    parapet.parentRoof = parapet.parentRoof.index;
    parapet.shape = parapet.primitive.shapeType;
    delete parapet.primitive;
    project.obstructions.push(parapet);
  });
  project.obstructions.forEach(function(obstruction) {
    //Check if obstruction height not empty
    if ((isNaN(obstruction.height) || obstruction.height == undefined || obstruction.height == '') && obstruction.height !== 0) {
      obstruction.height = 0;
    }

    //Convert feet to inches
    var setback = obstruction.setback;
    if (lc.measurementSystem == 3) {
      setback = lc.ConvertNumberFromTo(obstruction.setback, 2, 3, 5);
    }
    obstruction.setback = setback;

    if ((isNaN(obstruction.setback) || obstruction.setback == undefined || obstruction.setback == '') && obstruction.setback !== 0) {
      obstruction.setback = 0;
    }
  });

  inputs.forEach(function(input) {
    var dataToSend = {
      Project: project,
      ModuleId: PVModuleId,
      isPVSketch: true
    };

    // console.log(JSON.stringify(dataToSend));
    $.ajax({
      url: lc.shadowsServiceUrl,
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(dataToSend),
      dataType: 'json',
      success: function(data) {
        lc.AddShadows(data);

        if (callback) {
          callback(data);
        }
      },
      complete: function() {},
      error: function(error) {
        console.log(error);
        if (typeof errCb == 'function')
          errCb(error);
      }
    });
  });
};

LayoutControl.prototype.ClearShadows = function(object) {
  if (this.shadows == undefined) {
    this.shadows = [];
  }

  if (object) {
    var shadow = this.GetShadowByObject(object);
    if (shadow) {
      var index = this.shadows.indexOf(shadow);
      this.canvas.removePolygon(this.shadows[index].primitive);
      this.shadows.splice(index, 1);
    }
  } else {
    var shadowsCount = this.shadows.length;
    for (var i = 0; i < shadowsCount; i++) {
      this.canvas.removePolygon(this.shadows[i].primitive);
    }
    this.shadows.length = 0;
    this.lastIndexes.Shadow = 0;
  }
};

LayoutControl.prototype.TransformObject = function(object, type) {
  var lc = this;

  var ANGLE_DIFF = 5;

  if (object.primitive.shapeType === "circle") {
    return;
  }

  var roofLines = object.GetPolygonLines();

  // Set first line as primary
  var primaryLine = roofLines[0];

  // Closest line to south or north
  var closestLine = roofLines[0];

  // Northern hemisphere by default
  var northernHemisphere = true;
  if (primaryLine.p1.lat() < 0) {
    // Southern hemisphere
    northernHemisphere = false;
  }

  for (var i = 0; i < roofLines.length; i++) {
    var line = roofLines[i];
    // Getting distance between points
    var length = google.maps.geometry.spherical.computeDistanceBetween(line.p1, line.p2);
    line.length = length;
    // Getting line direction angle
    var lineDirection = google.maps.geometry.spherical.computeHeading(line.p1, line.p2);

    // Convert 360 degrees into 180
    if (lineDirection > 180)
      lineDirection -= 180;
    if (lineDirection < 0)
      lineDirection += 180;

    line.direction = lineDirection;
    line.index = i;

    // Find primary line
    if (length > primaryLine.length) {
      primaryLine = line;
    }

    // Find closest line north
    var azimuth = null;
    var prj = lc.map.getProjection();
    var cP1 = prj.fromLatLngToPoint(closestLine.p1);
    var cP2 = prj.fromLatLngToPoint(closestLine.p2);
    var p1 = prj.fromLatLngToPoint(line.p1);
    var p2 = prj.fromLatLngToPoint(line.p2);
    if ((p1.y <= cP2.y) && (p2.y <= cP1.y)) {
      closestLine = line;
    }

    // Set all lines as NOT primary
    primaryLine.primary = false;
  }

  // Set primary line as primary
  primaryLine.primary = true;

  var newPath = [];
  var linesSquared = false;

  for (var i = 0; i < roofLines.length; i++) {

    var line = roofLines[i];

    if (closestLine.index == primaryLine.index) {
      // Primary vertex is closes line to south or north
      // Make azimuth perpendicular to primary vertex line

      if (northernHemisphere) {
        // Northern hemisphere
        azimuth = closestLine.direction + 90;
      } else {
        // Southern hemisphere
        azimuth = closestLine.direction - 90;
      }
    }

    if (primaryLine.index != line.index) {
      // Find secondary line

      var angleDiff = lc.getAngle(primaryLine, line);

      // Transform paralel lines
      if (Math.abs(angleDiff) < ANGLE_DIFF) {

        var rotateAngle = angleDiff;

        var midPointOfLine = computeMidpoint(line.p1, line.p2);

        var rotatedLine = lc.RotateLine(line, rotateAngle, midPointOfLine);
        //Check rotated line angle
        var checkDiff = lc.getAngle(primaryLine, rotatedLine);
        var roundedDiff = Math.round(checkDiff * 1000) / 1000;
        if (roundedDiff === 0) {
          line.p1 = rotatedLine.p1;
          line.p2 = rotatedLine.p2;
        } else {
          //Rotate line to apposite direction
          rotatedLine = lc.RotateLine(line, -rotateAngle, midPointOfLine);
          line.p1 = rotatedLine.p1;
          line.p2 = rotatedLine.p2;
        }

        if (newPath.length === 0) {
          newPath.push(line.p1);
        } else {
          newPath[newPath.length - 1] = line.p1;
        }

        newPath.push(line.p2);

        if (closestLine.index == line.index) {
          // Rotated vertex is closes line to south or north
          // Make azimuth perpendicular to rotated vertex line
          var lineDirection = google.maps.geometry.spherical.computeHeading(line.p1, line.p2);
          // Convert 360 degrees into 180
          if (lineDirection < 0)
            lineDirection += 180;
          line.direction = lineDirection;
          azimuth = line.direction + 90;

          if (northernHemisphere) {
            // Northern hemisphere
            azimuth = line.direction + 90;
          } else {
            // Southern hemisphere
            azimuth = line.direction - 90;
          }
        }

        linesSquared = true;
      } else {
        if (newPath.length < roofLines.length) {
          newPath.push(line.p2);
        }
      }
    } else {
      if (newPath.length < roofLines.length) {
        newPath.push(line.p2);
      }
    }
  }

  // Transform perpedicular lines
  for (var i = 0; i < newPath.length; i++) {

    // Get lines from path array
    var prevLine;
    var lineToRotate;
    var nextLine;
    if (i === 0) {
      // First point
      lineToRotate = {
        p1: newPath[0],
        p2: newPath[1]
      };
      prevLine = {
        p1: newPath[newPath.length - 1],
        p2: newPath[0]
      };
      nextLine = {
        p1: newPath[1],
        p2: newPath[2]
      };
    } else if (i === newPath.length - 1) {
      // Last Point
      lineToRotate = {
        p1: newPath[i],
        p2: newPath[0]
      };
      prevLine = {
        p1: newPath[i - 1],
        p2: newPath[i]
      };
      nextLine = {
        p1: newPath[0],
        p2: newPath[1]
      };
    } else {
      // Not first not last point
      lineToRotate = {
        p1: newPath[i],
        p2: newPath[i + 1]
      };
      prevLine = {
        p1: newPath[i - 1],
        p2: newPath[i]
      };
      var nextLineP2Index = i < newPath.length - 2 ? i + 2 : 0;
      nextLine = {
        p1: newPath[i + 1],
        p2: newPath[nextLineP2Index]
      };
    }

    var angleDiff = lc.getAngle(primaryLine, lineToRotate);

    // Check if line perpendicular +-5 degree
    if (Math.abs(angleDiff) > (90 - ANGLE_DIFF) && Math.abs(angleDiff) < (90 + ANGLE_DIFF)) {

      var simpleRotate = true;
      // Check is prev Line is paralel to primary Line
      var checkDiff = lc.getAngle(primaryLine, prevLine);
      var roundedDiff = Math.round(checkDiff * 1000) / 1000;
      if (roundedDiff === 0 || roundedDiff === 180) {
        // Prev line is paralel to primary line,
        // need to rotate lineToRotate and not change direction of prev line
        simpleRotate = false;

        rotatedLine = lc.calculatePerpendicularLine(prevLine.p1, prevLine.p2, lineToRotate.p1, lineToRotate.p2);

        // Redefine point is path array
        newPath[i] = rotatedLine.p1;
        if (i < newPath.length - 1) {
          newPath[i + 1] = rotatedLine.p2;
        } else {
          newPath[0] = rotatedLine.p2;
        }
        continue;
      }

      // Check is next Line is paralel to primary Line
      var checkDiff = lc.getAngle(primaryLine, nextLine);
      var roundedDiff = Math.round(checkDiff * 1000) / 1000;
      if (roundedDiff === 0 || roundedDiff === 180) {
        // Next line is paralel to primary line,
        // need to rotate lineToRotate and not change direction of next line
        simpleRotate = false;

        rotatedLine = lc.calculatePerpendicularLine(nextLine.p2, nextLine.p1, lineToRotate.p2, lineToRotate.p1);

        // Redefine point is path array
        newPath[i] = rotatedLine.p2;
        if (i < newPath.length - 1) {
          newPath[i + 1] = rotatedLine.p1;
        } else {
          newPath[0] = rotatedLine.p1;
        }
        continue;
      }

      if (simpleRotate === true) {
        // Just rotate line and replace point in newPath array
        var rotatedLine = rotateLineToPerpendicular(lc, primaryLine, lineToRotate);

        // Redefine point is path array
        newPath[i] = rotatedLine.p2;
        if (i < newPath.length - 1) {
          newPath[i + 1] = rotatedLine.p1;
        } else {
          newPath[0] = rotatedLine.p1;
        }
      }

      linesSquared = true;
    }
  }

  // Primary or secondary vertex not closes to south or north
  if (azimuth == null) {
    // Make azimuth paralel to primary vertex

    if (northernHemisphere) {
      // Northern hemisphere
      if (primaryLine.direction < 90) {
        //Direct to south if in northern hemisphere
        azimuth = primaryLine.direction + 180;
      } else {
        azimuth = primaryLine.direction;
      }
    } else {
      // Southern hemisphere
      if (primaryLine.direction > 90) {
        //Direct to north if in Southern hemisphere
        azimuth = primaryLine.direction - 180;
      } else {
        azimuth = primaryLine.direction;
      }
    }
  }

  // Set new path of primitive
  object.primitive.setPath(newPath);

  // If roofFaces exist
  if (object.roofFaces && object.roofFaces[0]) {
    object.roofFaces[0].polygon2dLocal = GetPolygon2D(object.primitive);

    // Set roof azimuth direction
    object.roofFaces[0].azimuth = azimuth;
  } else {
    object.polygon2dLocal = GetPolygon2D(object.primitive);
  }

  if (linesSquared === true) {
    if (type === "roof") {
      displayToastrMessage("1 or more roof edges squared.");
    } else if (type === "obstruction") {
      displayToastrMessage("1 or more obstruction edges squared.");
    }
  }
};

LayoutControl.prototype.calculatePerpendicularLine = function(line1p1, line1p2, line2p1, line2p2) {

  // Triangle points
  //
  // line1p1
  // |
  // |
  // |
  // |
  // |
  // |
  //line1p2 |
  // --------------- line2p2
  // line2p1


  var lc = this;

  // line1p1, line1p2 are points of paralelToPrimaryLine
  var paralelToPrimaryLine = {
    p1: line1p1,
    p2: line1p2
  };
  // line2p1, line2p2 are points of line to rotate
  var line = {
    p1: line2p1,
    p2: line2p2
  };

  // Calcalatings hypotenuse (distance between center points of paralelToPrimaryLine and line)
  var midPointOfPrimaryLine = computeMidpoint(paralelToPrimaryLine.p1, paralelToPrimaryLine.p2);
  var midPointOfLine = computeMidpoint(line.p1, line.p2);

  var hypotenuseLine = {
    p1: midPointOfPrimaryLine,
    p2: midPointOfLine
  };
  var hypotenuseLenght = google.maps.geometry.spherical.computeDistanceBetween(hypotenuseLine.p1, hypotenuseLine.p2);

  var hypotenuseAngle = lc.getAngle(paralelToPrimaryLine, hypotenuseLine);
  hypotenuseAngle = (Math.abs(hypotenuseAngle) > 90) ? 180 - Math.abs(hypotenuseAngle) : hypotenuseAngle;

  var angleRad = hypotenuseAngle * Math.PI / 180.0;

  var newLineLenght = hypotenuseLenght * Math.cos(angleRad);

  //Get direction of primary line
  var lineDirection = google.maps.geometry.spherical.computeHeading(paralelToPrimaryLine.p1, paralelToPrimaryLine.p2);

  // Get Perpendicular point
  var newPoint1 = google.maps.geometry.spherical.computeOffset(hypotenuseLine.p1, newLineLenght, lineDirection);

  // Perpendicular line length to middle
  var perpLineToCenterLenght = google.maps.geometry.spherical.computeDistanceBetween(newPoint1, midPointOfLine);
  // Perpendicular line direction
  var perpLineDirection = google.maps.geometry.spherical.computeHeading(newPoint1, midPointOfLine);
  // Get Perpendicular line second point
  var newPoint2 = google.maps.geometry.spherical.computeOffset(midPointOfLine, perpLineToCenterLenght, perpLineDirection);

  return {
    p1: newPoint1,
    p2: newPoint2
  };
};

LayoutControl.prototype.getAngle = function(line1, line2) {
  var bearing1 = google.maps.geometry.spherical.computeHeading(line1.p1, line1.p2);
  var bearing2 = google.maps.geometry.spherical.computeHeading(line2.p1, line2.p2);
  var angle = this.getDifference(bearing1, bearing2);
  return angle;
};

LayoutControl.prototype.getDifference = function(a1, a2) {
  var _al = (a1 > 0) ? a1 : 360 + a1;
  var _a2 = (a2 > 0) ? a2 : 360 + a2;
  var angle = Math.abs(_al - _a2) + 180;
  if (angle > 180) {
    angle = 360 - angle;
  }
  return angle;
};

LayoutControl.prototype.RotateLine = function(line, angle, lineCenter) {
  var map = this.map;
  var prj = map.getProjection();

  // Before rotate points, need to convert from lat long, to pixels
  var origin = prj.fromLatLngToPoint(lineCenter);
  var p1 = prj.fromLatLngToPoint(line.p1);
  var p2 = prj.fromLatLngToPoint(line.p2);

  var rotatedP1LatLng = prj.fromPointToLatLng(rotatePoint(p1, origin, angle));
  var rotatedP2LatLng = prj.fromPointToLatLng(rotatePoint(p2, origin, angle));

  return {
    p1: rotatedP1LatLng,
    p2: rotatedP2LatLng
  };
};

function rotatePoint(point, origin, angle) {
  var angleRad = angle * Math.PI / 180.0;
  return {
    x: Math.cos(angleRad) * (point.x - origin.x) - Math.sin(angleRad) * (point.y - origin.y) + origin.x,
    y: Math.sin(angleRad) * (point.x - origin.x) + Math.cos(angleRad) * (point.y - origin.y) + origin.y
  };
}

function rotateLineToPerpendicular(lc, primaryLine, lineToRotate) {
  var line = {};
  var angleDiff = lc.getAngle(primaryLine, lineToRotate);
  var rotateAngle = angleDiff;
  var midPointOfLine = computeMidpoint(lineToRotate.p1, lineToRotate.p2);

  // Get direction of +90
  rotateAngle = rotateAngle > 0 ? rotateAngle + 90 : rotateAngle - 90;

  var rotatedLine = lc.RotateLine(lineToRotate, rotateAngle, midPointOfLine);

  //Check rotated line angle
  var checkDiff = lc.getAngle(primaryLine, rotatedLine);
  var roundedDiff = Math.round(checkDiff * 1000) / 1000;
  if (Math.abs(roundedDiff) === 90) {
    line.p1 = rotatedLine.p1;
    line.p2 = rotatedLine.p2;
  } else {
    //Rotate line to apposite direction
    rotatedLine = lc.RotateLine(lineToRotate, -rotateAngle, midPointOfLine);
    line.p1 = rotatedLine.p1;
    line.p2 = rotatedLine.p2;
  }

  return line;
}

function isObjectEmpty(obj) {
  if (Array.isArray(obj)) {
    return obj.length === 0;
  } else if (typeof obj === 'object') {
    for (var i in obj) {
      return false;
    }
    return true;
  } else {
    return !obj;
  }
}

// Parapet Logic
Roof.prototype.DrawParapet = function(lc) {
  var roof = this;
  if (!roof.parapetWallHeight || roof.parapetWallHeight == 0) return;

  // convert inches/feet
  var parapetHeight = roof.parapetWallHeight;
  if (lc.measurementSystem == 3) {
    parapetHeight = lc.ConvertNumberFromTo(parapetHeight, 2, 3, 5);
  }

  // create the primitive polylines from the roof edges
  var lines = roof.GetPolygonLines();
  for (var i = 0; i < lines.length; i++) {
    var line = [lines[i].p1, lines[i].p2];
    var primitive = new google.maps.Polyline({
      path: line,
      geodesic: true,
      shapeType: "polygon",
      strokeColor: layoutControlElementColors.Obstruction,
      strokeOpacity: 1,
      strokeWeight: 2,
      zIndex: layoutControlElementZIndexes.Parapet,
      map: lc.map,
    });

    // create the parapet obstructions from the polylines

    var idx = lc.lastIndexes.MapObject = lc.lastIndexes.MapObject + 1;
    var objectData = new Parapet(idx, primitive, "Parapet", "", parapetHeight, 0, roof, lc);
    var parapetObj = new mapObject(idx, "Obstruction", primitive, objectData);

    // track the parapet objects on the roof object
    roof.parapetWallObjects.push(parapetObj);
  }

  // trigger event to resolve shadows
  var onObjectCreated = {
    type: "onObjectCreated",
    createdObject: parapetObj
  };
  lc.dispatchEvent(onObjectCreated);
};
Roof.prototype.ClearParapet = function(lc) {
  if (this.parapetWallObjects instanceof Array && this.parapetWallObjects.length > 0) {
    var onObjectDeleted = {
      type: "onObjectDeleted",
      deletedObjects: angular.copy(this.parapetWallObjects)
    };

    // delete the parapet object drawings
    var parapetWallCount = this.parapetWallObjects.length;
    for (var i = 0; i < parapetWallCount; i++) {
      var parapetInstance = this.parapetWallObjects[i];
      parapetInstance.primitive.setMap(null);
    }

    // trigger event to resolve shadows
    lc.dispatchEvent(onObjectDeleted);
  }
  this.parapetWallObjects = [];
};

var Parapet = function(idx, primitive, name, description, height, setback, parentRoof, lc) {
  this.index = idx;
  this.name = name;
  this.description = description;
  this.height = height;
  this.setback = setback;
  this.primitive = primitive;

  this.polygon2dLocal = GetPolygon2D(primitive);

  this.parentRoof = parentRoof;

  lc.TransformObject(this, "parapet");
}
Parapet.prototype.GetPolygonLines = function() {
  var points = this.polygon2dLocal.vertices;

  var lines = [];
  var pointsLength = points.length;
  for (var i = 0; i < pointsLength; i++) {
    var p1 = i;
    var p2 = i + 1;
    if (p2 == pointsLength)
      p2 = 0;

    lines.push({
      p1: new google.maps.LatLng(points[p1].y, points[p1].x),
      p2: new google.maps.LatLng(points[p2].y, points[p2].x)
    });
  }
  return lines;
};

//Google Maps, add bounding box calc for polygons
google.maps.Polygon.prototype.getBoundingBox = function() {
  var bounds = new google.maps.LatLngBounds();

  this.getPath().forEach(function(element, index) {
    bounds.extend(element)
  });

  return (bounds);
};

// Add center calculation method
google.maps.Polygon.prototype.getCenter = function() {
  var boundsHeight = 0,
    boundsWidth = 0,
    centerPoint,
    heightIncr = 0,
    maxSearchLoops,
    maxSearchSteps = 10,
    n = 1,
    northWest,
    polygonBounds = this.getBoundingBox(),
    testPos,
    widthIncr = 0;


  // Get polygon Centroid
  centerPoint = polygonBounds.getCenter();

  if (google.maps.geometry.poly.containsLocation(centerPoint, this)) {
    // Nothing to do Centroid is in polygon use it as is
    return centerPoint;
  } else {
    maxSearchLoops = maxSearchSteps / 2;

    // Calculate NorthWest point so we can work out height of polygon NW->SE
    northWest = new google.maps.LatLng(polygonBounds.getNorthEast().lat(), polygonBounds.getSouthWest().lng());

    // Work out how tall and wide the bounds are and what our search increment will be
    boundsHeight = google.maps.geometry.spherical.computeDistanceBetween(northWest, polygonBounds.getSouthWest());
    heightIncr = boundsHeight / maxSearchSteps;
    boundsWidth = google.maps.geometry.spherical.computeDistanceBetween(northWest, polygonBounds.getNorthEast());
    widthIncr = boundsWidth / maxSearchSteps;

    // Expand out from Centroid and find a point within polygon at 0, 90, 180, 270 degrees
    for (; n <= maxSearchLoops; n++) {
      // Test point North of Centroid
      testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (heightIncr * n), 0);
      if (google.maps.geometry.poly.containsLocation(testPos, this)) {
        break;
      }

      // Test point East of Centroid
      testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (widthIncr * n), 90);
      if (google.maps.geometry.poly.containsLocation(testPos, this)) {
        break;
      }

      // Test point South of Centroid
      testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (heightIncr * n), 180);
      if (google.maps.geometry.poly.containsLocation(testPos, this)) {
        break;
      }

      // Test point West of Centroid
      testPos = google.maps.geometry.spherical.computeOffset(centerPoint, (widthIncr * n), 270);
      if (google.maps.geometry.poly.containsLocation(testPos, this)) {
        break;
      }
    }

    return (testPos);
  }
};
