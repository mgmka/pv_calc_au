function this.radians(this.degrees) {
  var pi = Math.PI;
  return this.degrees * (pi / 180);
}

function this.degrees(this.radians) {
  var pi = Math.PI;
  return this.radians * (180 / pi);
}

function calc(tilt_angle, azimuth) {
  var lat;
  var lng;
  var Tilt;
  var ndk;
  var nhk = 4.16666666666667E-03;
  var month_sum_energy_kwh;
//var center = get_center();
  lat = -37.732
  lng = 145.002
//console.log(center.lng())
  var time_zone = Math.round(lng / 0.004167 / 3600);
  ndk = 4.16666666666667E-03;
  var start = new Date('1900-01-01')
  var curent_date = new Date("2019-01-01")
  var calc_date = (curent_date - start)/ (1000 * 60 * 60 * 24);
  var no_tilt_kwh = [];

  for (var nm = 1; nm <= 12; nm++) {
    month_sum_energy_kwh = 0;
    for (var nd = 1; nd <= 30; nd++) {
      var time_past_local_midnight = 0;
      var day_sum_energy_kwh = 0;
      for (var nh = 1; nh <= 240; nh++) {
        time_past_local_midnight = time_past_local_midnight + 0.1 / 24;
        var julian_day = calc_date + 2415018.5 + nhk * nh - time_zone / 24;
        var julian_century = (julian_day - 2451545) / 36525;
        var geom_mean_long_sun = (280.46646 + julian_century * (36000.76983 + julian_century * 0.0003032)) % 360;
        var geom_mean_anom_sun = 357.52911 + julian_century * (35999.05029 - 0.0001537 * julian_century);
        var eccent_earth_orbit = 0.016708634 - julian_century * (0.000042037 + 0.0000001267 * julian_century);
        var sun_eq_of_ctr = Math.sin(this.radians(geom_mean_anom_sun)) * (1.914602 - julian_century * (0.004817 + 0.000014 * julian_century))
          + Math.sin(this.radians(2 * geom_mean_anom_sun)) * (0.019993 - 0.000101 * julian_century)
          + Math.sin(this.radians(3 * geom_mean_anom_sun)) * 0.000289;
        var sun_true_long = geom_mean_long_sun + sun_eq_of_ctr;
        var sun_true_anom = geom_mean_anom_sun + sun_eq_of_ctr;
        var sun_rad_vector = (1.000001018 * (1 - eccent_earth_orbit * eccent_earth_orbit)) / (1 + eccent_earth_orbit * Math.cos(this.radians(sun_true_anom)))
        var sun_app_long = sun_true_long - 0.00569 - 0.00478 * Math.sin(this.radians(125.04 - 1934.136 * julian_century));
        var mean_obliq_ecliptic = 23 + (26 + ((21.448 - julian_century * (46.815 + julian_century * (0.00059 - julian_century * 0.001813)))) / 60) / 60
        var dbliq_corr = mean_obliq_ecliptic + 0.00256 * Math.cos(this.radians(125.04 - 1934.136 * julian_century));
        var sun_rt_ascen = this.degrees(Math.atan2(Math.cos(this.radians(sun_app_long)),
          Math.cos(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))));
        var sun_declin = this.degrees(Math.asin(Math.sin(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))))
        var var_y = Math.tan(this.radians(dbliq_corr / 2)) * Math.tan(this.radians(dbliq_corr / 2))
        var eq_of_time = 4 * this.degrees(var_y * Math.sin(2 * this.radians(geom_mean_long_sun)) - 2 * eccent_earth_orbit
          * Math.sin(this.radians(geom_mean_anom_sun)) + 4 * eccent_earth_orbit * var_y * Math.sin(this.radians(geom_mean_anom_sun))
          * Math.cos(2 * this.radians(geom_mean_long_sun)) - 0.5 * var_y * var_y * Math.sin(4 * this.radians(geom_mean_long_sun))
          - 1.25 * eccent_earth_orbit * eccent_earth_orbit * Math.sin(2 * this.radians(geom_mean_anom_sun)))
        var ha_sunrise = this.degrees(Math.acos(Math.cos(this.radians(90.833)) / (Math.cos(this.radians(Math.round(lat))
          * Math.cos(this.radians(sun_declin))) - Math.tan(this.radians(Math.round(lat)))) * Math.tan(this.radians(sun_declin))))
        var solar_noon = (720 - 4 * lng - eq_of_time + time_zone * 60) / 1440;  //todo timezone constant
        var sunrise_time = solar_noon - ha_sunrise * 4 / 1440
        var sunset_time = solar_noon + ha_sunrise * 4 / 1440
        var sunlight_duration = 8 * ha_sunrise
        var true_solar_time = (time_past_local_midnight * 1440 + eq_of_time + 4 * lng - 60 * time_zone % 1440); //todo timezone constant
        var hour_angle;
        if (true_solar_time / 4 < 0) {
          hour_angle = true_solar_time / 4 + 180
        } else {
          hour_angle = true_solar_time / 4 - 180
        }

        var solar_zenith_angle = this.degrees(Math.acos(Math.sin(this.radians(lat)) * Math.sin(this.radians(sun_declin)) + Math.cos(this.radians(lat))
          * Math.cos(this.radians(sun_declin)) * Math.cos(this.radians(hour_angle))))
        var solar_elevation_angle = 90 - solar_zenith_angle
        var approx_atmospheric_refraction;
        if (solar_elevation_angle > 85) {
          approx_atmospheric_refraction = 0
        } else if (solar_elevation_angle > 5) {
          approx_atmospheric_refraction = (58.1 / Math.tan(this.radians(solar_elevation_angle)) - 0.07
            / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 3) + 0.000086
            / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 5)) / 3600
        } else if (solar_elevation_angle > -0.575) {
          approx_atmospheric_refraction = (1735 + solar_elevation_angle
            * (-518.2 + solar_elevation_angle * (103.4 + solar_elevation_angle * (-12.79 + solar_elevation_angle * 0.711)))) / 3600
        } else {
          approx_atmospheric_refraction = -20.772 / Math.tan(this.radians(solar_elevation_angle)) / 3600
        }
        var solar_elevation_corrected_for_atm_refraction = solar_elevation_angle + approx_atmospheric_refraction
        var solar_azimuth_angle;
        if (hour_angle > 0) {
          solar_azimuth_angle = ((this.degrees(Math.acos(((Math.sin(this.radians(lat)) * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin)))
            / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle))))) + 180) % 360)
        } else {
          solar_azimuth_angle = ((540 - this.degrees(Math.acos(((Math.sin(this.radians(lat)) * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin)))
            / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle)))))) % 360)
        }
        var air_m;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          air_m = 1 / (Math.cos((90 - solar_elevation_corrected_for_atm_refraction) / 180 * 3.14159265358979) + 0.50572
            * Math.pow(96.07995 - solar_elevation_corrected_for_atm_refraction, -1.6364))
        } else {
          air_m = 0
        }
        var i_wm2;
        if (air_m > 0) {
          i_wm2 = 1.1 * 1353 * (1 - 0 / 7.1) * 0.7 ^ (air_m ^ 0.678) + 0 / 7.1
        } else {
          i_wm2 = 0
        }
        var cos_betta;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          cos_betta = Math.cos(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          cos_betta = 0
        }
        var cos_g_air_mma = Math.cos((solar_azimuth_angle - azimuth) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(10, 2).Value
        var sin_omega = Math.sin((tilt_angle) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(9, 2).Value
        var sin_betta;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          sin_betta = Math.sin(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          sin_betta = 0
        }
        var cos_omega = Math.cos((tilt_angle) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(9, 2).Value
        var cos_tau;
        if (cos_betta * cos_g_air_mma * sin_omega + sin_betta * cos_omega > 0) {
          cos_tau = cos_betta * cos_g_air_mma * sin_omega + sin_betta * cos_omega
        } else {
          cos_tau = 0
        }
        var energy = i_wm2 * cos_tau
        // console.log(energy)
        day_sum_energy_kwh = day_sum_energy_kwh + energy
      }
      //console.log(day_sum_energy_kwh)
      month_sum_energy_kwh = month_sum_energy_kwh + day_sum_energy_kwh / 10000 * 1.05
      // Worksheets("Sheet1").Cells(14 + nm, 2).Value = MonthSumEnergykWh
      calc_date = calc_date + 1
    }
    no_tilt_kwh.push(month_sum_energy_kwh);
    //no_tilt_mj.push(month_sum_energy_kwh * 3.6)
  }
  //console.log(no_tilt_kwh)
  return no_tilt_kwh
}

function calculate_pw() {
  var Pnom = this.number_of_panels * this.panel_watts / 1000
  var Ktol = this.manufacures_tolerance / 100
  var KcabDC = this.cable_dc_loss / 100
  var KtempP = this.power_temp / 100
  var KcabAC = this.cable_ac_loss / 100
  var Kinv = (1 - this.efficiency / 100)
  var Kmount;
  if (this.installation_type === 1) {
    Kmount = 35
  } else if (this.installation_type === 2) {
    Kmount = 30
  } else if (this.installation_type === 3) {
    Kmount = 25
  } else {
    Kmount = 35
  }
  var no_tilt_kwh = [];
  var no_tilt_mj = [];
  var with_tilt_kwh = []
  var with_tilt_mj = []
  var actaul_no_tilt = []
  var actaul_with_tilt = []
  var actaul_with_tilt_kwh = []
  var actaul_pv_system = []

  no_tilt_kwh = calc(0, 0)
  for (const ell of no_tilt_kwh) {
    no_tilt_mj.push(ell * 3.6)
  }
  with_tilt_kwh = calc(this.polygon.tilt_angel, this.polygon.azimuth)
  for (const ell of with_tilt_kwh) {
    with_tilt_mj.push(ell * 3.6)
  }
  for (var nm = 1; nm <= 12; nm++) {
    var k_shade = (1 - this.polygon.shade[nm - 1] / 100) //Worksheets("vsplyvayushee_okno").Cells(34 + nm
    var k_temp = this.power_temp / 100 * (this.table_data[nm - 1]['solar'] + Kmount - 20)
    actaul_no_tilt.push(this.table_data[nm - 1]['solar'] * 30)
    actaul_with_tilt.push(with_tilt_mj[nm - 1] * actaul_no_tilt[nm - 1] / no_tilt_mj[nm - 1])
    actaul_with_tilt_kwh.push(actaul_with_tilt[nm - 1] / 3.6)
    actaul_pv_system.push(actaul_with_tilt_kwh[nm - 1] * Pnom * (1 - Ktol - KcabDC - KcabAC - k_shade - Kinv - KtempP / 100 * (k_temp + Kmount - 20)))
  }


}

calc()





solar_calc: function () {
  var lat;
  var lng;
  var Tilt;
  //var Orient;
  //var CurrYear;
  var ndk;
  var nhk;
  var month_sum_energy_kwh;
  var center = this.get_center();
  lat = center.lat()
  lng = center.lng()
  console.log(center.lng())
  var time_zone = Math.round(lng / 0.004167 / 3600);
  ndk = 4.16666666666667E-03;
  var start = new Date('1900-01-01')
  var curent_date = new Date("2019-01-01")
  var calc_date = (curent_date - start)/ (1000 * 60 * 60 * 24);
  var no_tilt_kwh = [];
  var no_tilt_mj = [];

  for (var nm = 1; nm <= 12; nm++) {
    month_sum_energy_kwh = 0;
    for (var nd = 1; nd <= 30; nd++) {
      var time_past_local_midnight = 0;
      var day_sum_energy_kwh = 0;
      for (var nh = 1; nh <= 240; nh++) {
        time_past_local_midnight = time_past_local_midnight + 0.1 / 24;
        var julian_day = calc_date + 2415018.5 + nh - time_zone / 24;
        var julian_century = (julian_day - 2451545) / 36525;
        var geom_mean_long_sun = (280.46646 + julian_century * (36000.76983 + julian_century * 0.0003032)) % 360;
        var geom_mean_anom_sun = 357.52911 + julian_century * (35999.05029 - 0.0001537 * julian_century);
        var eccent_earth_orbit = 0.016708634 - julian_century * (0.000042037 + 0.0000001267 * julian_century);
        var sun_eq_of_ctr = Math.sin(this.radians(geom_mean_anom_sun)) * (1.914602 - julian_century * (0.004817 + 0.000014 * julian_century))
          + Math.sin(this.radians(2 * geom_mean_anom_sun)) * (0.019993 - 0.000101 * julian_century)
          + Math.sin(this.radians(3 * geom_mean_anom_sun)) * 0.000289;
        var sun_true_long = geom_mean_long_sun + sun_eq_of_ctr;
        var sun_true_anom = geom_mean_anom_sun + sun_eq_of_ctr;
        var sun_rad_vector = (1.000001018 * (1 - eccent_earth_orbit * eccent_earth_orbit)) / (1 + eccent_earth_orbit * Math.cos(this.radians(sun_true_anom)))
        var sun_app_long = sun_true_long - 0.00569 - 0.00478 * Math.sin(this.radians(125.04 - 1934.136 * julian_century));
        var mean_obliq_ecliptic = 23 + (26 + ((21.448 - julian_century * (46.815 + julian_century * (0.00059 - julian_century * 0.001813)))) / 60) / 60
        var dbliq_corr = mean_obliq_ecliptic + 0.00256 * Math.cos(this.radians(125.04 - 1934.136 * julian_century));
        var sun_rt_ascen = this.degrees(Math.atan2(Math.cos(this.radians(sun_app_long)),
          Math.cos(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))));
        var sun_declin = this.degrees(Math.asin(Math.sin(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))))
        var var_y = Math.tan(this.radians(dbliq_corr / 2)) * Math.tan(this.radians(dbliq_corr / 2))
        var eq_of_time = 4 * this.degrees(var_y * Math.sin(2 * this.radians(geom_mean_long_sun)) - 2 * eccent_earth_orbit
          * Math.sin(this.radians(geom_mean_anom_sun)) + 4 * eccent_earth_orbit * var_y * Math.sin(this.radians(geom_mean_anom_sun))
          * Math.cos(2 * this.radians(geom_mean_long_sun)) - 0.5 * var_y * var_y * Math.sin(4 * this.radians(geom_mean_long_sun))
          - 1.25 * eccent_earth_orbit * eccent_earth_orbit * Math.sin(2 * this.radians(geom_mean_anom_sun)))
        var ha_sunrise = this.degrees(Math.acos(Math.cos(this.radians(90.833)) / (Math.cos(this.radians(Math.round(lat)) * Math.cos(this.radians(sun_declin))) - Math.tan(this.radians(Math.round(lat)))) * Math.tan(this.radians(sun_declin))))
        var solar_noon = (720 - 4 * lng - eq_of_time + time_zone * 60) / 1440;  //todo timezone constant
        var sunrise_time = solar_noon - ha_sunrise * 4 / 1440
        var sunset_time = solar_noon + ha_sunrise * 4 / 1440
        var sunlight_duration = 8 * ha_sunrise
        var true_solar_time = (time_past_local_midnight * 1440 + eq_of_time + 4 * lng - 60 * time_zone % 1440); //todo timezone constant
        var hour_angle;
        if (true_solar_time / 4 < 0) {
          hour_angle = true_solar_time / 4 + 180
        } else {
          hour_angle = true_solar_time / 4 - 180
        }

        var solar_zenith_angle = this.degrees(Math.acos(Math.sin(this.radians(lat)) * Math.sin(this.radians(sun_declin)) + Math.cos(this.radians(lat))* Math.cos(this.radians(sun_declin)*Math.cos(this.radians(hour_angle)))))
        var solar_elevation_angle = 90 - solar_zenith_angle
        var approx_atmospheric_refraction;
        if (solar_elevation_angle > 85) {
          approx_atmospheric_refraction = 0
        } else if (solar_elevation_angle > 5) {
          approx_atmospheric_refraction = (58.1 / Math.tan(this.radians(solar_elevation_angle)) - 0.07
            / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 3) + 0.000086
            / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 5)) / 3600
        } else if (solar_elevation_angle > -0.575) {
          approx_atmospheric_refraction = (1735 + solar_elevation_angle
            * (-518.2 + solar_elevation_angle * (103.4 + solar_elevation_angle * (-12.79 + solar_elevation_angle * 0.711)))) / 3600
        } else {
          approx_atmospheric_refraction = -20.772 / Math.tan(this.radians(solar_elevation_angle)) / 3600
        }
        var solar_elevation_corrected_for_atm_refraction = solar_elevation_angle + approx_atmospheric_refraction
        var solar_azimuth_angle;
        if (hour_angle > 0) {
          solar_azimuth_angle = ((this.degrees(Math.acos(((Math.sin(this.radians(lat)) * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin))) / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle))))) + 180) % 360)
        } else {
          solar_azimuth_angle = ((540 - this.degrees(Math.acos(((Math.sin(this.radians(lat)) * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin))) / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle))))) ) % 360)
        }
        var air_m;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          air_m = 1 / (Math.cos((90 - solar_elevation_corrected_for_atm_refraction) / 180 * 3.14159265358979) + 0.50572
            * Math.pow(96.07995 - solar_elevation_corrected_for_atm_refraction, -1.6364))
        } else {
          air_m = 0
        }
        var i_wm2;
        if (air_m > 0) {
          i_wm2 = 1.1 * 1353 * (1 - 0 / 7.1) * 0.7 ^ (air_m ^ 0.678) + 0 / 7.1
        } else {
          i_wm2 = 0
        }
        var cos_betta;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          cos_betta = Math.cos(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          cos_betta = 0
        }
        var cos_g_air_mma = Math.cos((solar_azimuth_angle - 0) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(10, 2).Value
        var sin_omega = Math.sin((0) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(9, 2).Value
        var sin_betta;
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          sin_betta = Math.sin(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          sin_betta = 0
        }
        var cos_omega = Math.cos((0) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(9, 2).Value
        var cos_tau;
        if (cos_betta * cos_g_air_mma * sin_omega + sin_betta * cos_omega > 0) {
          cos_tau = cos_betta * cos_g_air_mma * sin_omega + sin_betta * cos_omega
        } else {
          cos_tau = 0
        }
        var energy = i_wm2 * cos_tau
        day_sum_energy_kwh = day_sum_energy_kwh + energy
      }
      month_sum_energy_kwh = month_sum_energy_kwh + day_sum_energy_kwh / 10000 * 1.05
      // Worksheets("Sheet1").Cells(14 + nm, 2).Value = MonthSumEnergykWh
      calc_date = calc_date + 1
    }
    no_tilt_kwh.push(month_sum_energy_kwh);
    no_tilt_mj.push(month_sum_energy_kwh * 3.6)
  }


  time_zone = Math.round(lng / 0.004167 / 3600)
  ndk = 4.16666666666667E-03
  var start = new Date('1900-01-01')
  var curent_date = new Date("2019-01-01")
  var calc_date = (curent_date - start)/ (1000 * 60 * 60 * 24);

  var Pnom = this.number_of_panels * this.panel_watts / 1000
  var Ktol = this.manufacures_tolerance / 100
  var KcabDC = this.cable_dc_loss / 100
  var KtempP = this.power_temp / 100
  var KcabAC = this.cable_ac_loss / 100
  var Kinv = (1 - this.efficiency / 100)
  var Kmount;
  if (this.installation_type === 1) {
    Kmount = 35
  } else if (this.installation_type === 2) {
    Kmount = 30
  } else if (this.installation_type === 3) {
    Kmount = 25
  } else {
    Kmount = 35
  }
  var with_tilt_kwh = []
  var with_tilt_mj = []
  var actaul_no_tilt = []
  var actaul_with_tilt = []
  var actaul_with_tilt_kwh = []
  var actaul_pv_system = []

  for (nm = 1; nm <= 12; nm++) {
    month_sum_energy_kwh = 0
    for (nd = 1; nd <= 30; nd++) {
      time_past_local_midnight = 0
      day_sum_energy_kwh = 0
      for (nh = 1; nh <= 240; nh++) {
        time_past_local_midnight = time_past_local_midnight + 0.1 / 24
        julian_day = calc_date + 2415018.5 + nh - time_zone / 24
        julian_century = (julian_day - 2451545) / 36525
        geom_mean_long_sun = (280.46646 + julian_century * (36000.76983 + julian_century * 0.0003032)) % 360
        geom_mean_anom_sun = 357.52911 + julian_century * (35999.05029 - 0.0001537 * julian_century)
        eccent_earth_orbit = 0.016708634 - julian_century * (0.000042037 + 0.0000001267 * julian_century)
        sun_eq_of_ctr = Math.sin(this.radians(geom_mean_anom_sun)) * (1.914602 - julian_century
          * (0.004817 + 0.000014 * julian_century)) + Math.sin(this.radians(2 * geom_mean_anom_sun))
          * (0.019993 - 0.000101 * julian_century) + Math.sin(this.radians(3 * geom_mean_anom_sun)) * 0.000289
        sun_true_long = geom_mean_long_sun + sun_eq_of_ctr
        sun_true_anom = geom_mean_anom_sun + sun_eq_of_ctr
        sun_rad_vector = (1.000001018 * (1 - eccent_earth_orbit * eccent_earth_orbit)) / (1 + eccent_earth_orbit * Math.cos(this.radians(sun_true_anom)))
        sun_app_long = sun_true_long - 0.00569 - 0.00478 * Math.sin(this.radians(125.04 - 1934.136 * julian_century))
        mean_obliq_ecliptic = 23 + (26 + ((21.448 - julian_century * (46.815 + julian_century * (0.00059 - julian_century * 0.001813)))) / 60) / 60
        dbliq_corr = mean_obliq_ecliptic + 0.00256 * Math.cos(this.radians(125.04 - 1934.136 * julian_century))
        sun_rt_ascen = this.degrees(Math.atan2(Math.cos(this.radians(sun_app_long)),
          Math.cos(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))))
        sun_declin = this.degrees(Math.asin(Math.sin(this.radians(dbliq_corr)) * Math.sin(this.radians(sun_app_long))))
        var_y = Math.tan(this.radians(dbliq_corr / 2)) * Math.tan(this.radians(dbliq_corr / 2))
        eq_of_time = 4 * this.degrees(var_y * Math.sin(2 * this.radians(geom_mean_long_sun)) - 2
          * eccent_earth_orbit * Math.sin(this.radians(geom_mean_anom_sun)) + 4 * eccent_earth_orbit
          * var_y * Math.sin(this.radians(geom_mean_anom_sun)) * Math.cos(2 * this.radians(geom_mean_long_sun))
          - 0.5 * var_y * var_y * Math.sin(4 * this.radians(geom_mean_long_sun)) - 1.25 * eccent_earth_orbit
          * eccent_earth_orbit * Math.sin(2 * this.radians(geom_mean_anom_sun)))
        ha_sunrise = this.degrees(Math.acos(Math.cos(this.radians(90.833)) / (Math.cos(this.radians(Math.round(lat)) * Math.cos(this.radians(sun_declin))) - Math.tan(this.radians(Math.round(lat)))) * Math.tan(this.radians(sun_declin))))
        solar_noon = (720 - 4 * lng - eq_of_time + time_zone * 60) / 1440
        sunrise_time = solar_noon - ha_sunrise * 4 / 1440
        sunset_time = solar_noon + ha_sunrise * 4 / 1440
        sunlight_duration = 8 * ha_sunrise
        true_solar_time = (time_past_local_midnight * 1440 + eq_of_time + 4 * lng - 60 * time_zone % 1440)
        if (true_solar_time / 4 < 0) {
          hour_angle = true_solar_time / 4 + 180
        } else {
          hour_angle = true_solar_time / 4 - 180
        }
        solar_zenith_angle = this.degrees(Math.acos(Math.sin(this.radians(lat))
          * Math.sin(this.radians(sun_declin)) + Math.cos(this.radians(lat))
          * Math.cos(this.radians(sun_declin)) * Math.cos(this.radians(hour_angle))))
        solar_elevation_angle = 90 - solar_zenith_angle
        if (solar_elevation_angle > 85) {
          approx_atmospheric_refraction = 0
        } else if (solar_elevation_angle > 5) {
          approx_atmospheric_refraction = (58.1 / Math.tan(this.radians(solar_elevation_angle))
            - 0.07 / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 3) + 0.000086
            / Math.pow(Math.tan(this.radians(solar_elevation_angle)), 5)) / 3600
        } else if (solar_elevation_angle > -0.575) {
          approx_atmospheric_refraction = (1735 + solar_elevation_angle * (-518.2 + solar_elevation_angle
            * (103.4 + solar_elevation_angle * (-12.79 + solar_elevation_angle * 0.711)))) / 3600
        } else {
          approx_atmospheric_refraction = -20.772 / Math.tan(this.radians(solar_elevation_angle)) / 3600
        }
        solar_elevation_corrected_for_atm_refraction = solar_elevation_angle + approx_atmospheric_refraction
        if (hour_angle > 0) {
          solar_azimuth_angle = (this.degrees(Math.acos(((Math.sin(this.radians(lat))
            * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin)))
            / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle))))) + 180 % 360)
        } else {
          solar_azimuth_angle = (540 - this.degrees(Math.acos(((Math.sin(this.radians(lat))
            * Math.cos(this.radians(solar_zenith_angle))) - Math.sin(this.radians(sun_declin)))
            / (Math.cos(this.radians(lat)) * Math.sin(this.radians(solar_zenith_angle))))) % 360)
        }
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          air_m = 1 / (Math.cos((90 - solar_elevation_corrected_for_atm_refraction) / 180 * 3.14159265358979)
            + 0.50572 * Math.pow(96.07995 - solar_elevation_corrected_for_atm_refraction, -1.6364))
        } else {
          air_m = 0
        }

        if (air_m > 0) {
          i_wm2 = 1.1 * 1353 * (1 - 0 / 7.1) * 0.7 ^ (air_m ^ 0.678) + 0 / 7.1
        } else {
          i_wm2 = 0
        }
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          cos_betta = Math.cos(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          cos_betta = 0
        }
        var cos_gamma = Math.cos((solar_azimuth_angle - 90) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(12, 2).Value
        sin_omega = Math.sin((this.tilt_angle) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(11, 2
        if (solar_elevation_corrected_for_atm_refraction > 0) {
          sin_betta = Math.sin(solar_elevation_corrected_for_atm_refraction / 180 * 3.14159265358979)
        } else {
          sin_betta = 0
        }
        cos_omega = Math.cos((this.tilt_angle) / 180 * 3.14159265358979) //Worksheets("Sheet1").Cells(11, 2).Value
        if (cos_betta * cos_gamma * sin_omega + sin_betta * cos_omega > 0) {
          cos_tau = cos_betta * cos_gamma * sin_omega + sin_betta * cos_omega
        } else {
          cos_tau = 0
        }
        energy = i_wm2 * cos_tau
        day_sum_energy_kwh = day_sum_energy_kwh + energy
      }
      month_sum_energy_kwh = month_sum_energy_kwh + day_sum_energy_kwh / 10000
      //Worksheets("Sheet1").Cells(14 + nm, 3).Value = MonthSumEnergykWh

      calc_date = calc_date + 1
    }
    with_tilt_kwh.push(month_sum_energy_kwh)
    with_tilt_mj.push(month_sum_energy_kwh*3.6)
    var k_shade = (1 - this.polygon.shade[nm-1] / 100) //Worksheets("vsplyvayushee_okno").Cells(34 + nm
    var k_temp = this.power_temp / 100 * (this.table_data[nm-1]['solar'] + Kmount - 20)
    //this.get_solar()
    actaul_no_tilt.push(this.table_data[nm-1]['solar']*30)
    actaul_with_tilt.push(with_tilt_mj[nm-1]*actaul_no_tilt[nm-1]/no_tilt_mj[nm-1])
    actaul_with_tilt_kwh.push(actaul_with_tilt[nm-1]/3.6)
    actaul_pv_system.push(actaul_with_tilt_kwh[nm-1]*Pnom*(1-Ktol-KcabDC- KcabAC - k_shade - Kinv - KtempP / 100 * (k_temp + Kmount - 20)))
  }
  console.log(actaul_pv_system)
}
