//store/index.js
export const state = () => ({
  azimuth: null
})

export const mutations = {
  setAzimuth(state, angle) {
    state.azimuth = angle
  }
}

export const getters = {
  getAzimuth(state) {
    return state.azimuth
  }
}
